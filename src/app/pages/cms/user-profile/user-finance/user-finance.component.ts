import { ToastService } from './../../../../_services/toast.service';
import { UsersService } from './../../../../_services/users.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { fade } from './../../../../_animations/animations';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserPayoutComponent } from './payout/user-payout.component';

@Component({
  selector: 'app-user-finance',
  templateUrl: './user-finance.component.html',
  styleUrls: ['./user-finance.component.scss'],
  animations: [fade]
})
export class UserFinanceComponent implements OnInit, OnDestroy {
  public actualTab = 0;
  private userUid = '';
  public incomes = [];
  public payout = [];
  public toShowIncomes;
  public toShowPayouts;
  private subscription: Subscription = new Subscription();
  public loading = false;
  public activePageIncomes = 1;
  public activePagePayouts = 1;
  public toShow = 8;
  public columnsMobile = [
    {
      fieldName: 'date',
      title: 'Дата',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'status',
      title: 'Статус',
      useColumn: true
    },
    {
      fieldName: 'amount',
      title: 'Сумма',
      extraValue: 'amount',
      useColumn: true
    },
    {
      fieldName: 'description',
      title: 'Описание',
      extraValue: 'useName',
      row: 1
    },
  ];

  constructor(private dialog: MatDialog,
              private usersService: UsersService,
              public route: ActivatedRoute,
              public toastService: ToastService) { }

  ngOnInit(): void {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.userUid = params.get('userUid');
        this.getTransaktionsList();
    }));
  }

  getTransaktionsList() {
    this.subscription.add(this.usersService.getTransaktionsList(this.userUid)
      .subscribe((resultList: any) => {
        this.payout = resultList.result.listOfOutComes;
        this.incomes = resultList.result.listOfInComes;
        this.toShowIncomes = this.incomes.slice(0, this.toShow);
        this.toShowPayouts = this.payout.slice(0, this.toShow);
    }));
  }

  onChangePage(isPayouts) {
    if (isPayouts) {
      let maxValue = this.toShow * this.activePagePayouts;
      this.toShowPayouts = this.payout.slice((maxValue - this.toShow), maxValue);
    } else {
      let maxValue = this.toShow * this.activePageIncomes;
      this.toShowIncomes = this.incomes.slice((maxValue - this.toShow), maxValue);
    }
  }

  openPayout() {
    const payoutDialog = this.dialog.open(UserPayoutComponent, {
      backdropClass: 'dialog-back',
      width: '490px',
      data: {userUid: this.userUid}
    });
    this.subscription.add(payoutDialog.afterClosed()
    .subscribe(result => {
      if (result) {
        this.getTransaktionsList();
      }
    }));
  }



  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
