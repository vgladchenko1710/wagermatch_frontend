import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'wagers',
  templateUrl: './wagers.component.html',
  styleUrls: ['./wagers.component.scss']
})
export class WagersComponent implements OnInit, OnChanges {
  @Input() height = 19;
  @Input() color = 'black';
  public width = 19;
  constructor() { }

  ngOnInit(): void {
    this.width = this.height * (99 / 89);
  }

  ngOnChanges(): void {
    this.width = this.height * (99 / 89);
  }

}
