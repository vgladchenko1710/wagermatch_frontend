import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  authenticate(user: any) {
    return this.httpClient.post(`${environment.apiUrl}/auth/login`, user);
  }

  authenticateVk(data: any) {
    return this.httpClient.post(`${environment.apiUrl}/auth/vklogin`, data);
  }

  register(user: any) {
    return this.httpClient.post(`${environment.apiUrl}/users`, user);
  }

  emailVerify(key: any) {
    return this.httpClient.post(`${environment.apiUrl}/cmspublic/user`, key);
  }

  passwordRequest(email: any) {
    return this.httpClient.post(`${environment.apiUrl}/cmspublic/resetpassword`, email);
  }

  passwordChangingAccess(uid: any) {
    return this.httpClient.get(`${environment.apiUrl}/cmspublic/resetpassword/${uid}`);
  }

  passwordChange(uid: any, password) {
    return this.httpClient.put(`${environment.apiUrl}/cmspublic/resetpassword/${uid}`, password);
  }

  getUserIp() {
    return this.httpClient.get('http://api.ipify.org/?format=json');
  }

}
