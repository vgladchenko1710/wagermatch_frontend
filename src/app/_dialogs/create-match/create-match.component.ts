import { MatchesEventService } from './../../_services/events/matches-event.service';
import { RechargeComponent } from './../../pages/my-profile/account/recharge/recharge.component';
import { UserRechargeComponent } from './../../pages/cms/user-profile/user-account/recharge/user-recharge.component';
import { UsersService } from './../../_services/users.service';
import { NotificationService } from './../../_services/notification-service';
import { ToastService } from '../../_services/toast.service';
import { MatchesService } from '../../_services/matches.service';
import { Router } from '@angular/router';
import { InfoComponent } from '../info/info.component';
import { fadeMix } from '../../_animations/animations';
import { Subscription } from 'rxjs';
import { AuthUserService } from '../../_services/auth/auth-user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Inject, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { faXbox, faPlaystation } from '@fortawesome/fontawesome-free-brands';

@Component({
  selector: 'app-create-match',
  templateUrl: './create-match.component.html',
  styleUrls: ['./create-match.component.scss'],
  animations: [fadeMix]
})
export class CreateMatchComponent implements OnInit, OnDestroy {
  @ViewChild('bidInput', { static: false }) bidInput: ElementRef;
  private subscription: Subscription = new Subscription();
  public editData;
  public form: FormGroup;
  public activePlatform;
  public commission = 0;
  public commissionPerc = 0;
  public maxBid = 1000000;
  public minBid = 100;
  public isLoad = false;
  XboxIcon = faXbox;
  Ps4Icon = faPlaystation;
  withAlly = [{viewValue: 'Сам', value: 'one'}, {viewValue: 'С другом', value: 'many'}];
  public timeExpire = [
            {viewValue: '30 минут', value: 30},
            {viewValue: '1 час', value: 60},
            {viewValue: '3 часа', value: 180},
            {viewValue: '6 часов', value: 360},
          ];
  public gameMode = [
    {viewValue: 'Онлайн сезоны', value: 'Онлайн сезоны'},
    {viewValue: 'FUT', value: 'FUT'},
  ];

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              private authUserService: AuthUserService,
              private dialog: MatDialog,
              public router: Router,
              public matchesService: MatchesService,
              private matchesEventService: MatchesEventService,
              private usersService: UsersService,
              public toastService: ToastService,
              public dialogRef: MatDialogRef<CreateMatchComponent>) {}

  ngOnInit(): void {
    this.getRate();
    this.createForm();
    this.activePlatform =  this.authUserService.getUserPlatform();
    this.form.patchValue({platform: this.activePlatform});
    this.editData = this.data.editInfo;
    if (this.editData) {
      this.updateValues();
    }
  }

  createForm() {
    this.form = new FormGroup({
      bid: new FormControl(null, [Validators.required, Validators.max(this.maxBid)]),
      platform: new FormControl(null, [Validators.required]),
      withAlly: new FormControl(null, [Validators.required]),
      enemyWithAlly: new FormControl(null),
      gameMode: new FormControl((null), [Validators.required]),
      timeExpire: new FormControl((null), [Validators.required])
    });
    this.subscription.add(this.form.get('withAlly')
      .valueChanges.subscribe((value: any) => {
        const enemyWithAlly = this.form.get('enemyWithAlly');
        if (value === 'many') {
          enemyWithAlly.setValue(true);
        }
    }));
  }

  getRate() {
    this.usersService.getRates(this.authUserService.getUserUid())
      .subscribe ((response: any) => {
        this.commission = +response.result.matchRate;
        this.commissionPerc = Math.round((1 - this.commission) * 100);
        this.minBid = +response.result.minBid;
        this.maxBid = +response.result.maxBid;
        this.form.get('bid').setValidators([Validators.required,  Validators.max(this.maxBid), Validators.min(this.minBid)]);
    });
  }

  updateValues() {
    console.log(this.editData);
    this.timeExpire.unshift({viewValue: this.editData.timeExpire, value: 1});
    this.form.patchValue({
      bid: this.editData.bid,
      platform: this.editData.platform,
      withAlly: this.editData.withAlly ? 'many' : 'one',
      enemyWithAlly: this.editData.enemyWithAlly,
      gameMode: this.editData.gameMode,
      timeExpire: 1
    });
  }

  openInfo() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Недостаточный баланс для покрытия суммы пари!',
              infoText: 'Пополните свой баланс или измените сумму пари, чтобы создать матч',
              btn: 'Изменить сумму пари',
              btnOrange: 'Пополнить баланс'
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        if (result === 'btnOrange') {
          this.recharge();
        } else {
          this.bidInput.nativeElement.focus();
        }
      }));
  }

  createMatch() {
    this.isLoad = true;
    const userUid = this.authUserService.getUserUid();
    const match = {
      bid: this.form.get('bid').value,
      platform: this.form.get('platform').value,
      withAlly: this.form.get('withAlly').value === 'many' ? true : false,
      enemyWithAlly: this.form.get('enemyWithAlly').value ? this.form.get('enemyWithAlly').value : false,
      gameMode: this.form.get('gameMode').value,
      timeExpire: this.form.get('timeExpire').value
    };
    if (this.editData) {
      this.subscription.add(this.matchesService.editMatch(userUid, this.editData.matchUid, match)
        .subscribe ((response: any) => {
          this.toastService.success(response.message);
          this.matchesEventService.toUpdateBalance((+this.editData.bid - match.bid));
          setTimeout(() => {
            this.dialogRef.close(true);
          }, 1000);
        }, (err) => {
            this.isLoad = false;
            const error = err.error.errors[0].message;
            if (error.includes('Пополни кошелёк')) {
              this.openInfo();
            } else {
              this.toastService.error(error);
            }
        }));
    } else {
      this.subscription.add(this.matchesService.createMatch(userUid, match)
        .subscribe ((response: any) => {
          this.toastService.success(response.message);
          setTimeout(() => {
            this.router.navigate(['matches/match', response.results]);
            this.matchesEventService.toUpdateBalance(-match.bid);
            this.dialogRef.close();
          }, 1000);
        }, (err) => {
            this.isLoad = false;
            const error = err.error.errors[0].message;
            if (error.includes('Пополни кошелёк')) {
              this.openInfo();
            } else {
              this.toastService.error(error);
            }
      }));
    }
  }

  recharge() {
    this.dialog.open(RechargeComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '500px',
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
