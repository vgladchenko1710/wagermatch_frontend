export interface GetMatch {
    page: number;
    pageSize: number;
    platform: string;
    bidFrom: any;
    bidTo: any;
    sort: {
        isDateAsc: boolean;
        isBidAsc: boolean;
        isFirstData: boolean;
    };
}
