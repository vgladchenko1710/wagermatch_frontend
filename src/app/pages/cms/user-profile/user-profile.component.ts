import { InfoComponent } from './../../../_dialogs/info/info.component';
import { ToastService } from './../../../_services/toast.service';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from './../../../_services/users.service';
import { Subscription } from 'rxjs';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public loading = false;
  public userUid = '';
  public user: any = {
    login: '-',
    status: '-'
  };

  constructor(private authUserService: AuthUserService,
              private usersService: UsersService,
              public toastService: ToastService,
              public dialog: MatDialog,
              public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.userUid = params.get('userUid');
        this.getUserSettings();
    }));
  }

  getUserSettings() {
    this.subscription.add(this.usersService.getUsersSetting(this.userUid)
      .subscribe((response: any) => {
        this.user.login = response.result.login;
        this.user.status = response.result.status;
      }
    ));
  }

  deactivateUsers(newStatus) {
    const data = {
      userUidLists: [this.userUid],
      status: newStatus ? 'active' : 'block'
    };
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: `Ты действительно хочешь ` + (newStatus ?  `разблокировать ${this.user.login} ?` : `заблокировть ${this.user.login} ?`),
              infoText: '',
              btn: `Вернуться назад`,
              btnOrange: newStatus ? `Разблокировать` : 'Заблокировать'
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        console.log(result);
        if (result === 'btnOrange') {
          this.subscription.add(this.usersService.deactivateUsers(this.authUserService.getUserUid(), data)
          .subscribe ((response: any) => {
            this.toastService.success(response.message);
            this.getUserSettings();
            }, (err) => {
              this.toastService.warning(err.error.errors[0].message);
        }));
        }
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
