import { MatchStatusEnum } from './../../pages/matches/match.model';
import { NotificationService } from './../../_services/notification-service';
import { MatchesEventService } from './../../_services/events/matches-event.service';
import { UsersService } from './../../_services/users.service';
import { Balanse } from './../../_models/my-profile-interface';
import { Subscription } from 'rxjs';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { isTablet } from 'src/app/_services/help-services/device-detection';

@Component({
  selector: 'app-header-name',
  templateUrl: './header-name.component.html',
  styleUrls: ['./header-name.component.scss']
})
export class HeaderNameComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();
  public isTablet = isTablet();
  public balance = new Balanse();
  public user = {
    login: '',
  };

  constructor(private authUserService: AuthUserService,
              private usersService: UsersService,
              private matchesEventService: MatchesEventService) { }

  ngOnInit(): void {
    this.user = this.authUserService.getUser();
    this.getAccountInfo();
    this.listenToUpdateBalance();
  }

  getAccountInfo() {
    console.log('get balanse')
    this.subscription.add(this.usersService.getUserAccountInfo(this.authUserService.getUserUid())
    .subscribe((response: any) => {
      this.balance.actual =  response.result.bank;
      this.balance.toPay = response.result.toPay;
      this.balance.inMatches = response.result.inTheGame;
    }
   ));
  }

  listenToUpdateBalance() {
    this.subscription.add(this.matchesEventService.getBalance$
      .subscribe((wagers: number) => {
        console.log('listen')
        if (wagers === 0) {
          this.getAccountInfo();
        } else {
          this.balance.actual += wagers;
        }
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
