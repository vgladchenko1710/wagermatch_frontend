export const environment = {
  production: true,
  apiUrl: 'https://api.wager-match.pro/api',
  socketUrl: 'https://sockets.wager-match.pro',
  // apiUrl: 'http://localhost:3020/api',
  // socketUrl: 'http://localhost:2020',
  frontend: 'app.wager-match.pro',
  VERSION: '1.0.0',
  discordLink: 'https://discord.gg/N4AdGvMh7T'
};
