import { AuthUserService } from './../../_services/auth/auth-user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {
  public loading = false;
  public isAdmin: boolean;
  public user: any = {
    login: 'Anonim',
  };

  constructor(private authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.user = this.authUserService.getUser();
    this.isAdmin = this.authUserService.isAdmin();
  }

}
