import { PromoActionComponent } from './promo-action/promo-action.component';
import { PromoService } from './../_cms-services/promo.service';
import { ToastService } from './../../../_services/toast.service';
import { Pagination } from './../../../_models/pagination';
import { PROMO_COLUMNS, IPromo, PROMO_STATUSES } from './promo.model';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.scss']
})
export class PromoComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();
  public pagination = new Pagination();
  public isLoading: boolean;
  public columns = PROMO_COLUMNS;
  public statuses = PROMO_STATUSES;
  public status = 'all';
  public promoList: IPromo[] = [];

  constructor(private toastService: ToastService,
              private promoService: PromoService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.isLoading = true;
    this.subscription.add(this.promoService.getPromoList(this.status, this.pagination.newFormatRequest())
      .subscribe ((response: {result: {list: IPromo[], totalItems: number}}) => {
        this.promoList = response.result.list;
        this.pagination.totalItems = response.result.totalItems;
        this.isLoading = false;
    }, (err)=> {
      this.toastService.error("Не удалось получить данные");
      this.isLoading = false;
    }));
  }

  openPromo(data?: IPromo) {
    const actionPromoDialog =  this.dialog.open(PromoActionComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '500px',
      data
    });
    this.subscription.add(actionPromoDialog.afterClosed()
      .subscribe(res=> {
        if (res) this.getList()
      }))
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
