import { AuthUserService } from './../../_services/auth/auth-user.service';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Component, OnInit, Input, Output, ViewChild, OnChanges, EventEmitter } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-expanded-table',
  templateUrl: './expanded-table.component.html',
  styleUrls: ['./expanded-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ExpandedTableComponent implements OnInit, OnChanges {
  @Input() data: any = [];
  @Input() columns: any = {};
  @Input() onRowClick: (event: any) => any;
  @Input() disableExpand: boolean;
  @Input() useHead: boolean;
  @Input() rowCount = 1;
  @Input() paginationData;
  @Input() paginationSizeOptions = [30, 50, 100, 200];
  @Input() totalRows;
  @Input() usePagination: boolean;
  @Input() userUid = '';
  @Input() isAdmin = false;
  @Output() elementClicked: any = new EventEmitter();
  @Output() btnClicked: any = new EventEmitter();
  @Output() btnClicked2: any = new EventEmitter();
  @Output() paginationUpdate = new EventEmitter();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public dataSource: any;
  public displayedColumns: string[] = [];
  public expandedColumn = [];
  public expandedElement;

  isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');


  constructor(public authUserService: AuthUserService) { }

  ngOnInit() {
    if (this.usePagination) {
      this.paginationData.pageSize = this.paginationSizeOptions[0];
    }
    this.dataSource.sort = this.sort;
  }

  elementClick(columnName, row) {
    this.elementClicked.emit({columnName, row});
  }

  updatePagination(data, type) {
    this.paginationUpdate.emit({data, type});
  }

  ngOnChanges() {
    this.tableInitialization();
  }

  tableInitialization() {
    const columns = [];
    const expandedColumn = [];
    for (const column of this.columns) {
      column.useColumn ? columns.push(column.fieldName) : expandedColumn.push(column);
    }
    this.dataSource = new MatTableDataSource(this.data);
    this.displayedColumns = columns;
    this.expandedColumn = expandedColumn;
    this.dataSource.sort = this.sort;
    this.expandedElement = this.data ? this.data[0] : null;
  }

}
