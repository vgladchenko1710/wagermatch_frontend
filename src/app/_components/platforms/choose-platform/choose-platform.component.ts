import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-choose-platform',
  templateUrl: './choose-platform.component.html',
  styleUrls: ['./choose-platform.component.scss']
})
export class ChoosePlatformComponent implements OnInit {

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<ChoosePlatformComponent>) { }

  ngOnInit() {
  }

  choosePlatform(newPlatform: string) {
    this.dialogRef.close(newPlatform);
  }

}
