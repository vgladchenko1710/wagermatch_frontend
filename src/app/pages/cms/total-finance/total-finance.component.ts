import { incomesColumns, payoutColumns } from './total-finance.model';
import { ToastService } from './../../../_services/toast.service';
import { ISettingsRates } from './../main-settings/main-settings.component';
import { UsersService } from './../../../_services/users.service';
import { Router } from '@angular/router';
import { AdminService } from './../_cms-services/admin.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { Subscription } from 'rxjs';
import { PayoutConfirmComponent } from './payout-confirm/payout-confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-total-finance',
  templateUrl: './total-finance.component.html',
  styleUrls: ['./total-finance.component.scss']
})
export class TotalFinanceComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public data: IAdminOutcome[] = [];
  private rates: ISettingsRates;
  public load: boolean;
  public totalRows = 0;
  public statuses = [
    {
      viewValue: 'На выплату', value: 'toPayout'
    },
    {
      viewValue: 'Завершенные выплаты', value: 'payout'
    },
    {
      viewValue: 'Пополнения', value: 'incomes'
    }
  ];
  public actualStatus = 'toPayout';
  public paginationData = {
    pageIndex: 0,
    pageSize: 30,
    orderBy: '',
    orderDirection: '',
    filterValue: '',
  };
  public columns = [...payoutColumns]

  constructor(public dialog: MatDialog,
              public router: Router,
              private authUserService: AuthUserService,
              private adminService: AdminService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.getRates();
  }

  getRates() {
    this.subscription.add(this.adminService.getRates()
      .subscribe((response: any) => {
        this.rates = response.result;
        this.getList();
      },  (err) => {
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  paginationUpdate(event) {
    switch (event.type) {
      case 'page':
        this.paginationData.pageIndex = event.data.pageIndex;
        this.paginationData.pageSize = event.data.pageSize;
        break;
      case 'sort':
        this.paginationData.orderBy = event.data.active;
        this.paginationData.orderDirection = event.data.direction;
        break;
      case 'filter':
        this.paginationData.filterValue = event.data;
        break;
    }
    this.getList();
  }

  statusUpdate(event) {
    this.actualStatus = event;
    if (this.actualStatus === 'incomes') {
      this.columns = [...incomesColumns]
    } else {
      this.columns = [...payoutColumns]
    }
    this.getList();
  }

  getList() {
    this.load = true;
    const rowsFrom = this.paginationData.pageSize * (this.paginationData.pageIndex + 1) - this.paginationData.pageSize + 1;
    const rowsTo = this.paginationData.pageSize * (this.paginationData.pageIndex + 1);
    this.subscription.add(this.adminService.getPaymentList(
      this.authUserService.getUserUid(),
      rowsFrom,
      rowsTo,
      this.paginationData.orderBy,
      this.paginationData.orderDirection,
      this.paginationData.filterValue,
      this.actualStatus)
      .subscribe ((response: {result: {list: IAdminOutcome[], totalRows: number}}) => {
        this.totalRows = response.result.totalRows;
        this.data = response.result.list.map(item=> this.amountConvertation(item));
        this.load = false;
    }));
  }

  amountConvertation(row: IAdminOutcome): IAdminOutcome {
    const newRow = {...row};
    const wagerRate = row.type?.includes('UAH') ? this.rates.hrnRate : this.rates.rubRate;
    const currency = row.type?.includes('UAH') ? 'грн' : 'руб.';
    newRow.toPay = (row.amount/wagerRate * this.rates.outcomeRate).toFixed(0).toString() + ` ${currency}`;
    return newRow;
  }

  openUser(event) {
    if (event.column === 'login') {
      this.router.navigate(['cms/user-profile', event.row.userUid]);
    } else if (this.actualStatus === 'toPayout') {
      const payoutDialog = this.dialog.open(PayoutConfirmComponent, {
        backdropClass: 'dialog-back',
        width: '600px',
        data: event.row
      });
      this.subscription.add(payoutDialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.getList();
        }
      }));
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}

export interface IAdminOutcome {
  amount: number;
  toPay?: string;
  date: string;
  description: string;
  email: string;
  login: string;
  payData: string;
  status: string;
  type: string;
  uid: string;
  userUid: string;
}
