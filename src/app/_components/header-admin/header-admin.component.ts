import { ToastService } from './../../_services/toast.service';
import { Router } from '@angular/router';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header-admin',
  templateUrl: './header-admin.component.html',
  styleUrls: ['./header-admin.component.scss']
})
export class HeaderAdminComponent implements OnInit {

  constructor(private authUserService: AuthUserService,
              public router: Router,
              public toastService: ToastService) { }

  ngOnInit(): void {
  }

  logout() {
    this.authUserService.logout();
    this.router.navigateByUrl('');
    this.toastService.success(`Миллионы всегда будут с нами!`);
  }

}
