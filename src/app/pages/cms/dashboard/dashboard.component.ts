import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminService } from './../_cms-services/admin.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public users;
  public matches;
  public conflicts;
  public profit;
  public statistic;
  public payments;

  constructor(public router: Router,
              private authUserService: AuthUserService,
              private adminService: AdminService) { }


  ngOnInit(): void {
    this.getInfo();
  }

  getInfo() {
    this.subscription.add( this.adminService.getDashboardInfo(this.authUserService.getUserUid())
      .subscribe ((response: any) => {
        const result = response.result;
        this.users = result.users;
        this.matches = result.matches;
        this.conflicts = result.confclits;
        this.profit = result.profit;
        this.statistic = result.statistics;
        this.payments = result.payments;
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
