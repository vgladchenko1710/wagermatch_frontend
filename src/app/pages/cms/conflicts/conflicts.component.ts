import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ConflictService } from './../_cms-services/conflict.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-conflicts',
  templateUrl: './conflicts.component.html',
  styleUrls: ['./conflicts.component.scss']
})
export class ConflictsComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public conflicts = [];
  public paginationData = {
    pageIndex: 0,
    pageSize: 10,
    orderBy: '',
    orderDirection: '',
    filterValue: '',
  };
  public totalRows = 0;
  public statuses = [
    {
      value: 'all', viewValue: 'Все'
    },
    {
      value: 'new', viewValue: 'Новые'
    },
    {
      value: 'resolved', viewValue: 'Решенные'
    }
  ];
  public status = 'new';
  public columns = [
    {
      fieldName: 'uid',
      title: 'Номер Спора',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'date',
      title: 'Дата',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'statusView',
      title: 'Статус',
      extraValue: 'status',
      useColumn: true
    },
    {
      fieldName: 'btn',
      title: 'Открыть матч',
      extraValue: 'btn',
      row: 1
    },
    {
      fieldName: 'owner',
      title: 'Создатель',
      extraValue: 'matchDetail',
      row: 1
    },
    {
      fieldName: 'btn2',
      title: 'Открыть конфликт',
      extraValue: 'btn2',
      row: 1
    },
    {
      fieldName: 'description',
      title: 'Описание',
      extraValue: 'useName',
      row: 2
    },
  ];

  constructor(private authUserService: AuthUserService,
              private conflictService: ConflictService,
              public router: Router) { }

  ngOnInit(): void {
    this.getList(this.status);
  }

  paginationUpdate(event) {
    switch (event.type) {
      case 'page':
        this.paginationData.pageIndex = event.data.pageIndex;
        this.paginationData.pageSize = event.data.pageSize;
        break;
    }
    this.getList(this.status);
  }

  getList(status) {
    const rowsFrom = this.paginationData.pageSize * (this.paginationData.pageIndex + 1) - this.paginationData.pageSize + 1;
    const rowsTo = this.paginationData.pageSize * (this.paginationData.pageIndex + 1);
    this.status = status;
    this.subscription.add(this.conflictService.getConflictsList(this.authUserService.getUserUid(), this.status, rowsFrom, rowsTo)
      .subscribe ((response: any) => {
        this.conflicts = response.result.list;
        this.totalRows = response.result.totalRows;
    }));
  }

  openMatch(conflict) {
    this.router.navigate(['matches/match', conflict.matchUid]);
  }

  openConflict(conflict) {
    this.router.navigate(['cms/conflict', conflict.uid]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
