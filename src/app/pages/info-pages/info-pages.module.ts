import { CommonModule } from '@angular/common';
import { HelpersComponent } from './help/helpers/helpers.component';
import { HelpComponent } from './help/help.component';
import { ComponentsModule } from './../../_components/components.module';
import { CustomMaterialModule } from './../../@theme/custom-material.module';
import { InfoPagesRoutingModule } from './info-pages-routing.module';
import { NgModule } from '@angular/core';
import { FaqComponent } from './faq/faq.component';
import { UserAgreementComponent } from './user-agreement/user-agreement.component';
import { ScreenExampleComponent } from './screen-example/screen-example.component';


@NgModule({
   declarations: [
      HelpComponent,
      HelpersComponent,
      FaqComponent,
      UserAgreementComponent,
      ScreenExampleComponent,
   ],
   imports: [
      InfoPagesRoutingModule,
      CustomMaterialModule,
      CommonModule,
      ComponentsModule,
   ],
})
export class InfoPagesModule {}
