import { NotificationTypeEnum } from './notification.model';
import { BonusService } from './../../_services/bonus.service';
import { ToastService } from './../../_services/toast.service';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { NotificationService } from './../../_services/notification-service';
import { fade } from './../../_animations/animations';
import { Router } from '@angular/router';
import { MatchesEventService } from './../../_services/events/matches-event.service';
import { CreateMatchService } from './../../_services/controllers/create-match.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { isMobile } from '../../_services/help-services/device-detection';

@Component({
  selector: 'app-header-notification',
  templateUrl: './header-notification.component.html',
  styleUrls: ['./header-notification.component.scss'],
  animations: [fade]
})
export class HeaderNotificationComponent implements OnInit, OnDestroy {
  @ViewChild('payButton', { static: false }) payButton: ElementRef;
  public subscription: Subscription = new Subscription ();
  public isMobile = isMobile();
  public userUid: string;
  public messages = {
    isShake: false,
    isNew: false,
    matchUid: '',
    messageUid: ''
  };

  public notification = {
    isShake: false,
    countUnread: 0,
    list: []
  };

  constructor(
    private authUserService: AuthUserService,
    public createMatchService: CreateMatchService,
    public toastService: ToastService,
    private matchesEventService: MatchesEventService,
    private router: Router,
    private bonusService: BonusService,
    private notificationService: NotificationService) {
      this.notificationService.connect();
      this.notificationService.subscribeOnAll();
      this.userUid = this.authUserService.getUserUid();
      // this.dateNow += this.userUid;
  }

  ngOnInit(): void {
    this.getTypingStatus();
    this.getMessage();
    this.getReadStatus();
    this.getMatchStatus();

    this.getNotifications();
    this.getMyMessageReadStatus();
    this.getNotificationMessage();
    this.mobileViewListener();
  }

  getTypingStatus() {
    this.notificationService.getTypingStatus$
    .subscribe((data) => {
      this.matchesEventService.sendTypingStatus(data);
    });
  }

  getReadStatus() {
    this.notificationService.getReadStatus$
    .subscribe((data) => {
      this.matchesEventService.sendReadStatus(data);
    });
  }

  getMatchStatus() {
    this.subscription.add(this.notificationService.getMatchStatus$
      .subscribe((matchStatus: any) => {
        this.matchesEventService.toUpdateMatch();
        this.matchesEventService.sendMatchStatus(matchStatus);
        if ( matchStatus.status === NotificationTypeEnum.DELETE) {
          this.router.navigate(['matches/all-matches']);
          this.matchesEventService.toUpdateBalance(0);
        } else if (matchStatus.status === NotificationTypeEnum.CHANGE_ENEMY) {
          this.matchesEventService.toUpdateBalance(0);
        }
        this.notification.isShake = true;
        setTimeout(() => {
          this.notification.isShake = false;
          this.getNotifications();
        }, 1000);
    }));
  }

  getMyMessageReadStatus() {
    this.matchesEventService.myMessReadStatus$
      .subscribe((data) => {
        this.messages.isNew = false;
        this.messages.isShake = false;
    });
  }

  getMessage() {
    this.subscription.add(this.notificationService.getMessages$
      .subscribe((messageData: any) => {
        this.matchesEventService.sendMessage(messageData);
        if (!messageData.message.isOwner) {
          this.messages.isShake = true;
          this.messages.messageUid = messageData.message.messageUid;
          this.messages.matchUid = messageData.matchUid;
          setTimeout(() => {
            this.messages.isNew = true;
            this.messages.isShake = false;
          }, 1000);
        }
    }));
  }

  getNotificationMessage() {
    this.subscription.add(this.notificationService.getNotificationMessage(this.userUid)
    .subscribe ((response: any) => {
      if (response.result.uidMatch) {
        this.messages.matchUid = response.result.uidMatch;
        this.messages.messageUid = response.result.messageUid;
        this.messages.isNew = true;
      }
    }));
  }

  openMatch(matchUid, messageUid?) {
    if (messageUid) {
      this.subscription.add(this.notificationService.changeReadStatus(this.userUid, messageUid)
      .subscribe ((response: any) => {
        this.notificationService.sendReadStatus(matchUid);
        this.messages.isNew = false;
        this.messages.isShake = false;
      }));
    }
    this.router.navigate(['matches/match', matchUid]);
  }

  getNotifications() {
    this.subscription.add(this.notificationService.getNotification(this.userUid)
    .subscribe ((response: any) => {
      this.notification.list = response.result.notification;
      this.notification.countUnread = response.result.countUnread;
    }));
  }

  readNotifications() {
    this.subscription.add(this.notificationService.readNotification(this.userUid)
    .subscribe ((response: any) => {
      this.getNotifications();
      }, (err) => {
        this.toastService.error(err.error.errors[0].message);
    }));
  }

  mobileViewListener() {
    document.addEventListener(
      "visibilitychange"
      , () => { if (!document.hidden) {
          this.matchesEventService.toUpdateMatch();
          this.matchesEventService.toUpdateMessages();
          if (this.isMobile) {
            this.notificationService.connect();
          }
        }
      }
    );
  }

  // Bonus system

  checkUserBonus() {
    this.bonusService.getBonusInfo()
      .subscribe((res: {message: string, result: boolean})=> {
        this.authUserService.setBonus(res?.result)
      })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }



}
