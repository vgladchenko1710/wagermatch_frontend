import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {
  public head = '';
  public infoText = '';
  public btn = '';
  public btnOrange = '';
  public closeTimer = false;

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<InfoComponent>) {
                this.head = data.head;
                this.infoText = data.infoText;
                this.btn = data.btn;
                this.btnOrange = data.btnOrange;
                this.closeTimer = data.closeTimer ? true : false;
              }
  ngOnInit(): void {
    if (this.closeTimer) {
      setTimeout(() => {
        this.dialogRef.close();
      }, 5000);
    }
  }

}
