import { PromoComponent } from './promo/promo.component';
import { LogsComponent } from './logs/logs.component';
import { MainSettingsComponent } from './main-settings/main-settings.component';
import { TotalFinanceComponent } from './total-finance/total-finance.component';
import { UsersComponent } from './users/users.component';
import { ConflictInfoComponent } from './conflict-info/conflict-info.component';
import { ConflictsComponent } from './conflicts/conflicts.component';
import { AdminGuard } from './../../_services/auth/admin.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserMatchesComponent } from './user-matches/user-matches.component';

// определение дочерних маршрутов
const routes: Routes = [
  {
    path: '',
    canActivate: [AdminGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent, pathMatch: 'full'},
      { path: 'conflicts', component: ConflictsComponent},
      { path: 'conflict/:conflictUid', component: ConflictInfoComponent},
      { path: 'users', component: UsersComponent},
      { path: 'finance', component: TotalFinanceComponent},
      { 
        path: 'promo',
        loadChildren: () => import('./promo/promo.module').then(m => m.PromoModule),
      },
      { path: 'logs', component: LogsComponent},
      {
        path: 'user-profile/:userUid',
        component: UserProfileComponent,
        loadChildren: () => import('./user-profile/user-profile.module').then(m => m.UserProfileModule)
      },
      {
        path: 'main-settings', component: MainSettingsComponent
      },
      {
        path: 'user-matches/:userUid', component: UserMatchesComponent
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full'}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule {}
