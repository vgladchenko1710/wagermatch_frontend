import { equalToFieldValue } from 'src/app/_services/errors/validators';
import { ToastService } from './../../../_services/toast.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { UsersService } from './../../../_services/users.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['../setting/setting.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  public isLoad = false;
  constructor(private usersService: UsersService,
              public toastService: ToastService,
              private authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm() {
    this.form = new FormGroup({
      password: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      passwordNew: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      passwordConfirm: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(50)])
    });
    this.subscription.add(this.form.get('passwordNew')
    .valueChanges
    .subscribe(() => {
      const control = this.form.get('passwordConfirm');
      control.setValidators([Validators.required, equalToFieldValue(this.form.get('passwordNew').value)]);
      control.updateValueAndValidity();
    }));
    this.subscription.add(this.form.get('passwordConfirm')
      .valueChanges
      .subscribe(() => {
        this.form.get('passwordConfirm').setValidators([Validators.required, equalToFieldValue(this.form.get('passwordNew').value)]);
      }));

  }
  savePassword() {
    this.isLoad = true;
    const uid = this.authUserService.getUserUid();
    const password = {
      passwordOld: this.form.get('password').value,
      passwordNew: this.form.get('passwordNew').value
    };
    this.subscription.add(this.usersService.changePassword(uid, password)
    .subscribe ((response: any) => {
      this.toastService.success(response.message);
      setTimeout(() => {
        window.location.href = window.location.protocol + '/my-profile';
      }, 1500);
      }, (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
