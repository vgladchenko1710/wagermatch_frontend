import { HttpInterceptor, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse, HttpHandler, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUserService } from './auth-user.service';
import { environment } from '../../../environments/environment';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authUserService: AuthUserService,
              private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const requestedURL = req.url.replace(environment.apiUrl, '');
    let copiedReq = req;
    if (requestedURL !== '/auth/login' && !requestedURL.includes('ipify')) {
      const headers = new HttpHeaders({
          'x-access-token': this.authUserService.getToken(),
          'uid': this.authUserService.getUserUid(),
      });
      copiedReq = req.clone({ headers});
    }
    return next.handle(copiedReq).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {}
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          this.authUserService.logout(true);
          this.router.navigate(['']);
        }
      }
    }));
  }
}
