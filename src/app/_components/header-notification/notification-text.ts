import { NotificationTypeEnum } from './notification.model';
export function getNotification(shortName: string, username: string) {
    const obj = {
        join: `Игрок ${username} присоеденился к матчу`,
        drop: `Игрок ${username} покинул матч`,
        changeEnemy: `Игрок ${username} отказался с тобой играть`,
        delete: `Игрок ${username} удалил матч к которому ты был присоеденен`,
        ready: `Игрок ${username} готов играть матч`,
        notReady: `Игрок ${username} не готов играть матч`,
        start: `Матч с игроком ${username} начался`,
        verification : `Игрок ${username} отправил результат матча`,
        conflict: `Возьник спор в матче с ${username}`,
        conflictResolve: `Вынесено арбитражное решение по спору с игроком ${username}`,
        timesUp: `Срок действия созданного тобою матча истек. Матч больше неактивен`,
        finished: `Матч с игроком ${username} завершен`
    };
    return obj[shortName];
};

