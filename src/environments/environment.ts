// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://api.wager-match.pro/api',
  socketUrl: 'https://sockets.wager-match.pro',
  // apiUrl: 'http://localhost:3020/api',
  // socketUrl: 'http://localhost:2020',
  frontend: 'http://localhost:4200',
  VERSION: '1.0.0',
  discordLink: 'https://discord.gg/N4AdGvMh7T'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
