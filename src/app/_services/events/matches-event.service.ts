import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MatchesEventService {
  public updateList$ = new Subject();
  public updateMatch$ = new Subject();
  public updateMessages$ = new Subject();
  public myMessReadStatus$ = new Subject();

  public getMessages$ = new Subject();
  public getTypingStatus$ = new Subject();
  public getReadStatus$ = new Subject();
  public getMatchStatus$ = new Subject();

  public getBalance$ = new Subject<number>();

  public toUpdateList() {
    this.updateList$.next();
  }

  public toUpdateMatch() {
    this.updateMatch$.next();
  }

  public toUpdateBalance(wagers: number) {
    this.getBalance$.next(wagers);
  }

  public toUpdateMessages() {
    this.updateMessages$.next();
  }

  public updateReadStatus() {
    this.myMessReadStatus$.next();
  }

  public sendMessage(data) {
    console.log('inMessage')
    this.getMessages$.next(data);
  }
  public sendTypingStatus(data) {
    this.getTypingStatus$.next(data);
  }
  public sendReadStatus(data) {
    this.getReadStatus$.next(data);
  }
  public sendMatchStatus(data) {
    this.getMatchStatus$.next(data);
  }
}
