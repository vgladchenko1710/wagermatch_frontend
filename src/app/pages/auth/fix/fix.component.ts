import { MatchesService } from './../../../_services/matches.service';
import { GetMatch } from './../../../_models/match';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-fix',
  templateUrl: './fix.component.html',
  styleUrls: ['./fix.component.scss']
})
export class FixComponent implements OnInit {
  public request: GetMatch = {
    page: 1,
    pageSize: 7,
    platform: 'all',
    bidFrom: 0,
    bidTo: 10000,
    sort: {isDateAsc: false, isBidAsc: false, isFirstData: true}
  };

  constructor(private matchesService: MatchesService,
              public router: Router) { }

  ngOnInit(): void {}

  reload() {
    window.location.reload();
  }


}
