import { GetMatch } from './../_models/match';
import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MatchesService {
  constructor(private httpClient: HttpClient) {}

createMatch(userUid: string, match: any) {
  return this.httpClient.post(`${environment.apiUrl}/matches/create-match/${userUid}`, match);
}

getMatchInfo(matchUid: string, userUid: string) {
  return this.httpClient.get(`${environment.apiUrl}/matches/match-info/${matchUid}?userUid=${userUid}`);
}

editMatch(userUid: string, matchUid: string, match: any) {
  return this.httpClient.put(`${environment.apiUrl}/matches/edit-match/${userUid}/${matchUid}`, match);
}

joinToMatch(userUid: string, matchUid: string) {
  return this.httpClient.post(`${environment.apiUrl}/matches/join-match/${userUid}/${matchUid}`, {});
}

readyToMatch(userUid: string, matchUid: string, isReady: any) {
  return this.httpClient.post(`${environment.apiUrl}/matches/ready/${userUid}/${matchUid}`, isReady);
}

startMatch(userUid: string, matchUid: string) {
  return this.httpClient.post(`${environment.apiUrl}/matches/start/${userUid}/${matchUid}`, {});
}

dropFropMatch(userUid: string, matchUid: string) {
  return this.httpClient.post(`${environment.apiUrl}/matches/drop-from-match/${userUid}/${matchUid}`, {});
}

changeEnemy(userUid: string, matchUid: string) {
  return this.httpClient.post(`${environment.apiUrl}/matches/change-enemy/${userUid}/${matchUid}`, {});
}

deleteMatch(userUid: string, matchUid: any) {
  return this.httpClient.delete(`${environment.apiUrl}/matches/delete-match/${userUid}/${matchUid}`);
}

getMatchlist(request: GetMatch, userUid ?: any) {
  const finaluserUid = userUid ? userUid : '';
  return this.httpClient.get(`${environment.apiUrl}/cmsPublic/matchList?userUid=${finaluserUid}&page=${request.page}&pageSize=${request.pageSize}&isDateAsc=${request.sort.isDateAsc}&isBidAsc=${request.sort.isBidAsc}&isFirstDate=${request.sort.isFirstData}&platform=${request.platform}&bidFrom=${request.bidFrom}&bidTo=${request.bidTo}`);
}

sendResult(userUid: string, matchUid: string, data: any) {
  return this.httpClient.post(`${environment.apiUrl}/matches/result/${userUid}/${matchUid}`, data);
}
getMyMatches(userUid: string, page: number, pageSize: number, isDateAsc: boolean, isBidAsc: boolean, isFirstDate: boolean, status: string) {
  return this.httpClient.get(`${environment.apiUrl}/matches/my-matches/${userUid}?page=${page}&pageSize=${pageSize}&isDateAsc=${isDateAsc}&isBidAsc=${isBidAsc}&isFirstDate=${isFirstDate}&status=${status}`)
}

}
