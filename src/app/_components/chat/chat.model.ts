export class IMessage {
    message: string;
    date: Date;
    isOwner: boolean;
    isRead: boolean;
    messageUid?: string;
    lastRead?: boolean;
}