import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { RechargeComponent } from './recharge/recharge.component';
import { Balanse } from '../../../_models/my-profile-interface';
import { ToastService } from './../../../_services/toast.service';
import { Subscription } from 'rxjs';
import { UsersService } from './../../../_services/users.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})

export class AccountComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private uid = '';
  public balanse: Balanse = {
    actual: 0,
    toPay: 0,
    inMatches: 0
  };
  actualMatches = 0;
  actualWinMatches = 0;
  actualLoseMatches = 0;
  allMatches = 0;
  wins = 0;
  lose = 0;

  constructor(private authUserService: AuthUserService,
              private route: ActivatedRoute,
              public toastService: ToastService,
              public dialog: MatDialog,
              private usersService: UsersService) { }

  ngOnInit(): void {
    this.uid = this.authUserService.getUserUid();
    this.subscription.add(this.route.queryParamMap
      .subscribe((params) => {
        if (params.get('recharge')) {
          this.recharge();
        }
    }));
    this.getAccountInfo();
  }

  getAccountInfo() {
    this.subscription.add(this.usersService.getUserAccountInfo(this.uid)
    .subscribe((response: any) => {
      this.balanse = {
        actual: response.result.bank,
        toPay: response.result.toPay,
        inMatches: response.result.inTheGame
      };
      this.allMatches = response.result.matchesPlayd;
      this.wins = response.result.winGames;
      this.lose = response.result.loseGames;
      const timespeed = 15000 / this.allMatches > 200 ? 200 : 150000 / this.allMatches;
      this.matchesTimeout(timespeed);
    },  (err) => {
      this.toastService.error(err.error.errors[0].message);
    }
  ));
  }


  matchesTimeout(timeSpeed) {
    setTimeout(() => {
      this.allMatches > this.actualMatches ? this.actualMatches++ : null
      this.wins > this.actualWinMatches ?  this.actualWinMatches++ : null;
      this.lose > this.actualLoseMatches ?  this.actualLoseMatches++ : null;
      timeSpeed /= 1.15;
      if (this.actualMatches < this.allMatches) {
        this.matchesTimeout(timeSpeed);
      }
    }, timeSpeed);
  }

  recharge() {
    const rechargeDialog = this.dialog.open(RechargeComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
    this.subscription.add(rechargeDialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.getAccountInfo();
        }
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
