export class Pagination {
    constructor(itemsPerPage?: number) {
        if (itemsPerPage) {
            this.pageSize = itemsPerPage;
        }
    }

    pageIndex = 0;
    pageSize =  30;
    totalItems = 0;
    orderBy = '';
    orderDirection: SortFilterPaginationRequest['orderDirection'] = 'desc';
    filterValue = '';

    newFormatRequest(): SortFilterPaginationRequest {
        return {
            currentPage: (this.pageIndex+1).toString(),
            itemsPerPage: this.pageSize.toString(),
            orderBy: this.orderBy,
            orderDirection: this.orderDirection,
            filter: this.filterValue
        }
    }
  
}

export interface SortFilterPaginationRequest {
    currentPage: string;
    itemsPerPage: string;
    orderBy: string;
    orderDirection: 'asc' | 'desc';
    filter: string;
}