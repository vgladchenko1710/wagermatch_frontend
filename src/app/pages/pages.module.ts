import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from './../_shared/shared.module';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from './../@theme/custom-material.module';
import { NgModule } from '@angular/core';
import { MainPageComponent } from './main-page/main-page.component';
import { PagesRoutingModule } from './pages-routing.module';



@NgModule({
   declarations: [
      MainPageComponent,
   ],
   imports: [
      CommonModule,
      PagesRoutingModule,
      CustomMaterialModule,
      FontAwesomeModule,
      SharedModule
   ],
   providers: []
})
export class PagesModule { }
