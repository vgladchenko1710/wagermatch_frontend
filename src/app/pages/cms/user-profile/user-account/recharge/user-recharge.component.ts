import { AdminService } from './../../../_cms-services/admin.service';
import { ToastService } from './../../../../../_services/toast.service';
import { AuthUserService } from './../../../../../_services/auth/auth-user.service';
import { Subscription } from 'rxjs';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';

@Component({
  selector: 'app-user-recharge',
  templateUrl: './user-recharge.component.html',
  styleUrls: ['./user-recharge.component.scss']
})
export class UserRechargeComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  public minRecharge = -1000000;
  public maxRecharge = 1000000;
  public payDescription: '';
  public isLoad: boolean;
  public userUid;
  public descriptions = [
    'Корректировка баланса.',
    'Снятие средств за нарушение.',
    'Награда за участие в бонусном матче.'
  ];

  constructor(public dialogRef: MatDialogRef<UserRechargeComponent>,
              @Inject (MAT_DIALOG_DATA) public data: any,
              public toastService: ToastService,
              public authUserService: AuthUserService,
              private adminService: AdminService) {
                this.minRecharge = 0 - (+this.data.balanse);
              }

  ngOnInit(): void {
    this.userUid = this.data.userUid;
    this.form = new FormGroup({
      amount: new FormControl(0, [Validators.required,  Validators.max(this.maxRecharge), Validators.min(this.minRecharge)]),
      description: new FormControl('', [Validators.required]),
    });
  }

  plus() {
    const value = this.form.get('amount').value;
    const newValue = value + 100 <= this.maxRecharge ? value + 100 : this.maxRecharge;
    this.form.patchValue({amount: newValue});
  }

  minus() {
    const value = this.form.get('amount').value;
    const newValue = value - 100 >= this.minRecharge ? value - 100 : this.minRecharge;
    this.form.patchValue({amount: newValue});
  }

  toRecharge() {
    this.isLoad = true;
    const data = {
      description : '[Admin] ' + this.form.get('description').value,
      amount : this.form.get('amount').value
    };
    this.subscription.add(this.adminService.changeBalanse(this.userUid, data)
      .subscribe ((response: any) => {
          this.toastService.success(response.message);
          setTimeout(() => {
            this.dialogRef.close(true);
          }, 600);
        }, (err) => {
          this.toastService.error(err.error.errors[0].message);
          this.isLoad = false;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
