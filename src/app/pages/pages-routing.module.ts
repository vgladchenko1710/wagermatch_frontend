import { NoAdminGuard } from './../_services/auth/noAdmin.guard';
import { ApiGuard } from './../_services/controllers/api.guard';
import { NgModule } from '@angular/core';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { MainPageComponent } from './main-page/main-page.component';
import { Routes, RouterModule } from '@angular/router';
import { CmsComponent } from './cms/cms.component';


export const routes: Routes = [
  {
    path: '',
    canActivate: [ApiGuard],
    children: [
      {
        path: '',
        component: MainPageComponent,
        canActivate: [ApiGuard, NoAdminGuard]
      },
      {
        path: 'matches',
        loadChildren: () => import('./matches/matches.module').then(m => m.MatchesModule)
      },
      {
        path: 'info',
        loadChildren: () => import('./info-pages/info-pages.module').then(m => m.InfoPagesModule)
      },
      {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
      },
      {
        path: 'my-profile',
        component: MyProfileComponent,
        canActivate: [ApiGuard],
        loadChildren: () => import('./my-profile/my-profile.module').then(m => m.MyProfileModule)
      },
      {
        path: 'cms',
        component: CmsComponent,
        canActivate: [ApiGuard],
        loadChildren: () => import('./cms/cms.module').then(m => m.CmsModule)
      },
      // {path: '**', component: NotFoundComponent}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }