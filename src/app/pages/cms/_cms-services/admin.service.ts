import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { environment } from '../../../../environments/environment';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  constructor(private httpClient: HttpClient,
              private authUserService: AuthUserService) {}

  get adminUid() {
    return this.authUserService.getUserUid();
  }

  getRates() {
    return this.httpClient.get(`${environment.apiUrl}/admin/${this.adminUid}/settings-rate`);
  }

  updateRates(adminUid: string, data: any) {
    return this.httpClient.put(`${environment.apiUrl}/admin/${adminUid}/settings-rate`, data);
  }

  getPaymentList(adminUid: string, rowsFrom?: number, rowsTo? :number, orderBy?: string, orderDirection?: string, filterValue?: string, status?: string) {
    return this.httpClient.get(`${environment.apiUrl}/admin/${adminUid}/outcomes?rowsFrom=${rowsFrom}&rowsTo=${rowsTo}&orderBy=${orderBy}&orderDirection=${orderDirection}&filterValue=${filterValue}&status=${status}`);
  }

  changePaymentStatus(adminUid: string, data: any) {
    return this.httpClient.put(`${environment.apiUrl}/admin/${adminUid}/outcomes`, data);
  }

  getDashboardInfo(adminUid: string) {
    return this.httpClient.get(`${environment.apiUrl}/admin/dashboard/${adminUid}`);
  }

  changeBalanse(userUid: string, data) {
    return this.httpClient.post(`${environment.apiUrl}/admin/admin-recharge/${userUid}`, data);
  }

  cleanMatchesAfterTimeout() {
    return this.httpClient.get(`${environment.apiUrl}/worker/matches-timeup`);
  }

  finishMatchesAfterVerificationTimeout() {
    return this.httpClient.get(`${environment.apiUrl}/worker/matches-end-match`);
  }

  getLogs(adminUid: string, rowsFrom: number, rowsTo: number, orderBy: string, orderDirection: string, filterValue: string) {
    return this.httpClient.get(`${environment.apiUrl}/admin/finance-log/${adminUid}?rowsFrom=${rowsFrom}&rowsTo=${rowsTo}&orderBy=${orderBy}&orderDirection=${orderDirection}&filterValue=${filterValue}`);
  }

}
