const { exec } = require('child_process');

let command;
if (process.env.ENV === 'prod') {
    console.log('Start prod script');
    command = exec("ng build --output-path angularapp  --aot --configuration='production'");
} else if (process.env.ENV === 'test') {
    console.log('Start script for testing');
    command = exec("ng build --output-path angularapp  --aot --configuration='testing'");
}

if (command != undefined) {
    command.stdout.on('data', (data) => {
        console.log(data);
    });

    command.stderr.on('data', (data) => {
        console.error(data);
    });

    command.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
} else {
    console.error('process.env.ENV: ' + process.env.ENV);
}
