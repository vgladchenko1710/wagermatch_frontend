import { LoginComponent } from './../../../_dialogs/login/login.component';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { AuthService } from './../../../_services/auth/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmed-email',
  templateUrl: './confirmed-email.component.html',
  styleUrls: ['./confirmed-email.component.scss']
})
export class ConfirmedEmailComponent implements OnInit {
  private key: string;
  private userLogin: string;
  public error = false;
  public success = false;

  constructor(public router: Router,
              private dialog: MatDialog,
              public authService: AuthService,
              public authUserService: AuthUserService,
              private route: ActivatedRoute) {
                if (this.authUserService.isAuthenticated()) {
                  this.authUserService.logout(true);
                }
              }

  ngOnInit(): void {
    this.route.paramMap
    .subscribe((params: ParamMap) => {
      this.key =  params.get('key');
      this.emailVerify();
    });
    this.userLogin = this.route.snapshot.queryParams['login'];
  }

  emailVerify() {
    this.authService.emailVerify({uuid: this.key})
      .subscribe(response => {
        this.success = true;
        this.error = false;
        this.redirect();
      }, (err) => {
        if (err.error.errors[0].message.includes('Mail already verified')) {
          this.error = true;
          this.success = false;
        } else {
          this.router.navigate([this.key]);
        }
    });
  }

  openLogin() {
    this.dialog.open(LoginComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {params: {login: this.userLogin}}
    });
  }

  redirect() {
    setTimeout(() => {
      this.openLogin();
      this.router.navigate(['/']);
    }, 2000);
  }

}
