import { IPromo } from './../../pages/cms/promo/promo.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-promo-info',
  templateUrl: './promo-info.component.html',
  styleUrls: ['./promo-info.component.scss']
})
export class PromoInfoComponent {

  constructor(@Inject (MAT_DIALOG_DATA) public promo: IPromo,
              public dialogRef: MatDialogRef<PromoInfoComponent>) {}

}
