import { UserRechargeComponent } from './recharge/user-recharge.component';
import { UsersService } from './../../../../_services/users.service';
import { ToastService } from './../../../../_services/toast.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { Balanse } from './../../../../_models/my-profile-interface';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})

export class UserAccountComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  public uid = '';
  public balanse: Balanse = new Balanse();
  actualMatches = 0;
  actualWinMatches = 0;
  actualLoseMatches = 0;
  allMatches = 0;
  wins = 0;
  lose = 0;

  constructor(private authUserService: AuthUserService,
              private route: ActivatedRoute,
              public toastService: ToastService,
              public dialog: MatDialog,
              private usersService: UsersService) { }

  ngOnInit(): void {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.uid = params.get('userUid');
        this.getAccountInfo();
    }));
  }

  getAccountInfo() {
    this.subscription.add(this.usersService.getUserAccountInfo(this.uid)
    .subscribe((response: any) => {
      this.balanse = {
        actual: response.result.bank,
        toPay: response.result.toPay,
        inMatches: response.result.inTheGame
      };
      this.allMatches = response.result.matchesPlayd;
      this.wins = response.result.winGames;
      this.lose = response.result.loseGames;
      const timespeed = 15000 / this.allMatches > 200 ? 200 : 15000 / this.allMatches;
      this.matchesTimeout(timespeed);
    },  (err) => {
      this.toastService.error(err.error.errors[0].message);
    }
  ));
  }


  matchesTimeout(timeSpeed) {
    setTimeout(() => {
      this.allMatches > this.actualMatches ? this.actualMatches++ : null
      this.wins > this.actualWinMatches ?  this.actualWinMatches++ : null;
      this.lose > this.actualLoseMatches ?  this.actualLoseMatches++ : null;
      timeSpeed /= 1.2;
      if (this.actualMatches < this.allMatches) {
        this.matchesTimeout(timeSpeed);
      }
    }, timeSpeed);
  }

  recharge() {
    const rechargeDialog = this.dialog.open(UserRechargeComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '500px',
      data: {userUid : this.uid, balanse: this.balanse.actual}
    });
    this.subscription.add(rechargeDialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.getAccountInfo();
        }
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
