import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { AdminService } from './../_cms-services/admin.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public isLoading: boolean;
  public totalRows = 0;
  public data = [];
  public paginationData = {
    pageIndex: 0,
    pageSize: 30,
    orderBy: '',
    orderDirection: '',
    filterValue: '',
  };
  public columns = [
    {
      fieldName: 'userLogin',
      title: 'Логин',
    },
    {
      fieldName: 'email',
      title: 'Email',
    },
    {
      fieldName: 'inTheBank',
      title: 'На счету',
    },
    {
      fieldName: 'inTheGame',
      title: 'В доигрыше',
    },
    {
      fieldName: 'toPay',
      title: 'На выплату',
    },
    {
      fieldName: 'totalBank',
      title: 'Общий баланс',
    },
    {
      fieldName: 'description',
      title: 'Описание',
    },
    {
      fieldName: 'date',
      title: 'Дата тран-и',
      extraValue: 'date'
    }
  ];

  constructor(private router: Router,
              private adminService: AdminService,
              private authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.isLoading = true;
    const rowsFrom = this.paginationData.pageSize * (this.paginationData.pageIndex + 1) - this.paginationData.pageSize + 1;
    const rowsTo = this.paginationData.pageSize * (this.paginationData.pageIndex + 1);
    this.subscription.add(this.adminService.getLogs(
      this.authUserService.getUserUid(),
      rowsFrom,
      rowsTo,
      this.paginationData.orderBy,
      this.paginationData.orderDirection,
      this.paginationData.filterValue)
      .subscribe ((response: any) => {
        this.totalRows = response.result.length;
        this.data = response.result.list;
        this.isLoading = false;
    }));
  }

  paginationUpdate(event) {
    switch (event.type) {
      case 'page':
        this.paginationData.pageIndex = event.data.pageIndex;
        this.paginationData.pageSize = event.data.pageSize;
        break;
      case 'sort':
        this.paginationData.orderBy = event.data.active;
        this.paginationData.orderDirection = event.data.direction;
        break;
      case 'filter':
        this.paginationData.filterValue = event.data;
        break;
    }
    this.getList();
  }

  openUser(event) {
    this.router.navigate(['cms/user-profile', event.row.userUid]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
