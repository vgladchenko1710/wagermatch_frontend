import { Injectable } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WindowsSizeService {
  public mobileSize = 992;
  public isMobile$ = new BehaviorSubject(window.innerWidth < this.mobileSize);

  constructor( public breakpointObserver: BreakpointObserver) {}

  listen() {
    this.breakpointObserver
      .observe([`(max-width: ${this.mobileSize}px)`])
      .subscribe((state: BreakpointState) => {
        this.isMobile$.next(state.breakpoints[`(max-width: ${this.mobileSize}px)`]);
    });
  }




}
