import { RechargeComponent } from './../account/recharge/recharge.component';
import { fade } from './../../../_animations/animations';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from './../../../_services/users.service';
import { Subscription } from 'rxjs';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { ToastService } from './../../../_services/toast.service';
import { MatDialog } from '@angular/material/dialog';
import { PayoutComponent } from './payout/payout.component';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.scss'],
  animations: [fade]
})
export class FinanceComponent implements OnInit, OnDestroy {
  public actualTab = 0;
  public incomes = [];
  public payout = [];
  public toShowIncomes;
  public toShowPayouts;
  private subscription: Subscription = new Subscription();
  public loading = false;
  public activePageIncomes = 1;
  public activePagePayouts = 1;
  public toShow = 8;
  public columnsMobile = [
    {
      fieldName: 'date',
      title: 'Дата',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'status',
      title: 'Статус',
      extraValue: 'payStatus',
      useColumn: true
    },
    {
      fieldName: 'amount',
      title: 'Сумма',
      extraValue: 'amount',
      useColumn: true
    },
    {
      fieldName: 'description',
      title: 'Описание',
      extraValue: 'useName',
      row: 1
    },
  ];

  constructor(private dialog: MatDialog,
              private authUserService: AuthUserService,
              private activatedRoute: ActivatedRoute,
              private usersService: UsersService,
              private route: ActivatedRoute,
              public toastService: ToastService) {
                this.subscription.add(this.route.queryParamMap
                  .subscribe((params) => {
                    if (params.get('payout')) {
                      this.openPayout();
                    }
                }));
              }

  ngOnInit(): void {
    this.getTransaktionsList();
  }

  getTransaktionsList() {
    const uid = this.authUserService.getUserUid();
    this.subscription.add(this.usersService.getTransaktionsList(uid)
      .subscribe((resultList: any) => {
        this.payout = resultList.result.listOfOutComes;
        this.incomes = resultList.result.listOfInComes;
        this.toShowIncomes = this.incomes.slice(0, this.toShow);
        this.toShowPayouts = this.payout.slice(0, this.toShow);
    }));
  }

  onChangePage(isPayouts) {
    if (isPayouts) {
      let maxValue = this.toShow * this.activePagePayouts;
      this.toShowPayouts = this.payout.slice((maxValue - this.toShow), maxValue);
    } else {
      let maxValue = this.toShow * this.activePageIncomes;
      this.toShowIncomes = this.incomes.slice((maxValue - this.toShow), maxValue);
    }
  }

  openPayout() {
    const payoutDialog = this.dialog.open(PayoutComponent, {
      backdropClass: 'dialog-back',
      width: '490px',
      disableClose: true,
      data: {}
    });
    this.subscription.add(payoutDialog.afterClosed()
    .subscribe(result => {
      if (result) {
        this.getTransaktionsList();
      }
    }));
  }

  recharge() {
    const rechargeDialog = this.dialog.open(RechargeComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
    this.subscription.add(rechargeDialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.getTransaktionsList();
        }
    }));
  }



  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
