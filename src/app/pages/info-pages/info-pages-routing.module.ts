import { UserAgreementComponent } from './user-agreement/user-agreement.component';
import { ScreenExampleComponent } from './screen-example/screen-example.component';
import { HelpComponent } from './help/help.component';
import { FaqComponent } from './faq/faq.component';
import { HelpersComponent } from './help/helpers/helpers.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



export const infoRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'help', 
        component: HelpComponent,
      },
      {
        path: 'helper/:helperName', 
        component: HelpersComponent
      },
      {
        path: 'faq', 
        component: FaqComponent,
      },
      { 
        path: 'screen-example', 
        component: ScreenExampleComponent,
      },
      { path: 'user-agreement', 
        component: UserAgreementComponent,
      },
      { path: '', redirectTo: 'help', pathMatch: 'full'},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(infoRoutes)],
  exports: [RouterModule]
})
export class InfoPagesRoutingModule { }
