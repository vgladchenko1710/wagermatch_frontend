import { CustomMaterialModule } from 'src/app/@theme/custom-material.module';
import { PayoutComponent } from './../../my-profile/finance/payout/payout.component';
import { ComponentsModule } from './../../../_components/components.module';
import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserRechargeComponent } from './user-account/recharge/user-recharge.component';
import { UserPayoutComponent } from './user-finance/payout/user-payout.component';
import { UserArbitrazComponent } from './user-arbitraz/user-arbitraz.component';
import { UserFinanceComponent } from './user-finance/user-finance.component';
import { UserChangePasswordComponent } from './user-change-password/user-change-password.component';
import { UserProfileMenuComponent } from './user-profile-menu/user-profile-menu.component';
import { UserSettingComponent } from './user-setting/user-setting.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { UserProfileComponent } from './user-profile.component';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CreditCardDirectivesModule } from 'angular-cc-library';

@NgModule({
  declarations: [
    UserProfileComponent,
    UserAccountComponent,
    UserSettingComponent,
    UserProfileMenuComponent,
    UserChangePasswordComponent,
    UserFinanceComponent,
    UserArbitrazComponent,
    UserPayoutComponent,
    UserRechargeComponent
  ],
  imports: [
    CustomMaterialModule,
    ComponentsModule,
    CommonModule,
    NgbPaginationModule,
    UserProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RoundProgressModule,
    CreditCardDirectivesModule
  ],
  providers: [
  ],
  entryComponents: [
    PayoutComponent
  ]
})
export class UserProfileModule {}
