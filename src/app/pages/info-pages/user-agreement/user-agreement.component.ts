import { element } from 'protractor';
import { violations } from './../help/violation-text';
import { Title, Meta } from '@angular/platform-browser';
import { environment } from './../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { helpList } from '../help/help-text';

@Component({
  selector: 'app-user-agreement',
  templateUrl: './user-agreement.component.html',
  styleUrls: ['./user-agreement.component.scss']
})
export class UserAgreementComponent implements OnInit {
  @ViewChild('section', { static: false }) section: ElementRef;
  @ViewChild('returnPolicy', { static: false }) returnPolicy: ElementRef;
  @ViewChild('personalData', { static: false }) personalData: ElementRef;
  @ViewChild('gameRules', { static: false }) gameRules: ElementRef;
  public rulesList = helpList();
  public violationsList = violations;
  public contactEmail = 'contact@wager-match.pro';
  public frontLink = environment.frontend;
  public fullLink;
  public scrollBtn = '';

  constructor(private route: ActivatedRoute,
              private titleService: Title,
              private meta: Meta) {
    this.fullLink = environment.production ? 'https://' + this.frontLink : this.frontLink;
    this.titleService.setTitle(`Пользовательское соглашение`+' ‣Wagermatch');
    this.meta.updateTag({name: 'description', content: 'Пользовательское соглашение. Сервис Wagermatch является гарантом, позволяющим пользователям заключать пари в FIFA 21 при помощи виртуальной валюты.'})
   }

  ngOnInit(): void {
    this.route.fragment.subscribe(fragment => { fragment && this.toAnchor(fragment) });
  }

  toAnchor(fragment) {

    let element = 0;
    if (fragment === 'returnPolicy') {
      element = this.returnPolicy?.nativeElement?.offsetTop
    }
    if (fragment === 'personalData') {
      element = this.personalData?.nativeElement?.offsetTop
    }
    if (fragment === 'gameRules') {
      element = this.gameRules?.nativeElement?.offsetTop
    }
    setTimeout(() => {
      element > 0 ? window.scroll(0, element) : this.toAnchor(fragment);
    }, 200)
  }

  toScroll() {
    window.scroll({
      top: this.scrollBtn.includes('bottom') ? document.body.offsetHeight : 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
    const limit = document.body.offsetHeight - window.innerHeight;
    const rate = window.pageYOffset /limit;
    if (rate < 0.01 || rate > 0.98) {
      this.scrollBtn = '';
    } else if (rate > 0.01 && rate <0.5) {
      this.scrollBtn = 'bottom';
    } else {
      this.scrollBtn = 'top'
    }
  }

}
