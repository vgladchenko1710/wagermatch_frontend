export function createSendInterkassaRechargeForm(payData: InterkassaData) {
    const header = document.querySelector('#header-notification');
    const template = `
      <form name="payment" method="post" action="https://sci.interkassa.com/" accept-charset="UTF-8" style="visibility: hidden;">
        <input type="hidden" name="ik_co_id" value="601e5a99abdc1b56ca5102af"/>
        <input type="hidden" name="ik_pm_no" value="${payData.paymentId}"/>
        <input type="hidden" name="ik_am" value="${payData.amount}"/>
        <input type="hidden" name="ik_desc" value="Покупка вагеров на Wagermatch"/>
        <input type="hidden" name="ik_cur" value="${payData.currency}"/>
        <input type="submit" value="Pay" id="recharge-button">
      </form>`
    const div = document.createElement('div');
    div.innerHTML = template;
    header.appendChild(div);
    (header.querySelector('#recharge-button') as HTMLElement).click();
}

export interface InterkassaData {
    userUid: string,
    paymentId: string,
    amount: number,
    currency: 'RUB' | 'UAH'
    paymentType: string,
    code?: string
  }