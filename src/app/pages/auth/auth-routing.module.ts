import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { FixComponent } from './fix/fix.component';
import { VkAuthComponent } from './vk-auth/vk-auth.component';
import { NewPasswordComponent } from './../../_dialogs/login/new-password/new-password.component';
import { ConfirmedEmailComponent } from './confirmed-email/confirmed-email.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'confirm-email/:key',
                component: ConfirmedEmailComponent,
            },
            {
                path: 'new-password/:key',
                component: NewPasswordComponent,
            },
            {
                path: 'payment-confirmation',
                component: PaymentConfirmComponent,
            },
            {
                path: 'vk',
                component: VkAuthComponent,
            },
            {
                path: 'fix',
                component: FixComponent,
            },
            { path: '', redirectTo: 'not-found', pathMatch: 'full'},
            { path: 'auth', redirectTo: 'not-found', pathMatch: 'full'}
        ]
    }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }