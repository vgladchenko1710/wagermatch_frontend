import { RechargeComponent } from './../../pages/my-profile/account/recharge/recharge.component';
import { NotificationService } from './../../_services/notification-service';
import { GuardInfoComponent } from '../guard-info/guard-info.component';
import { MatchesEventService } from '../../_services/events/matches-event.service';
import { InfoComponent } from './../info/info.component';
import { Subscription } from 'rxjs';
import { ToastService } from '../../_services/toast.service';
import { MatchesService } from '../../_services/matches.service';
import { Router } from '@angular/router';
import { AuthUserService } from '../../_services/auth/auth-user.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-join-match',
  templateUrl: './join-match.component.html',
  styleUrls: ['./join-match.component.scss']
})
export class JoinMatchComponent implements OnInit, OnDestroy {
  public subscription: Subscription = new Subscription ();
  public matchUid = '';
  public isLoad = false;

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              private authUserService: AuthUserService,
              private dialog: MatDialog,
              public router: Router,
              public matchesService: MatchesService,
              public notificationService: NotificationService,
              public matchesEventService: MatchesEventService,
              public toastService: ToastService,
              public dialogRef: MatDialogRef<JoinMatchComponent>) {
                this.matchUid = data.matchUid;
                if (data.withAlly) {
                  setTimeout(() => {
                    this.openExtraInfo();
                  }, 700);
                }
              }

  ngOnInit(): void {
    console.log(this.data)
  }

  toJoinMatch() {
    this.isLoad = true;
    const userUid = this.authUserService.getUserUid();
    this.subscription.add(this.matchesService.joinToMatch(userUid, this.matchUid)
      .subscribe ((response: any) => {
        this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: 'joined'});
        this.matchesEventService.toUpdateBalance(0);
        this.matchesEventService.toUpdateMatch();
        this.toastService.success(response.message);
        setTimeout(() => {
          this.dialogRef.close();
          this.isLoad = false;
        }, 500);
        }, (err) => {
          this.isLoad = false;
          const error = err.error.errors[0].message;
          if (error.includes('Пополни кошелёк')) {
            this.balanceInfo();
          } else {
            this.toastService.error(error);
            this.matchesEventService.toUpdateMatch();
            setTimeout(() => {
              this.dialogRef.close();
            }, 500);
          }
    }));
  }

  openExtraInfo() {
    this.dialog.open(GuardInfoComponent, {
      backdropClass: 'dialog-back',
      width: '500px',
    });
  }

  balanceInfo() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Недостаточный баланс для покрытия суммы пари!',
              infoText: 'Пополни свой баланс или выбери другой матч со списка',
              btn: 'Выбрать другой матч',
              btnOrange: 'Пополнить баланс'
      },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        if (result === 'btnOrange') {
          this.recharge();
        } else {
          this.router.navigate(['matches/all-matches']);
          this.dialogRef.close();
        }
      }));
  }

  recharge() {
    this.dialog.open(RechargeComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '500px',
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
