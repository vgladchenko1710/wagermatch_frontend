import { LogsService, ISourceLogBody, LogActionEnum } from './_services/logs.service';
import { DOCUMENT, isPlatformBrowser, Location } from "@angular/common";
import { isEmptyObject } from "src/app/_shared/common";
import {
  AfterViewInit,
  APP_ID,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from "@angular/router";
import { Subject, Subscription } from "rxjs";
import { RechargeComponent } from "./pages/my-profile/account/recharge/recharge.component";
import { PayoutComponent } from "./pages/my-profile/finance/payout/payout.component";
import { fade } from "./_animations/animations";
import { InfoComponent } from "./_dialogs/info/info.component";
import { LoginComponent } from "./_dialogs/login/login.component";
import { RegisterComponent } from "./_dialogs/register/register.component";
import { AuthUserService } from "./_services/auth/auth-user.service";
import { BonusService } from "./_services/bonus.service";
import { ApiConnectService } from "./_services/controllers/api-connect.service.";
import { CreateMatchService } from "./_services/controllers/create-match.service";
import { WindowsSizeService } from "./_services/help-services/window-size.service";
import { NotificationService } from "./_services/notification-service";
import { ToastService } from "./_services/toast.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [fade],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("sidenav", { static: false }) sidenav: any;
  public subscription: Subscription = new Subscription();
  public isLoading = false;
  public isMobile$ = new Subject();
  public isFirstLoading = true;
  public isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  public isConnection = true;
  public login = "";

  constructor(
    private dialog: MatDialog,
    public createMatchService: CreateMatchService,
    public router: Router,
    public location: Location,
    public toastService: ToastService,
    private notificationService: NotificationService,
    private bonusService: BonusService,
    public apiConnectService: ApiConnectService,
    private route: ActivatedRoute,
    private logsService: LogsService,
    public windowsSizeService: WindowsSizeService,
    @Inject(DOCUMENT) private document: Document,
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string,
    private authUserService: AuthUserService
  ) {
    this.login = this.authUserService.getLogin();
  }

  routeEvent(router: Router) {
    router.events.subscribe((evt) => {
      if (evt instanceof NavigationStart) {
        this.isLoading = true;
      } else if (
        evt instanceof NavigationCancel ||
        evt instanceof NavigationError ||
        evt instanceof NavigationEnd
      ) {
        this.isLoading = false;
        this.isFirstLoading = false;
      }
    });
  }

  ngOnInit() {
    this.checkParams();
    this.isFirstLoading = true;
    if (isPlatformBrowser(this.platformId)) {
      this.routeEvent(this.router);
      this.windowsSizeService.listen();
      this.isMobile$ = this.windowsSizeService.isMobile$;
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.checkParams();
    });
  }

  logedIn() {
    return this.authUserService.isAuthenticated();
  }

  isActive() {
    return this.authUserService.isActive();
  }

  isAdmin() {
    return this.authUserService.isAdmin();
  }

  hasBonus() {
    return this.authUserService.getUserBonus();
  }

  navAction(toOpen?: boolean) {
    if (this.isConnection) {
      toOpen ? this.sidenav.toggle() : this.sidenav.close();
    }
  }

  takeBonus() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: "dialog-back",
      width: "600px",
      data: {
        head: "Бонус 200 вагеров за регистрацию!",
        btnOrange: "Получить бонус",
      },
    });
    this.subscription.add(
      infoDialog.afterClosed().subscribe((result) => {
        if (result === "btnOrange") {
          this.subscription.add(
            this.bonusService.useBonusInfo().subscribe(
              (response: any) => {
                this.toastService.success("Бонус успешно получен!");
                this.authUserService.setBonus(false);
                window.location.reload();
              },
              (err) => {
                const error = err.error.errors[0].message;
                this.toastService.error(error);
                setTimeout(() => window.location.reload(), 1000);
              }
            )
          );
        }
      })
    );
  }

  toRecharge() {
    this.dialog.open(RechargeComponent, {
      disableClose: true,
      backdropClass: "dialog-back",
      width: "490px",
      data: {},
    });
  }

  toPayout() {
    this.dialog.open(PayoutComponent, {
      backdropClass: "dialog-back",
      width: "490px",
      disableClose: true,
      data: {},
    });
  }

  createMatch() {
    if (this.isConnection) {
      this.createMatchService.checkStatus();
    }
  }

  openLogin() {
    if (this.isConnection) {
      this.dialog.open(LoginComponent, {
        disableClose: true,
        backdropClass: "dialog-back",
        width: "490px",
        data: {},
      });
    }
  }

  openRegister() {
    if (this.isConnection) {
      this.dialog.open(RegisterComponent, {
        disableClose: true,
        backdropClass: "dialog-back",
        width: "490px",
        data: {},
      });
    }
  }

  logout() {
    this.authUserService.logout();
    this.router.navigateByUrl("");
    this.toastService.success(`До встречи в Wagermatch !`);
    this.notificationService.disconnect();
  }

  checkParams() {
    const queryParams: IInitQueryParams = this.route.snapshot.queryParams;
    if (isEmptyObject(queryParams)) return;
    const state = queryParams.state;
    if (state === "register") {
      this.openRegister();
    }
    if (queryParams.sourceLink) {
      this.sendSourceLog(queryParams.sourceLink);
    }
    const newQueryParams: IInitQueryParams = {
      sourceLink: null,
      state: null,
    };
    if (queryParams.sourceLink) {
      newQueryParams.sourceLinkAction = queryParams.sourceLink;
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: newQueryParams,
      queryParamsHandling: "merge",
    });
  }

  sendSourceLog(sourceLink: string) {
    const data: ISourceLogBody = {
      action: LogActionEnum.ENTRY,
      sourceLink
    };
    this.logsService.sendSourceLog(data).subscribe(()=> {});
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}

export interface IInitQueryParams {
  state?: "login" | "register";
  login?: string;
  email?: string;
  sourceLink?: string;
  sourceLinkAction?: string;
  bonus?: boolean;
}
