import { ApiConnectService } from './api-connect.service.';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlockedApiGuard implements CanActivate {
  constructor(private apiConectService: ApiConnectService,
              private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // this.apiConectService.getMatchlist();
    return false;
    if (!this.apiConectService.getApiStatus()) {
      return true;
    } else {
      console.log('bad blocked');
      this.router.navigate(['']);
    }
  }
}
