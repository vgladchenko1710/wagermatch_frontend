import { environment } from './../../../environments/environment';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { ForgotPasswordComponent } from './../../_dialogs/login/forgot-password/forgot-password.component';
import { MatDialog } from '@angular/material/dialog';
import { CreateMatchService } from './../../_services/controllers/create-match.service';
import { Component, OnInit } from '@angular/core';

declare let jivo_api: any;


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {
  public env = environment;
  constructor(private createMatchService: CreateMatchService,
              private dialog: MatDialog,
              private authUserService: AuthUserService) { }

  ngOnInit() {
  }

  createMatch() {
    this.createMatchService.checkStatus();
  }

  forgotPassword() {
    this.dialog.open(ForgotPasswordComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
  }

  openChat() {
    if(this.authUserService.isAuthenticated() && jivo_api) {
      const user = this.authUserService.getUser();
      const contact = user.vkID ? 'vkID: ' + user.vkID : 'email: ' + user.email;
      jivo_api?.setContactInfo({
        "name": user.login,
        "email": user.email ? user.email : 'VKUSER@gmail.com',
        "phone": "",
        "description": `plarform: ${user.platform}, ${contact}`
      });
    }
    jivo_api?.open();
  }

}
