import {style, state, animate, transition, trigger, animation, useAnimation} from '@angular/animations';
export let fadeInAnimation = animation([
    style({opacity : 0}),
    animate('{{duration}} {{easing}}')
], {
    params:{
        duration: '1s',
        easing: 'ease-in'
    }
});

export let fadeOutAnimation = animation([
    style({opacity : 1}),
    animate('{{duration}} {{easing}}', style({opacity: 0}))
], {
    params: {
        duration: '1s',
        easing: 'ease-out'
    }
});

export let fadeInFastAnimation = animation([
    style({opacity : 0}),
    animate('{{duration}} {{easing}}')
], {
    params:{
        duration: '300ms',
        easing: 'ease-in'
    }
});

export let fadeOutFastAnimation = animation([
    style({opacity : 1}),
    animate('{{duration}} {{easing}}', style({opacity: 0}))
], {
    params: {
        duration: '300ms',
        easing: 'ease-out'
    }
});

export let fade =
    trigger('fade', [
          transition(':enter',
            useAnimation(fadeInAnimation)
          ),
        ]);
export let step0 =
    trigger('step0', [
            transition(':enter',
            useAnimation(fadeInAnimation,  {
                params: {
                duration: '1000ms'}
            }))
]);
export let step1 =
trigger('step1', [
        transition(':enter',
        useAnimation(fadeInAnimation, {
            params: {
                duration: '1200ms'}
        }))
]);
export let step2 =
trigger('step2', [
        transition(':enter',
        useAnimation(fadeInAnimation, {
            params: {
                duration: '1400ms'}
        }))
]);
export let step3 =
trigger('step3', [
        transition(':enter',
        useAnimation(fadeInAnimation, {
            params: {
                duration: '1600ms'}
        }))
]);
export let fadeMix =
    trigger('fadeMix', [
          transition(':enter',
            useAnimation(fadeInAnimation)
          ),
          transition(':leave',
              useAnimation(fadeOutAnimation)
          )
]);

export let fadeMixfast =
    trigger('fadeMixfast', [
          transition(':enter',
            useAnimation(fadeInFastAnimation)
          ),
          transition(':leave',
              useAnimation(fadeOutFastAnimation)
          )
]);