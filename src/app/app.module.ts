import { DialogsModule } from './_dialogs/dialogs.module';
import { SharedModule } from './_shared/shared.module';
import { HeaderNotificationComponent } from './_components/header-notification/header-notification.component';
import { CustomMaterialModule } from './@theme/custom-material.module';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { FooterComponent } from './_components/footer/footer.component';
import { AuthInterceptor } from './_services/auth/auth.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { PagesModule } from './pages/pages.module';
import { HeaderNameComponent } from './_components/header-name/header-name.component';
import { HeaderAdminComponent } from './_components/header-admin/header-admin.component';
import { NotificationPipe } from './_components/header-notification/notification.pipe';
registerLocaleData(localeFr);


@NgModule({
   declarations: [
      AppComponent,
      FooterComponent,
      NotFoundComponent,
      HeaderNotificationComponent,
      HeaderNameComponent,
      HeaderAdminComponent,
      NotificationPipe
   ],
   imports: [
      BrowserModule.withServerTransition({ appId: 'serverApp' }),
      PagesModule,
      AppRoutingModule,
      CustomMaterialModule,
      BrowserAnimationsModule,
      SharedModule,
      DialogsModule,
      [ HttpClientModule ]
   ],
   providers: [
      { provide: LOCALE_ID, useValue: 'fr-FR'},
      { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
