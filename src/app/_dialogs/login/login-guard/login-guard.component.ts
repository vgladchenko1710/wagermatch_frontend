import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-login-guard',
  templateUrl: './login-guard.component.html',
  styleUrls: ['./login-guard.component.scss']
})
export class LoginGuardComponent implements OnInit {
  public action = '';

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<LoginGuardComponent>) {
                this.action = data.action;
               }

  ngOnInit(): void {
  }

}
