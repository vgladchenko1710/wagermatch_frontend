import { Title, Meta } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screen-example',
  templateUrl: './screen-example.component.html',
  styleUrls: ['./screen-example.component.scss']
})
export class ScreenExampleComponent implements OnInit {

  constructor(private titleService: Title,
              private meta: Meta) {
                this.titleService.setTitle(`Пример скриншота`+' ‣Wagermatch');
                this.meta.updateTag({name: 'description', content: 'Как должен выглядить пример скиншота, который нужно отправить после победы на Wagermatch.'})
              }

  ngOnInit(): void {
  }

}
