import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {MatAccordion} from '@angular/material/expansion';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})
export class RulesComponent implements OnInit {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  @Input() title;
  @Input() list = [];

  constructor() { }

  ngOnInit(): void {
  }

}
