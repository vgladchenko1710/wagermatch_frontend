import { IMessage } from './chat.model';
import { debounceTime } from 'rxjs/operators';
import { MatchesEventService } from './../../_services/events/matches-event.service';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { NotificationService } from './../../_services/notification-service';
import { ToastService } from './../../_services/toast.service';
import { Subscription, Subject } from 'rxjs';
import { Component, OnInit, ViewChild, ElementRef, AfterViewChecked, OnChanges, Input, OnDestroy } from '@angular/core';
import { isMobile } from '../../_services/help-services/device-detection';

const DEBOUNCE_TIME = 4000;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  providers: [NotificationService]
})
export class ChatComponent implements OnInit, OnChanges, OnDestroy {
  public subscription = new Subscription();
  public updateChat: Subject<any> = new Subject<any>();
  public myMessageSubscription = new Subscription();
  @ViewChild('chatArea') private chatArea: ElementRef;
  @Input() message;
  @Input() userName;
  public messages: IMessage[] =  [];
  @Input() matchUid: string;
  public isUnread = true;
  private userUid = '';
  public newMessage = '';
  public showDate = false;
  public typingNow: boolean;
  public isTyping: boolean;
  public sent = false;
  public isSending: boolean;
  public isMobile = isMobile();

  constructor(private notificationService: NotificationService,
              private authUserService: AuthUserService,
              private matchesEventService: MatchesEventService,
              public toastService: ToastService) {
                this.userUid = this.authUserService.getUserUid();
              }

  ngOnInit(): void {
    this.scrollToBottom(true);
    this.getMessages();
    this.getSocketMessage();
    this.getTypingStatus();
    this.getReadStatus();
    if (this.isMobile) {
      this.toUpdateChat();
    }

    this.subscription.add(this.matchesEventService.updateMessages$
      .subscribe ((response: any) => {
        this.getMessages();
    }));
  }

  getMessages() {
    this.subscription.add(this.notificationService.getMessages(this.userUid, this.matchUid)
    .subscribe ((response: {result: {chatList: IMessage[]}}) => {
      this.messages = response.result.chatList;
      this.findLastReaded();
      setTimeout(() => this.scrollToBottom(), 1000);
    }));
  }

  getSocketMessage() {
    this.subscription.add(this.matchesEventService.getMessages$
      .subscribe((messageData: any) => {
        const isExist = this.messages.some(message => message?.messageUid===messageData?.message?.messageUid);
        if (!isExist) {
          this.messages.push(messageData.message);
          this.isUnread = true;
          this.scrollToBottom();
          this.findLastReaded();
        }
    }));
  }

  getTypingStatus() {
    this.subscription.add(this.matchesEventService.getTypingStatus$
      .subscribe((isTyping: boolean) => {
        this.isTyping = isTyping;
    }));
  }

  getReadStatus() {
    this.subscription.add(this.matchesEventService.getReadStatus$
      .subscribe((isTyping: boolean) => {
        for (let i = this.messages.length - 1; i >= 0; i--) {
          if (this.messages[i].isOwner) {
            this.messages[i].isRead = true;
            break;
          }
        }
        this.findLastReaded();
        this.scrollToBottom();
    }));
  }

  ngOnChanges() {
    this.scrollToBottom();
    this.findLastReaded();
  }

  toUpdateChat() {
    this.updateChat
      .pipe(debounceTime(DEBOUNCE_TIME))
      .subscribe(() => {
        this.getMessages();
        this.notificationService.connect();
    });
    document.addEventListener("touchend", () =>  this.updateChat.next());
  }

  messageClick() {
    this.showDate = true;
    setTimeout(() => {
      this.showDate = false;
    }, 1000);
  }

  sendMessage() {
    if (this.newMessage.trim() !== '' && !this.isSending)  {
      this.isSending = true;
      this.subscription.add(this.notificationService.sendMessage(this.userUid, this.matchUid, {message: this.newMessage})
        .subscribe ((response: any) => {
          this.isSending = false;
          this.messages.push({message: this.newMessage, date: new Date(), isOwner: true, isRead: false});
          // Socket sygnal
          const messageData = {
            newMessage: this.newMessage,
            matchUid: this.matchUid,
            userUid: this.authUserService.getUserUid(),
            messageUid: response.messageUid
          };
          this.notificationService.sendSocketMessage(messageData);
          // =====================>
          this.scrollToBottom();
          this.newMessage = '';
          }, (err) => {
            this.isSending = false;
            this.toastService.warning(err.error.errors[0].message);
      }));
    }
  }

  sendTypingStatus(isTyping) {
    this.checkReadStatus();
    this.notificationService.sendTypingStatus({matchUid: this.matchUid, isTyping});
  }

  checkReadStatus() {
    if (this.isUnread) {
      for (let i = this.messages.length - 1; i >= 0; i--) {
        if (!this.messages[i].isOwner && !this.messages[i].isRead ) {
          this.sendReadStatus(this.messages[i].messageUid);
          break;
        }
      }
    }
  }

  sendReadStatus(messageUid) {
    this.isUnread = false;
    this.subscription.add(this.notificationService.changeReadStatus(this.userUid, messageUid)
      .subscribe ((response: any) => {
        // Socket sygnal
        this.notificationService.sendReadStatus(this.matchUid);
        // =====================>
        }, (err) => {
          this.isUnread = true;
          this.toastService.warning(err.error.errors[0].message);
    }));
  }

  findLastReaded() {
    let found = false;
    let count = 0;
    for (let i = this.messages.length - 1; i >= 0; i--) {
      count++;
      if (this.messages[i].isOwner && this.messages[i].isRead && !found) {
        this.messages[i].lastRead = true;
        found = true;
      } else if (this.messages[i].isOwner && this.messages[i].isRead && found) {
        this.messages[i].lastRead = false;
        break;
      }
    }
  }

  private scrollToBottom(noSmooth?: boolean): void {
    setTimeout(() => {
      this.chatArea.nativeElement.scroll({
        top: this.chatArea.nativeElement.scrollHeight,
        left: 0,
        behavior: noSmooth ? 'auto' : 'smooth'
      });
    }, 100);
  }

  onKeydown(event) {
    event.preventDefault();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
