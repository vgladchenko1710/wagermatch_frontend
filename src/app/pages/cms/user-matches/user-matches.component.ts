import { ActivatedRoute } from '@angular/router';
import { UsersService } from './../../../_services/users.service';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-matches',
  templateUrl: './user-matches.component.html',
  styleUrls: ['./user-matches.component.scss']
})
export class UserMatchesComponent implements OnInit, OnDestroy {
  public selectedIndex = 0;
  public userUid = '';
  public user;
  public subscription = new Subscription();

  constructor(private usersService: UsersService,
              public route: ActivatedRoute) { }

  ngOnInit() {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.userUid = params.get('userUid');
        this.getUserSettings();
    }));
  }

   getUserSettings() {
    this.subscription.add(this.usersService.getUsersSetting(this.userUid)
      .subscribe((response: any) => {
        this.user = response.result;
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
