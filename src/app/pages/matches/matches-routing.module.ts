import { AuthGuard } from './../../_services/auth/auth.guard';
import { MyMatchesComponent } from './my-matches/my-matches.component';
import { MatchComponent } from './match/match.component';
import { AllMatchesComponent } from './all-matches/all-matches.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';


const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'all-matches',
                component: AllMatchesComponent,
            },
            {
                path: 'match/:matchUid',
                component: MatchComponent,
            },
            {
                path: 'all-matches',
                component: MatchComponent,
            },
            {
                path: 'my-matches',
                component: MyMatchesComponent,
                canActivate: [AuthGuard]
            },
            { path: '', redirectTo: 'all-matches', pathMatch: 'full'}
        ]
    }
]


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatchesRoutingModule { }