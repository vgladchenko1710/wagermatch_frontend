import { AuthUserService } from './auth/auth-user.service';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class BonusService {

  constructor(private httpClient: HttpClient,
              private authUserService: AuthUserService) {}

  get userUid() {
    return this.authUserService.getUserUid();
  }

  getBonusInfo() {
    return this.httpClient.get(`${environment.apiUrl}/users/${this.userUid}/promo-start`)
  }

  useBonusInfo() {
    return this.httpClient.post(`${environment.apiUrl}/users/promo-start`, {userUid: this.userUid})
  }

  checkPromo(code: string) {
    return this.httpClient.get(`${environment.apiUrl}/users/${this.userUid}/promocode/check/${code}`);
  }

}
