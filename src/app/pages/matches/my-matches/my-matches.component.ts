import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-matches',
  templateUrl: './my-matches.component.html',
  styleUrls: ['./my-matches.component.scss']
})
export class MyMatchesComponent implements OnInit {
  private subscription: Subscription = new Subscription();
  public selectedIndex = 0;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.subscription.add(this.route.queryParamMap
      .subscribe((params) => {
        if (params.get('status') === 'archive') {
          this.selectedIndex = 3;
        }
    }));
  }


}
