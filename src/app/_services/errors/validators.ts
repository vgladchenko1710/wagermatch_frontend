import { FormControl, FormGroup, ValidatorFn, ValidationErrors } from '@angular/forms';

export const atLeastOne = (validator: ValidatorFn, controls: string[] = null) => (group: FormGroup): ValidationErrors | null => {
    if (!controls) {
      controls = Object.keys(group.controls);
    }
    const hasAtLeastOne = group && group.controls && controls
      .some(k => !validator(group.controls[k]));
    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
};

function SpaceCount(str) { 
    return str.split(" ").length;
}

function hasNumber(myString) {
    return /\d/.test(myString);
}

export function twoWords(control: FormControl): { [ key: string ]: boolean } {
    return (control.value != null  && !hasNumber(control.value) && SpaceCount(control.value) === 2) ? null : { 'twoWords': true };
}
export function equalToFieldValue(fieldValue: string) {
    // tslint:disable-next-line
    return (control: FormControl): { [key: string]: boolean } => fieldValue != control.value ? { 'equalToFieldValue': true } : null;
}