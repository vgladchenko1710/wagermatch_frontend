import { UserArbitrazComponent } from './user-arbitraz/user-arbitraz.component';
import { UserFinanceComponent } from './user-finance/user-finance.component';
import { UserChangePasswordComponent } from './user-change-password/user-change-password.component';
import { UserSettingComponent } from './user-setting/user-setting.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { AdminGuard } from './../../../_services/auth/admin.guard';
import { AuthGuard } from './../../../_services/auth/auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

// определение дочерних маршрутов
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard, AdminGuard],
    children: [
      { path: 'account', component: UserAccountComponent, pathMatch: 'full'},
      { path: 'setting', component: UserSettingComponent},
      { path: 'password', component: UserChangePasswordComponent},
      { path: 'transactions', component: UserFinanceComponent},
      { path: 'dispute', component: UserArbitrazComponent},
      // { path: 'notifications', component: SettingComponent},
      { path: '', redirectTo: 'account', pathMatch: 'full'}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserProfileRoutingModule {}
