"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
exports.AppComponent = void 0;
var animations_1 = require("./_animations/animations");
var rxjs_1 = require("rxjs");
var register_component_1 = require("./_dialogs/register/register.component");
var login_component_1 = require("./_dialogs/login/login.component");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var AppComponent = /** @class */ (function () {
    function AppComponent(dialog, createMatchService, router, location, toastService, document, authUserService) {
        this.dialog = dialog;
        this.createMatchService = createMatchService;
        this.router = router;
        this.location = location;
        this.toastService = toastService;
        this.document = document;
        this.authUserService = authUserService;
        this.subscription = new rxjs_1.Subscription();
        this.title = 'wagermatch';
        this.isLoading = true;
        this.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        this.routeEvent(this.router);
        window.scrollTo(0, 0);
    }
    AppComponent.prototype.routeEvent = function (router) {
        var _this = this;
        router.events.subscribe(function (evt) {
            if (evt instanceof router_1.NavigationStart) {
                _this.isLoading = true;
            }
            else if (evt instanceof router_1.NavigationCancel ||
                evt instanceof router_1.NavigationError ||
                evt instanceof router_1.NavigationEnd) {
                if (_this.location.path().includes('all-matches') || _this.location.path().includes('my-matches')) {
                    _this.isLoading = false;
                    window.scrollTo(0, 0);
                }
                setTimeout(function () {
                    _this.isLoading = false;
                    window.scrollTo(0, 0);
                }, 800);
            }
        });
    };
    AppComponent.prototype.ngOnInit = function () {
        window.scrollTo(0, 0);
    };
    AppComponent.prototype.logedIn = function () {
        return this.authUserService.isAuthenticated();
    };
    AppComponent.prototype.isActive = function () {
        return this.authUserService.isActive();
    };
    AppComponent.prototype.navAction = function (toOpen) {
        window.scrollTo(0, 0);
        toOpen ? this.sidenav.toggle() : this.sidenav.close();
    };
    AppComponent.prototype.openLogin = function () {
        this.dialog.open(login_component_1.LoginComponent, {
            disableClose: true,
            backdropClass: 'dialog-back',
            width: '490px',
            data: {}
        });
    };
    AppComponent.prototype.openRegister = function () {
        this.dialog.open(register_component_1.RegisterComponent, {
            disableClose: true,
            backdropClass: 'dialog-back',
            width: '490px',
            data: {}
        });
    };
    AppComponent.prototype.logout = function () {
        this.authUserService.logout();
        this.router.navigateByUrl('');
        this.toastService.success("\u0414\u043E \u0432\u0441\u0442\u0440\u0435\u0447\u0438 \u0432 Wager match !");
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    __decorate([
        core_1.ViewChild('sidenav', { static: false })
    ], AppComponent.prototype, "sidenav");
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss'],
            animations: [animations_1.fade]
        }),
        __param(5, core_2.Inject(common_1.DOCUMENT))
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
