import { IMatchInfo } from './../match.model';
import { NotificationTypeEnum } from './../../../_components/header-notification/notification.model';
import { ResultMatchComponent } from './../../../_dialogs/result-match/result-match.component';
import { InfoComponent } from './../../../_dialogs/info/info.component';
import { CreateMatchComponent } from '../../../_dialogs/create-match/create-match.component';
import { MatchesService } from './../../../_services/matches.service';
import { NotificationService } from './../../../_services/notification-service';
import { MatchesEventService } from './../../../_services/events/matches-event.service';
import { CreateMatchService } from './../../../_services/controllers/create-match.service';
import { ToastService } from './../../../_services/toast.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {Location} from '@angular/common';
import { faXbox, faPlaystation } from '@fortawesome/fontawesome-free-brands';
import { MatDialog } from '@angular/material/dialog';
import {faqList} from '../../../pages/info-pages/faq/faq-text'

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit, OnDestroy {
  public subscription: Subscription = new Subscription ();
  public isLoading = true;
  public userUid;
  public matchUid;
  public matchInfo;
  public messages = [];
  public commission = 0;
  public XboxIcon = faXbox;
  public Ps4Icon = faPlaystation;
  public timerExpire = '00:00';
  public isAdmin: boolean;
  public faqList;

  constructor(private location: Location,
              private dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private authUserService: AuthUserService,
              private toastService: ToastService,
              public createMatchService: CreateMatchService,
              private matchesEventService: MatchesEventService,
              private notificationService: NotificationService,
              private matchesService: MatchesService,
              private titleService: Title,
              private meta: Meta) {
                this.isAdmin = this.authUserService.isAdmin();
              }

  ngOnInit(): void {
    this.userUid = this.authUserService.getUserUid();
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.matchUid = params.get('matchUid');
        this.getMatch();
    }));
    this.subscription.add(this.matchesEventService.updateMatch$
      .subscribe ((response: any) => {
        this.getMatch();
    }));
  }

  getMatch() {
    this.subscription.add(this.matchesService.getMatchInfo(this.matchUid, this.userUid)
      .subscribe ((response: {result: IMatchInfo}) => {
        response.result.status.shortName === 'Archive status' ?
          this.router.navigate(['matches/my-matches'], { queryParams: { status: 'archive' } }) : null;
        this.matchInfo = response.result;
        this.messages = response.result.messages;
        this.timerExpire = this.convertToHrsMins(this.matchInfo.haveTime);
        this.generateFAQ();
        this.titleService.setTitle(`Матч: ${this.matchInfo.ownerLogin}, ${this.matchInfo.bid} вагеров`+' ‣Wagermatch');
        this.meta.addTag({name: 'description', content: 'Сыграй матч в FIFA 21 и забери свой выигрыш. Доступно на всех платформах PlayStation 4, PlayStation 5 или Xbox One, Xbox Series S, Xbox Series X.'})
        this.isLoading = false;
        }, (err) => {
          this.router.navigate(['not-founded']);
      }));
  }

  editMatch() {
    const editInfo = {
      bid: this.matchInfo.bid,
      platform: this.matchInfo.platform,
      withAlly: this.matchInfo.withAlly,
      enemyWithAlly: this.matchInfo.enemyWithAlly,
      timeExpire: 'Выбранный: ' + this.timerExpire,
      matchUid: this.matchUid,
      gameMode: this.matchInfo.gameMode
    };
    const editDialog = this.dialog.open(CreateMatchComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {editInfo}
    });
    this.subscription.add(editDialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.getMatch();
        }
    }));
  }

  deleteMatch() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Ты действительно хочешь удалить этот матч ?',
              infoText: '',
              btn: 'Удалить этот матч',
              btnOrange: 'Вернуться к матчу'
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        if (result === 'btn') {
          this.subscription.add(this.matchesService.deleteMatch(this.userUid, this.matchUid)
            .subscribe ((response: any) => {
              this.toastService.success(response.message);
              this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: NotificationTypeEnum.DELETE});
              setTimeout(() => {
                this.router.navigate(['matches/all-matches']);
                this.matchesEventService.toUpdateBalance(+this.matchInfo.bid)
              }, 1000);
            }, (err) => {
                const error = err.error.errors[0].message;
                this.toastService.error(error);
                this.getMatch();
          }));
        }
      }));
  }

  dropFropMatch() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Ты действительно хочешь отказаться от участия ?',
              infoText: 'Сумма пари будет в полном размере возвращена на твой баланс',
              btn: 'Отказаться от участия',
              btnOrange: 'Вернуться к матчу'
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        if (result === 'btn') {
          this.subscription.add(this.matchesService.dropFropMatch(this.userUid, this.matchUid)
            .subscribe ((response: any) => {
              this.toastService.success(response.message);
              this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: 'dropped'});
              this.matchesEventService.toUpdateBalance(this.matchInfo.bid)
              setTimeout(() => {
                this.router.navigate(['matches/all-matches']);
              }, 1000);
            }, (err) => {
                const error = err.error.errors[0].message;
                this.toastService.error(error);
                this.getMatch();
          }));
        }
      }));
  }

  isReady(isReady: boolean) {
    this.subscription.add(this.matchesService.readyToMatch(this.userUid, this.matchUid, {isReady})
    .subscribe ((response: any) => {
      this.getMatch();
      this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: 'ready'});
      }, (err) => {
        this.toastService.error(err.error.errors[0].message);
        this.getMatch();
    }));
  }

  startMatch() {
    this.subscription.add(this.matchesService.startMatch(this.userUid, this.matchUid)
    .subscribe ((response: any) => {
      this.toastService.success(response.message);
      this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: 'start'});
      this.getMatch();
      }, (err) => {
        this.toastService.error(err.error.errors[0].message);
        this.getMatch();
    }));
  }

  changeEnemy() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Ты действительно хочешь сменить соперника ?',
              btn: 'Сменить соперника',
              btnOrange: 'Вернуться к матчу'
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        if (result === 'btn') {
          this.subscription.add(this.matchesService.changeEnemy(this.userUid, this.matchUid)
            .subscribe ((response: any) => {
              this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: NotificationTypeEnum.CHANGE_ENEMY});
              this.toastService.success(response.message);
              this.getMatch();
            }, (err) => {
                const error = err.error.errors[0].message;
                this.toastService.error(error);
                this.getMatch();
          }));
        }
      }));
  }

  matchResult(isWin: boolean, noMatch?: boolean) {
    const matchResult = this.dialog.open(ResultMatchComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '700px',
      data: {
        isWin,
        noMatch,
        homeName: this.matchInfo.ownerNickName,
        awayName: this.matchInfo.enemyNickName,
        isOwner: this.matchInfo.isOwner,
        isEnemy:  this.matchInfo.isEnemy,
        matchUid: this.matchUid
      }
    });
    this.subscription.add(matchResult.afterClosed()
      .subscribe(result => {
        if (result) {
          this.notificationService.sendMatchStatus({matchUid: this.matchUid, status: 'result'});
          this.matchesEventService.toUpdateBalance(0);
          this.getMatch();
        }
    }));
  }

  convertToHrsMins(minutes) {
    const h = Math.floor(minutes / 60);
    const m = minutes % 60;
    const hh = h < 10 ? '0' + h : h;
    const mm = m < 10 ? '0' + m : m;
    return hh + ':' + mm;
  }

  generateFAQ() {
    const status = this.matchInfo.status.shortName;
    let who: string;
    if (this.matchInfo.isOwner) {
      who = 'host';
    } else if (this.matchInfo.isEnemy) {
      who = 'enemy';
    } else {
      who = 'guest';
    }
    this.faqList = faqList().find(group => group.status.includes(status) && group.who.includes(who));
    !this.faqList ? this.faqList = faqList().find(group => group.status.includes('match') && group.who.includes('guest')) : null;
  }

  back() {
    this.location.back();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
