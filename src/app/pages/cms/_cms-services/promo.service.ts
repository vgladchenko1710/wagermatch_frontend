import { IPromo } from './../promo/promo.model';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { SortFilterPaginationRequest } from './../../../_models/pagination';
import { environment } from '../../../../environments/environment';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
@Injectable()
export class PromoService {
  constructor(private httpClient: HttpClient,
              private authUserService: AuthUserService) {}


  get adminUid() {
    return this.authUserService.getUserUid();
  }

  getPromoList(status: string, sortFilterPagination: SortFilterPaginationRequest) {
    return this.httpClient.get(`${environment.apiUrl}/admin/promocodes`, {params: {status, ...sortFilterPagination}});
  }

  addPromo(data: IPromo) {
    return this.httpClient.post(`${environment.apiUrl}/admin/promocodes`, data);
  }

  editPromo(data: IPromo) {
    return this.httpClient.put(`${environment.apiUrl}/admin/promocodes`, data);
  }

  deletePromo(data: {id: number}) {
    return this.httpClient.put(`${environment.apiUrl}/admin/promocodes/delete`, data);
  }


}
