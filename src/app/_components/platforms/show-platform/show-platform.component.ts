import { faXbox, faPlaystation } from '@fortawesome/fontawesome-free-brands';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-platform',
  templateUrl: './show-platform.component.html',
  styleUrls: ['./show-platform.component.scss']
})
export class ShowPlatformComponent implements OnInit {
  @Input() platform;
  @Input() shortForm: boolean;
  public XboxIcon = faXbox;
  public Ps4Icon = faPlaystation;

  constructor() { }

  ngOnInit(): void {
  }

}
