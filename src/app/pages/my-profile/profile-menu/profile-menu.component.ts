import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.scss']
})
export class ProfileMenuComponent implements OnInit {
  public isAdmin: boolean;

  constructor(public Router: ActivatedRoute,
              private authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.isAdmin = this.authUserService.isAdmin();
  }

}
