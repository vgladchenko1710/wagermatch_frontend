import { InfoComponent } from './../../../_dialogs/info/info.component';
import { ToastService } from './../../../_services/toast.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { AuthService } from './../../../_services/auth/auth.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-vk-auth',
  templateUrl: './vk-auth.component.html',
  styleUrls: ['./vk-auth.component.scss']
})
export class VkAuthComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public error = false;
  public success = false;

  constructor(public router: Router,
              public authService: AuthService,
              public authUserService: AuthUserService,
              public dialog: MatDialog,
              public toastService: ToastService,
              private route: ActivatedRoute) {
                if (this.authUserService.isAuthenticated()) {
                  this.authUserService.logout(true);
                }
              }

  ngOnInit(): void {
    this.subscription.add(this.route.queryParamMap
    .subscribe((queryParamMap) => {
      const code =  queryParamMap.get('code');
      if (code) {
        this.toLogin(code);
      } else {
        this.errorLogin();
      }
    }));
  }

  toLogin(code) {
    this.subscription.add(this.authUserService.vkLogin({code})
    .subscribe((result: any) => {
      result.isCookie ? this.adminLogin(result) : this.successLogin(result);
      },
    (err) => {
      if (err.error?.errors[0]?.message) {
        this.toastService.error(err.error?.errors[0]?.message);
        setTimeout(()=> {
          this.errorLogin();
        }, 2000)
      } else {
        this.errorLogin();
      }
    }));
  }

  successLogin(result: any) {
    setTimeout(() => {
      result.firstTimeLogin ?
        this.openInfo() :
        this.toastService.success(result.message);
      return this.authUserService.isActive() ? this.router.navigateByUrl('matches/all-matches') : this.router.navigateByUrl('my-profile/setting');
    }, 500);
  }

  adminLogin(result: any) {
    setTimeout(() => {
      this.toastService.success('Lecimy z koksem :)');
      this.router.navigateByUrl('cms');
    }, 500);
  }

  errorLogin() {
    this.error = true;
    this.redirect();
  }

  openInfo() {
    this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Приветствуем в Wagermatch!',
              infoText: 'Для участия в матчах необходимо заполнить все обязательные поля.',
              closeTimer: true
            }
    });
  }


  redirect() {
    setTimeout(() => {
      this.router.navigate(['/']);
    }, 3000);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
