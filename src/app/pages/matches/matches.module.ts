import { OwnerCreatedComponent } from './match/components/owner/owner-created/owner-created.component';
import { SharedModule } from './../../_shared/shared.module';
import { MatchComponent } from './match/match.component';
import { AllMatchesComponent } from './all-matches/all-matches.component';
import { MyMatchesComponent } from './my-matches/my-matches.component';
import { MatchListComponent } from './my-matches/match-list/match-list.component';
import { FilterMobileComponent } from './all-matches/filter-mobile/filter-mobile.component';
import { InfoComponent } from './../../_dialogs/info/info.component';
import { ResultMatchComponent } from './../../_dialogs/result-match/result-match.component';
import { JoinMatchComponent } from './../../_dialogs/join-match/join-match.component';
import { CreateMatchComponent } from './../../_dialogs/create-match/create-match.component';
import { NgxImageCompressService } from 'ngx-image-compress';
import { CustomMaterialModule } from './../../@theme/custom-material.module';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from './../../_components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchesRoutingModule } from './matches-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const matchAreaComponent = [
  OwnerCreatedComponent
]

@NgModule({
  declarations: [
    AllMatchesComponent,
    MyMatchesComponent,
    MatchListComponent,
    MatchComponent,
    FilterMobileComponent,
    JoinMatchComponent,
    ResultMatchComponent,
    CreateMatchComponent,
    InfoComponent,
    ...matchAreaComponent
  ],
  entryComponents: [
    CreateMatchComponent,
    JoinMatchComponent,
    ResultMatchComponent,
  ],
  imports: [
    CommonModule,
    MatchesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    NgbPaginationModule,
    CustomMaterialModule,
    SharedModule
  ],
  providers: [
    NgxImageCompressService
  ]
})
export class MatchesModule { }
