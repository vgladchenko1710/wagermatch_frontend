import { AuthGuard } from './../../_services/auth/auth.guard';
import { ArbitrazComponent } from './arbitraz/arbitraz.component';
import { FinanceComponent } from './finance/finance.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SettingComponent } from './setting/setting.component';
import { AccountComponent } from './account/account.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

// определение дочерних маршрутов
const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      { path: 'account', component: AccountComponent, pathMatch: 'full'},
      { path: 'setting', component: SettingComponent},
      { path: 'password', component: ChangePasswordComponent},
      { path: 'transactions', component: FinanceComponent},
      { path: 'dispute', component: ArbitrazComponent},
      // { path: 'notifications', component: SettingComponent},
      { path: '', redirectTo: 'account', pathMatch: 'full'}
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule {}
