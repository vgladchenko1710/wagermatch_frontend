import { ToastService } from './../../../_services/toast.service';
import { InfoComponent } from './../../../_dialogs/info/info.component';
import { MatDialog } from '@angular/material/dialog';
import { ConflictService } from './../_cms-services/conflict.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-conflict-info',
  templateUrl: './conflict-info.component.html',
  styleUrls: ['./conflict-info.component.scss']
})
export class ConflictInfoComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public conflictUid = '';
  public conflict;
  public hostDescription = '';
  public enemyDescription = '';
  public noMatchDescription = '';
  public descriptions = [
    'Игрок приложил скриншот, не соответвующий требованиям сайта.',
    'Игрок проигнорировал обращение администрации.',
    'Игрок нарушил регламент сайта, а именно п.'
  ];

  public NoMatchDescriptions = [
    'Игрок выбравший результат "Победа" приложил скриншот не соответвующий требованиям сайта.',
    'Недостаточно информации для определения победителя.'
  ];

  constructor(public route: ActivatedRoute,
              public location: Location,
              public dialog: MatDialog,
              private authUserService: AuthUserService,
              public toastService: ToastService,
              private conflictService: ConflictService,
              public router: Router) { }

  ngOnInit(): void {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.conflictUid = params.get('conflictUid');
        this.getConflict();
    }));
  }

  getConflict() {
    this.subscription.add(this.conflictService.getConflict(this.authUserService.getUserUid(), this.conflictUid)
    .subscribe ((response: any) => {
      this.conflict = response.result;
    }));
  }

  openScreen(link: any) {
    window.open(link, '_blank');
  }

  openUser(userUid) {
    this.router.navigate(['cms/user-profile', userUid]);
  }

  resolve(result: string, description: string, name?: string) {
    const popUpConfirm = result === 'draw' ? `Ты действительно считаешь что матч не состоялся??` : `Ты действительно хочешь решить конфликт в пользу ${name} ?`;
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: popUpConfirm,
              infoText: '',
              btn: `Вернуться назад`,
              btnOrange: `Решить конфликт`
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(res => {
        if (res === 'btnOrange') {
          const finalDescription = result === 'draw' ? description + '\n ' + `Матч не состоялся.` : description + '\n ' + `Победа в пользу ${name}.`;
          const data = {result, description : finalDescription};
          this.subscription.add(this.conflictService.resolveConflict(this.authUserService.getUserUid(), this.conflictUid, data)
            .subscribe ((response: any) => {
              this.toastService.success(response.message);
              setTimeout(() => {
                this.router.navigate(['cms/conflicts']);
              }, 1000);
            }, (err) => {
                const error = err.error.errors[0].message;
                this.toastService.error(error);
          }));
        }
      }));
  }

  // resolveNoMatch(description) {
  //   const infoDialog = this.dialog.open(InfoComponent, {
  //     disableClose: true,
  //     backdropClass: 'dialog-back',
  //     width: '600px',
  //     data: { head: `Ты действительно считаешь что матч не состоялся??`,
  //             infoText: '',
  //             btn: `Вернуться назад`,
  //             btnOrange: `Решить конфликт`
  //           },
  //   });
  //   this.subscription.add(infoDialog.afterClosed()
  //     .subscribe(result => {
  //       if (result === 'btnOrange') {
  //         const data = {isHostWin: false, isEnemyWin: false, description: description + '\n ' + `Матч не состоялся.`};
  //         this.subscription.add(this.conflictService.resolveConflict(this.authUserService.getUserUid(), this.conflictUid, data)
  //           .subscribe ((response: any) => {
  //             this.toastService.success(response.message);
  //             setTimeout(() => {
  //               this.router.navigate(['cms/conflicts']);
  //             }, 1000);
  //           }, (err) => {
  //               const error = err.error.errors[0].message;
  //               this.toastService.error(error);
  //         }));
  //       }
  //     }));
  // }

  back() {
    this.location.back();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
