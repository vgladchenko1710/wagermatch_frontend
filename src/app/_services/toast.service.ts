import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(private snackBar: MatSnackBar) {}

  error(message: any) {
    this.snackBar.open(this.getErrorMessage(message), '✖', {
      duration: 2500,
      panelClass: ['error'],
      verticalPosition: 'bottom',
      horizontalPosition: 'right'
    });
  }

  warning(message: string) {
    this.snackBar.open(message, 'Warning', {
      duration: 2500,
      panelClass: ['warning'],
      verticalPosition: 'bottom',
      horizontalPosition: 'right'
    });
  }

  success(message: string) {
    this.snackBar.open(message, '✔', {
      duration: 2500,
      panelClass: ['success'],
      verticalPosition: 'bottom',
      horizontalPosition: 'right'
    });
  }

  info(message: string) {
    this.snackBar.open(message, 'Success', {
      duration: 2500,
      panelClass: ['info'],
      verticalPosition: 'bottom',
      horizontalPosition: 'right'
    });
  }

  private getErrorMessage(error: any): string {
    let message = error;
    if (error.error) {
      const errorsArray = error.error.errors || [];
      if (errorsArray.length) {
        message = errorsArray[0].message || '';
      }
    }
    return message + '';
  }
}
