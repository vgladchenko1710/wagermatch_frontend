import { CreateMatchService } from './../../../../_services/controllers/create-match.service';
import { FormGroup, FormControl } from '@angular/forms';
import { faXbox, faPlaystation } from '@fortawesome/fontawesome-free-brands';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-filter-mobile',
  templateUrl: './filter-mobile.component.html',
  styleUrls: ['./filter-mobile.component.scss']
})
export class FilterMobileComponent implements OnInit {
  @Input() activePlatform;
  public bidForm: FormGroup;
  XboxIcon = faXbox;
  Ps4Icon = faPlaystation;

  constructor(private filterMobile: MatBottomSheetRef<FilterMobileComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) public filterData: any,
              public createMatchService: CreateMatchService) { }

  ngOnInit(): void {
    this.createBidForm();
    this.updateValues();
  }

  createBidForm() {
    this.bidForm = new FormGroup({
      min: new FormControl(),
      max: new FormControl()
    });
  }

  updateValues() {
    this.bidForm.patchValue({
      min: this.filterData.min,
      max: this.filterData.max
    });
    this.activePlatform = this.filterData.platform;
  }

  platformFilter(platform) {
    this.activePlatform = platform;
  }

  useFilter() {
    const filterData = {
      min: this.bidForm.get('min').value,
      max: this.bidForm.get('max').value,
      platform: this.activePlatform
    };
    this.filterMobile.dismiss(filterData);
  }

  close() {
    this.filterMobile.dismiss();
  }

}
