"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AppModule = void 0;
var footer_component_1 = require("./_components/footer/footer.component");
var custom_material_module_1 = require("src/app/@theme/custom-material.module");
var auth_interceptor_1 = require("./_services/auth/auth.interceptor");
var components_module_1 = require("./_components/components.module");
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var animations_1 = require("@angular/platform-browser/animations");
var angular_fontawesome_1 = require("@fortawesome/angular-fontawesome");
var angular_svg_icon_1 = require("angular-svg-icon");
var http_1 = require("@angular/common/http");
var core_2 = require("@angular/core");
var common_1 = require("@angular/common");
var fr_1 = require("@angular/common/locales/fr");
var pages_module_1 = require("./pages/pages.module");
common_1.registerLocaleData(fr_1["default"]);
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                footer_component_1.FooterComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                pages_module_1.PagesModule,
                app_routing_module_1.AppRoutingModule,
                animations_1.BrowserAnimationsModule,
                angular_fontawesome_1.FontAwesomeModule,
                components_module_1.ComponentsModule,
                custom_material_module_1.CustomMaterialModule,
                [http_1.HttpClientModule, angular_svg_icon_1.AngularSvgIconModule.forRoot()]
            ],
            providers: [
                { provide: core_2.LOCALE_ID, useValue: 'fr-FR' },
                { provide: http_1.HTTP_INTERCEPTORS, useClass: auth_interceptor_1.AuthInterceptor, multi: true },
            ],
            bootstrap: [
                app_component_1.AppComponent
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
