import { MatchesEventService } from './events/matches-event.service';
import { Router } from '@angular/router';
import { ToastService } from './toast.service';
import { AuthUserService } from './auth/auth-user.service';
import { Subscription, Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketIoService {
  public socket;
  public reconection;
  public reconnectCounter = 0;

  constructor(public router: Router,
              public matchesEventService: MatchesEventService,
              private authUserService: AuthUserService) {}

    connect() {
      if (!this.socket?.connected) {
        this.socket = io.connect(environment.socketUrl, {"transports": ['websocket']});
        this.userIdentification();
        this.onDisconnect();
        setTimeout(()=> {
          if (!this.socket?.connected) {
            this.reconnect();
          }
        }, 3000);
      }
    }


    onDisconnect() {
      this.socket.on("disconnect", (reason) => {
        setTimeout(() => {
          if (this.socket) {
            this.reconnect();
          }
        }, 1000)
      });
    }

    reconnect() {
      this.reconnectCounter ++;
      this.socket?.connect();
      setTimeout(() => {
        if (!this.socket?.connected) {
          this.reconnectCounter < 30 && this.reconnect();
        } else {
          this.userIdentification();
          this.matchesEventService.toUpdateMatch();
          this.reconnectCounter = 0;
        }
      }, 1000);
    }

    disconnect() {
      this.socket.disconnect();
      this.socket = undefined;
    }




    userIdentification() {
      this.socket.emit('uid', this.authUserService.getUserUid());
    }

    sendMessage(messageData) {
      this.socket.emit('message', messageData);
      console.log('send message:', messageData);
    }

    getMessage() {
      return new Observable((observer) => {
        this.socket.on('message', (message) => {
          observer.next(message);
        });
      });
    }

    sendTypingStatus(isTyping) {
      this.socket.emit('typing', isTyping);
    }

    getTyping() {
      return new Observable((observer) => {
        this.socket.on('typing', (isTyping) => {
            observer.next(isTyping);
        });
      });
    }

    sendReadStatus(matchUid) {
      this.socket.emit('read', matchUid);
      this.matchesEventService.updateReadStatus();
    }

    getReadStauts() {
      return new Observable((observer) => {
        this.socket.on('read', () => {
          observer.next();
        });
      });
    }

    sendMatchStatus(newStatus) {
      this.socket.emit('matchStatus', newStatus);
    }

    getMatchStatus() {
      return new Observable((observer) => {
        this.socket.on('matchStatus', (newStatus) => {
          observer.next(newStatus);
          if (newStatus.status === 'joined') {
            this.socket.emit('updateMatch', newStatus.matchUid);
          }
        });
      });
    }

}
