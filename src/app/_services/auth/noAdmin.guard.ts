import { AuthUserService } from './auth-user.service';
import { ApiConnectService } from '../controllers/api-connect.service.';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoAdminGuard implements CanActivate {
  constructor(private authUserService: AuthUserService,
              private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authUserService.isAdmin()) {
      return true;
    } else {
      this.router.navigate(['cms']);
    }
  }
}
