import { BonusService } from './../bonus.service';
import { LoginComponent } from './../../_dialogs/login/login.component';
import { MatDialog } from '@angular/material/dialog';
import { Auth } from './auth.enum';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { of as observableOf, Observable, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';

declare let jivo_api: any;

interface IUserData {
  uid: string;
  login: string;
  platform: string;
  gamertag: string;
  email: string;
  vkID: string;
  isCookie: boolean;
  isProxy: boolean;
  hasBonus?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthUserService {
  private authenticated = false;
  private token = '';
  private userData: IUserData = {
    uid: '',
    login: '',
    platform: '',
    gamertag: '',
    email: '',
    vkID: '',
    isCookie: false,
    isProxy: false,
    hasBonus: false,
  };
  constructor(private authService: AuthService,
              public dialog: MatDialog) {
    try {
      const user = JSON.parse(localStorage.getItem(Auth.KEY));
      this.setUser(user);
    } catch (error) {}
   }

   public login(data: any): Observable<object> {
    return this.authService.authenticate(data)
      .pipe(
        switchMap((result: any) => {
          const user = result.response;
          user.token = result.response.token;
          user.message = result.message;
          return this.setUser(user);
        })
      );
  }

  public vkLogin(data: any): Observable<object> {
    return this.authService.authenticateVk(data)
      .pipe(
        switchMap((result: any) => {
          const user = result.response;
          user.token = result.response.token;
          user.message = result.message;
          return this.setUser(user);
        })
      );
  }

  private setUser(user: any) {
    localStorage.setItem(Auth.KEY, JSON.stringify(user));
    this.token = user.token;
    this.userData = {
      uid: user.userUid,
      login: user.login,
      platform: user.platform,
      gamertag: user.gamertag,
      email: user.email,
      vkID: user.vkID,
      isCookie: user.isCookie,
      isProxy: user.isProxy
    };
    this.authenticated = true;
    this.setUserToChat();
    return observableOf(user);
  }

  setUserToChat() {
    const contact = this.userData.vkID ? 'vkID: ' + this.userData.vkID : 'email: ' + this.userData.email;
    setTimeout(() => {
      if (jivo_api) {
        jivo_api?.setContactInfo({
          "name": this.userData.login,
          "email": this.userData.email ? this.userData.email : 'VKUSER@gmail.com',
          "phone": "",
          "description": `plarform: ${this.userData.platform}, ${contact}`
        });
      }
    }, 3000)
  }

  public setBonus(hasBonus: boolean) {
    this.userData.hasBonus = hasBonus;
  }

  public getUser() {
    return this.userData;
  }

  public isAuthenticated() {
    return this.authenticated;
  }

  public isActive() {
    if (this.userData.platform && this.userData.gamertag && this.userData.login) {
      return true;
    }
    return false;
  }

  public getLogin() {
    return this.userData.login;
  }

  public logout(noLogin?) {
    localStorage.removeItem(Auth.KEY);
    this.authenticated = false;
    this.token = '';
    this.userData = {
      uid: '',
      login: '',
      platform: '',
      gamertag: '',
      email: '',
      vkID: '',
      isCookie: false,
      isProxy: false
    };
    noLogin ? null : setTimeout(() => this.openLogin(), 700);
  }

  openLogin() {
    this.dialog.open(LoginComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
  }

  public getToken() {
    return this.token;
  }

  public isAdmin() {
    return this.isAuthenticated && this.userData.isCookie;
  }

  public isSuperAdmin() {
    return this.isAuthenticated && this.userData.isProxy;
  }


  public getUserUid() {
    return this.userData.uid;
  }

  public getUserPlatform() {
    return this.userData.platform;
  }

  public getUserBonus() {
    return this.userData.hasBonus;
  }

  public isVk() {
    return this.userData.vkID ? true : false;
  }
}
