export const payoutColumns = [
  {
    fieldName: 'login',
    title: 'Логин',
  },
  {
    fieldName: 'email',
    title: 'Email',
  },
  {
    fieldName: 'date',
    title: 'Дата тран-и',
    extraValue: 'date'
  },
  {
    fieldName: 'amount',
    title: 'Вагеров',
    extraValue: 'wagers'
  },
  {
    fieldName: 'toPay',
    title: 'К выплате',
  },
  {
    fieldName: 'description',
    title: 'Описание',
  },
  {
    fieldName: 'type',
    title: 'Тип',
  },
  {
    fieldName: 'status',
    title: 'Статус',
  }
];

export const incomesColumns = [
  {
    fieldName: 'login',
    title: 'Логин',
  },
  {
    fieldName: 'email',
    title: 'Email',
  },
  {
    fieldName: 'date',
    title: 'Дата тран-и',
  },
  {
    fieldName: 'amount',
    title: 'Вагеров',
    extraValue: 'wagers'
  },
  {
    fieldName: 'description',
    title: 'Описание',
  },
  {
    fieldName: 'type',
    title: 'Тип',
  },
  {
    fieldName: 'status',
    title: 'Статус',
  }
]
