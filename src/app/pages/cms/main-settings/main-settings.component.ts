import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from './../../../_services/toast.service';
import { Subscription } from 'rxjs';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { AdminService } from './../_cms-services/admin.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

export interface ISettingsRates {
    matchRate: number,
    outcomeRate: number,
    minOutcome: number,
    maxOutcome: number,
    minBid: number,
    maxBid: number,
    minIncome: number,
    maxIncome: number,
    hrnRate: number,
    rubRate: number
}

@Component({
  selector: 'app-main-settings',
  templateUrl: './main-settings.component.html',
  styleUrls: ['./main-settings.component.scss']
})
export class MainSettingsComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public form: FormGroup;
  public isLoad: boolean;
  private adminUid = '';
  public rates: ISettingsRates;

  constructor(public adminService: AdminService,
              public toastService: ToastService,
              private authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.createForm();
    this.adminUid = this.authUserService.getUserUid();
    this.getRates();
  }

  getRates() {
    this.subscription.add(this.adminService.getRates()
      .subscribe((response: any) => {
        this.rates = response.result;
        this.updateValues();
      },  (err) => {
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  createForm() {
    this.form = new FormGroup({
      matchRate: new FormControl(null, [Validators.required]),
      outcomeRate: new FormControl(null, [Validators.required]),
      minOutcome: new FormControl(null, [Validators.required]),
      maxOutcome: new FormControl(null, [Validators.required]),
      minBid: new FormControl(null, [Validators.required]),
      maxBid: new FormControl(null, [Validators.required]),
      minIncome: new FormControl(null, [Validators.required]),
      maxIncome: new FormControl(null, [Validators.required]),
      hrnRate: new FormControl(null, [Validators.required]),
      rubRate: new FormControl(null, [Validators.required]),
    });
  }

  updateValues() {
    this.form.patchValue({
      matchRate: Math.round((1 - this.rates.matchRate) * 100),
      outcomeRate: Math.round((1 - this.rates.outcomeRate) * 100),
      minOutcome: this.rates.minOutcome,
      maxOutcome: this.rates.maxOutcome,
      minBid: this.rates.minBid,
      maxBid: this.rates.maxBid,
      minIncome: this.rates.minIncome,
      maxIncome: this.rates.maxIncome,
      hrnRate: this.rates.hrnRate,
      rubRate: this.rates.rubRate,
    });
  }

  saveValues() {
    this.isLoad = true;
    const data = {
      matchRate: 1 - (this.form.get('matchRate').value / 100),
      outcomeRate: 1 -  (this.form.get('outcomeRate').value / 100),
      minOutcome: this.form.get('minOutcome').value,
      maxOutcome: this.form.get('maxOutcome').value,
      minBid: this.form.get('minBid').value,
      maxBid: this.form.get('maxBid').value,
      minIncome: this.form.get('minIncome').value,
      maxIncome: this.form.get('maxIncome').value,
      hrnRate: this.form.get('hrnRate').value,
      rubRate: this.form.get('rubRate').value,
    };
    this.subscription.add(this.adminService.updateRates(this.adminUid, data)
      .subscribe((response: any) => {
        this.toastService.success(response.message);
        this.getRates();
        this.isLoad = false;
      },  (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  cleanMatchesAfterTimeout() {
    this.subscription.add(this.adminService.cleanMatchesAfterTimeout()
    .subscribe((response: any) => {
      this.toastService.success(response.message);
    },  (err) => {
      this.toastService.error(err.error.errors[0].message);
    }
    ));
  }

  finishMatchesAfterVerificationTimeout() {
    this.subscription.add(this.adminService.finishMatchesAfterVerificationTimeout()
    .subscribe((response: any) => {
      this.toastService.success(response.message);
    },  (err) => {
      this.toastService.error(err.error.errors[0].message);
    }
    ));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
