import { MatchesService } from './../../../../_services/matches.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'app-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss']
})
export class MatchListComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  @Input() status: string;
  public userUid = '';
  MatchesList = [];
  public toShowMatches = 15;
  public activePage = 1;
  public overalMatches = 0;
  // FILTER
  public loading = false;
  public filterDate = false;
  public filterBid = false;
  public isFirstDate = true;
  public isOwnerFilter: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authUserService: AuthUserService,
    private matchesService: MatchesService) { }

  ngOnInit() {
    this.userUid = this.authUserService.getUserUid();
    this.subscription.add(this.route.queryParamMap
      .subscribe((params) => {
        this.activePage = + (params.get('page') ? params.get('page') : '1');
        this.isFirstDate = params.get('isFirstDate') === 'false' ? false : true;
        this.filterDate = params.get('filterDate') === 'true' ? true : false;
        this.isOwnerFilter = params.get('isOwner') === 'true' ? true : false;
        this.loadMatches();
    }));
  }

  loadMatches() {
    this.loading = true;
    this.subscription.add(this.matchesService.getMyMatches(this.userUid, this.activePage, this.toShowMatches, this.filterDate, this.filterBid, this.isFirstDate, this.status)
      .subscribe(response => {
        this.MatchesList = response['matchList'];
        this.overalMatches = response['count'];
        setTimeout( () => {
          this.loading = false;
        }, 300);
        }, (err) => {
        this.loading = false;
    }));
  }

  changeSort(isFirstDate: boolean) {
    isFirstDate ? this.filterDate = !this.filterDate : this.filterBid = !this.filterBid;
    this.isFirstDate = isFirstDate;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {isFirstDate: this.isFirstDate, filterDate: this.filterDate, filterBid: this.filterBid, page: null},
        queryParamsHandling: 'merge'
      });
  }

  onChangePage() {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {page: this.activePage},
        queryParamsHandling: 'merge'
      });
  }

  isOwner() {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {isOwner: this.isOwnerFilter},
        queryParamsHandling: 'merge'
      });
  }

  openMatch(uid) {
    if (this.status !== 'archived') {
      this.router.navigate(['matches/match', uid]);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
