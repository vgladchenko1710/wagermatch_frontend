export enum NotificationTypeEnum {
  JOIN = 'join',
  DROP = 'drop',
  CHANGE_ENEMY = 'changeEnemy',
  DELETE = 'delete',
  READY = 'ready',
  NOT_READY = 'notReady',
  START = 'start',
  VERIFICATION = 'verification',
  CONFLICT = 'conflict',
  CONFLICT_RESOLVE = 'conflictResolve',
  TIMES_UP = 'timesUp',
  FINISHED = 'finished'
}
