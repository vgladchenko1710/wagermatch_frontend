import { GuardInfoComponent } from './../../_dialogs/guard-info/guard-info.component';
import { LoginGuardComponent } from './../../_dialogs/login/login-guard/login-guard.component';
import { EmailConfirmComponent } from './../../_dialogs/register/email-confirm/email-confirm.component';
import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { RegisterComponent } from './../../_dialogs/register/register.component';
import { LoginComponent } from './../../_dialogs/login/login.component';
import { ForgotPasswordComponent } from './../../_dialogs/login/forgot-password/forgot-password.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ComponentsModule } from './../../_components/components.module';
import { CustomMaterialModule } from './../../@theme/custom-material.module';
import { FixComponent } from './fix/fix.component';
import { VkAuthComponent } from './vk-auth/vk-auth.component';
import { NewPasswordComponent } from './../../_dialogs/login/new-password/new-password.component';
import { ConfirmedEmailComponent } from './confirmed-email/confirmed-email.component';
import { AuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    ConfirmedEmailComponent,
    NewPasswordComponent,
    VkAuthComponent,
    FixComponent,
    ForgotPasswordComponent,
    LoginComponent,
    RegisterComponent,
    EmailConfirmComponent,
    GuardInfoComponent,
    LoginGuardComponent,
    PaymentConfirmComponent
  ],
  entryComponents: [
    LoginComponent,
    RegisterComponent,
 ],
  imports: [
    AuthRoutingModule,
    CommonModule,
    CustomMaterialModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AuthModule { }
