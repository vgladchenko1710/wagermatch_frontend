export const PROMO_COLUMNS: IColumn[]  = [
    {
        fieldName: 'dateFrom',
        title: 'Дествителен от',
        extraValue: 'date'
    },
    {
        fieldName: 'dateTo',
        title: 'Дествителен до',
        extraValue: 'date'
    },
    {
        fieldName: 'code',
        title: 'Код'
    },
    {
        fieldName: 'value',
        title: 'Бонус',
    },
    {
        fieldName: 'minAmount',
        title: 'Минимальная сумма попол.',
    },
    {
        fieldName: 'status',
        title: 'Status',
        extraValue: 'status'
    },
];

export const PROMO_STATUSES = [
    {
      value: 'all', viewValue: 'All'
    },
    {
      value: 'active', viewValue: 'Active'
    },
    {
      value: 'inActive', viewValue: 'Inactive'
    }
];

interface IColumn {
    fieldName: (keyof IPromo),
    title: string;
    extraValue? : string;
}


export type IPromo = {
    id?: number,
    dateFrom: Date,
    dateTo: Date,
    value: number;
    code: string;
    minAmount: number;
    status?: PromoStatusEnum
}

export enum PromoStatusEnum {
    USED = 'used',
    ACTIVE = 'Active',
    INACTIVE = 'inActive'
}
