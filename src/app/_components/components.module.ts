import { SharedModule } from './../_shared/shared.module';
import { CustomMaterialModule } from '../@theme/custom-material.module';
import { EmptyListComponent } from './empty-list/empty-list.component';
import { SearchTableComponent } from './search-table/search-table.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TablePageComponent } from './page-components/table-page/table-page.component';
import { RulesComponent } from './rules/rules.component';
import { ChatComponent } from './chat/chat.component';
import { ExpandedTableComponent } from './expanded-table/expanded-table.component';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  imports: [
    CommonModule,
    CustomMaterialModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
    FontAwesomeModule,
    MatPaginatorModule,
    SharedModule
  ],
  declarations: [
    TablePageComponent,
    RulesComponent,
    ChatComponent,
    ExpandedTableComponent,
    SearchTableComponent,
    EmptyListComponent,
],
  exports: [
    TablePageComponent,
    RulesComponent,
    ChatComponent,
    ExpandedTableComponent,
    SearchTableComponent,
    EmptyListComponent,
    MatPaginatorModule,
    FontAwesomeModule,
    SharedModule
  ]
})
export class ComponentsModule {
}
