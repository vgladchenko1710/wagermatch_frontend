import { HttpClient } from '@angular/common/http';
import { SocketIoService } from './socket-io.service';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  public getMessages$ = new Subject();
  public getTypingStatus$ = new Subject();
  public getReadStatus$ = new Subject();
  public getMatchStatus$ = new Subject();

  public getPaymentEvent$ = new Subject();

  constructor(public socketIoService: SocketIoService,
              private httpClient: HttpClient) {}

    reconnect() {
      this.socketIoService.reconnect();
    }

    connect() {
      this.socketIoService.connect();
    }

    subscribeOnAll() {
      this.getMessage();
      this.getTyping();
      this.getReadStatus();
      this.getMatchStatus();
    }

    disconnect() {
      this.socketIoService.disconnect();
    }

    sendSocketMessage(messageData) {
      this.socketIoService.sendMessage(messageData);
    }

    getMessage() {
      this.socketIoService.getMessage()
        .subscribe((mes) => {
          this.getMessages$.next(mes);
      });
    }

    sendPaymentEvent(amount: number) {
      this.getPaymentEvent$.next(amount);
    }


    sendTypingStatus(isTyping) {
      this.socketIoService.sendTypingStatus(isTyping);
    }

    getTyping() {
      this.socketIoService.getTyping()
      .subscribe((status) => {
        this.getTypingStatus$.next(status);
      });
    }

    sendReadStatus(matchUid) {
      this.socketIoService.sendReadStatus(matchUid);
    }

    getReadStatus() {
      this.socketIoService.getReadStauts()
      .subscribe(() => {
        this.getReadStatus$.next();
      });
    }

    sendMatchStatus(newStatus) {
      this.socketIoService.sendMatchStatus(newStatus);
    }

    getMatchStatus() {
      this.socketIoService.getMatchStatus()
        .subscribe((status => {
          this.getMatchStatus$.next(status);
      }));
    }

    getNotification(userUid: string) {
      return this.httpClient.get(`${environment.apiUrl}/matches/notification/${userUid}`);
    }

    getNotificationMessage(userUid: string) {
      return this.httpClient.get(`${environment.apiUrl}/matches/notification-message/${userUid}`);
    }

    readNotification(userUid: string) {
      return this.httpClient.put(`${environment.apiUrl}/matches/notification/${userUid}`, {});
    }

    sendMessage(userUid: string, matchUid: string, message: any) {
      return this.httpClient.post(`${environment.apiUrl}/matches/chat/${userUid}/${matchUid}`, message);
    }

    getMessages(userUid: string, matchUid: string) {
      return this.httpClient.get(`${environment.apiUrl}/matches/chat/${userUid}/${matchUid}`);
    }

    changeReadStatus(userUid: string, messageUid: string) {
      return this.httpClient.put(`${environment.apiUrl}/matches/readMessage/${userUid}/${messageUid}`, {});
    }

}
