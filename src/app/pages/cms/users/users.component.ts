import { InfoComponent } from './../../../_dialogs/info/info.component';
import { MatDialog } from '@angular/material/dialog';
import { ToastService } from './../../../_services/toast.service';
import { Router } from '@angular/router';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { UsersService } from './../../../_services/users.service';
import { Subscription, BehaviorSubject } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public clearSelected$ = new BehaviorSubject<boolean>(false);
  public isSuperAdmin = false;
  public users = [];
  public selectedUsersUid = [];
  public selectedUsers;
  public actualButton = '';
  public actualAdminButton = '';
  public load = false;
  public totalRows = 0;
  public status = 'all';
  public paginationData = {
    pageIndex: 0,
    pageSize: 30,
    orderBy: '',
    orderDirection: '',
    filterValue: '',
  };
  public statuses = [
    {
      value: 'all', viewValue: 'All'
    },
    {
      value: 'Active', viewValue: 'Active'
    },
    {
      value: 'inActive', viewValue: 'Inactive'
    },
    {
      value: 'Blocked', viewValue: 'Blocked'
    }
  ];
  public columns = [
    {
      fieldName: 'login',
      title: 'Логин',
    },
    {
      fieldName: 'email',
      title: 'Email',
    },
    {
      fieldName: 'registrationDate',
      title: 'Дата Рег.',
    },
    {
      fieldName: 'balanceAmount',
      title: 'Баланс',
    },
    {
      fieldName: 'winGame',
      title: 'Побед',
    },
    {
      fieldName: 'loseGame',
      title: 'Поражений',
    },
    {
      fieldName: 'lostConflicts',
      title: 'Проигр. Конфликтов',
    },
    {
      fieldName: 'isAdmin',
      title: 'Permission',
      extraValue: 'isAdmin'
    },
    {
      fieldName: 'status',
      title: 'Status',
      extraValue: 'status'
    },
  ];

  constructor(public usersService: UsersService,
              public dialog: MatDialog,
              public router: Router,
              public toastService: ToastService,
              public authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.isSuperAdmin = this.authUserService.isSuperAdmin();
    this.getList();
  }

  paginationUpdate(event) {
    switch (event.type) {
      case 'page':
        this.paginationData.pageIndex = event.data.pageIndex;
        this.paginationData.pageSize = event.data.pageSize;
        break;
      case 'sort':
        this.paginationData.orderBy = event.data.active;
        this.paginationData.orderDirection = event.data.direction;
        break;
      case 'filter':
        this.paginationData.filterValue = event.data;
        break;
    }
    this.getList();
  }

  statusUpdate(status) {
    this.status = status;
    this.getList();
  }

  getList() {
    this.load = true;
    const rowsFrom = this.paginationData.pageSize * (this.paginationData.pageIndex + 1) - this.paginationData.pageSize + 1;
    const rowsTo = this.paginationData.pageSize * (this.paginationData.pageIndex + 1);
    this.subscription.add(this.usersService.getUsersList(
      this.authUserService.getUserUid(),
      rowsFrom,
      rowsTo,
      this.paginationData.orderBy,
      this.paginationData.orderDirection,
      this.paginationData.filterValue,
      this.status)
      .subscribe ((response: any) => {
        this.totalRows = response.result.totalRows;
        this.users = response.result.userList;
        this.load = false;
    }));
  }

  openUser = (event: any) => {
    if (event.column === 'login') {
      this.router.navigate(['cms/user-profile', event.row.uid]);
    }
  }

  selectUsers = (users: any) => {
    const selectedUid = [];
    users.forEach((user => { selectedUid.push(user.uid)}));
    this.selectedUsersUid = selectedUid;
    this.selectedUsers = users;
    this.checkStatus();
    this.checkAdminPermission();
  }

  checkStatus() {
    console.log(this.selectedUsers);
    const isActive = this.selectedUsers.some(user => user.status === 'Active');
    const isBlocked = this.selectedUsers.some(user => user.status === 'Blocked');
    if (isActive && !isBlocked) {
      this.actualButton = 'deactivate';
    } else if (!isActive && isBlocked) {
      this.actualButton = 'activate';
    } else {
      this.actualButton = '';
    }
  }

  checkAdminPermission() {
    if (this.selectedUsers.length === 1) {
      this.selectedUsers[0].isAdmin ? this.actualAdminButton = 'notAdmin' : this.actualAdminButton = 'isAdmin';
    } else {
      this.actualAdminButton = '';
    }
  }

  changeAdminStatus(newStatus) {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: `Ты действительно хочешь ` + (newStatus ?  `сделать  АДМИНОМ ${this.selectedUsers[0].login} ?` : `убрать статус АДМИНА для ${this.selectedUsers[0].login} ?`),
              infoText: '',
              btn: `Вернуться назад`,
              btnOrange: newStatus ? `Сделать Админом` : 'Убрать Админа'
            },
    });
    this.subscription.add(infoDialog.afterClosed()
      .subscribe(result => {
        console.log(result);
        if (result === 'btnOrange') {
          const data = {
            isAdmin: newStatus
          };
          this.clearSelected$.next(false);
          this.actualAdminButton = '';
          this.subscription.add(this.usersService.changeAdminStatus(this.authUserService.getUserUid(), this.selectedUsersUid[0], data)
            .subscribe ((response: any) => {
              this.toastService.success(response.message);
              this.getList();
              }, (err) => {
                this.toastService.warning(err.error.errors[0].message);
          }));
        }
      }));
  }

  deactivateUsers(newStatus) {
    const data = {
      userUidLists: this.selectedUsersUid,
      status: newStatus
    };
    this.clearSelected$.next(false);
    this.actualButton = '';
    this.subscription.add(this.usersService.deactivateUsers(this.authUserService.getUserUid(), data)
      .subscribe ((response: any) => {
        this.toastService.success(response.message);
        this.getList();
        }, (err) => {
          this.toastService.warning(err.error.errors[0].message);
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
