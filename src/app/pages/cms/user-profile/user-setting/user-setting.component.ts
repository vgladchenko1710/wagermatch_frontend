import { Auth } from './../../../../_services/auth/auth.enum';
import { UsersService } from './../../../../_services/users.service';
import { ToastService } from './../../../../_services/toast.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-setting',
  templateUrl: './user-setting.component.html',
  styleUrls: ['./user-setting.component.scss']
})
export class UserSettingComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  private uid = '';
  public isLoad = false;
  public isVk = false;

  constructor(private authUserService: AuthUserService,
              public toastService: ToastService,
              public router: Router,
              public route: ActivatedRoute,
              private usersService: UsersService) {
              }

  ngOnInit(): void {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.uid = params.get('userUid');
        this.createForm();
        this.getUserSettings();
    }));
  }

  getUserSettings() {
    this.subscription.add(this.usersService.getUsersSetting(this.uid)
      .subscribe((response: any) => {
        const user = response.result;
        this.updateValues(user);
        this.form.markAllAsTouched();
      },  (err) => {
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  createForm() {
    this.form = new FormGroup({
      login: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      email: new FormControl({value: null, disabled: true}),
      vkID: new FormControl({value: null, disabled: true}),
      platform: new FormControl('', [Validators.required]),
      gamertag: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
    });
  }

  get platform() {
    return this.form.get('platform').value;
  }

  updateValues(user: any) {
    if (user.vkID) {
      this.isVk = true
    }
    this.form.patchValue({
      login: user.login,
      email: user.email,
      vkID: user.vkID,
      platform: user.platform ? user.platform : '',
      gamertag: user.gamertag
    });
  }

    saveValues() {
    this.isLoad = true;
    const user = {
      login : this.form.get('login').value,
      platform : this.platform,
      gamertag: this.form.get('gamertag').value
    };
    this.subscription.add(this.usersService.setUserSetting(this.uid, user)
      .subscribe((response: any) => {
        this.toastService.success(response.message);
      },  (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
