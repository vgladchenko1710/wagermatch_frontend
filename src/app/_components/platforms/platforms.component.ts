import { ChoosePlatformComponent } from './choose-platform/choose-platform.component';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { faXbox, faPlaystation } from '@fortawesome/fontawesome-free-brands';

@Component({
  selector: 'app-platforms',
  templateUrl: './platforms.component.html',
  styleUrls: ['./platforms.component.scss']
})
export class PlatformsComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();
  @Input() actualPlatform = '';
  @Input() theme = '';
  @Input() disabled: boolean;
  @Output() updatePlatform = new EventEmitter();
  public XboxIcon = faXbox;
  public Ps4Icon = faPlaystation;


  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  choosePlatform(model: string) {
    const platformDialog = this.dialog.open(ChoosePlatformComponent, {
      backdropClass: 'dialog-back',
      width: '500px',
      data: {model, platform: this.actualPlatform}
    });
    this.subscription.add(platformDialog.afterClosed()
      .subscribe(result => {
        if (result) {
          this.updatePlatform.emit(result);
        }
    }));
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
