import { Title, Meta } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { helpList } from '../help-text';

@Component({
  selector: 'app-helpers',
  templateUrl: './helpers.component.html',
  styleUrls: ['./helpers.component.scss']
})
export class HelpersComponent implements OnInit {
  public subscription = new Subscription();
  public helpersList = helpList();
  public activeHelper;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private titleService: Title,
              private meta: Meta) {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        const ruleName = params.get('helperName');
        this.findRule(ruleName);
    }));
  }

  ngOnInit(): void {
  }

  findRule(ruleName) {
    this.activeHelper = this.helpersList.find(rule => rule.route === ruleName);
    if (!this.activeHelper) {
      this.router.navigate(['not-found']);
    }
    this.titleService.setTitle(`Помощь: ${this.activeHelper.title}`+' ‣Wagermatch');
    this.meta.updateTag({name: 'description', content: this.activeHelper.description})
  }

}
