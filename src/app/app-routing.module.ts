import { NotFoundComponent } from './pages/not-found/not-found.component';
import { BlockedApiGuard } from './_services/controllers/blockedApi.guard';
import { ApiGuard } from './_services/controllers/api.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
    canActivate: [ApiGuard]
  },
  {
    path: '**', component: NotFoundComponent
  },
];

const config: ExtraOptions = {
  useHash: false,
  scrollPositionRestoration: 'top',
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
