import { ShowPlatformComponent } from './../_components/platforms/show-platform/show-platform.component';
import { ChoosePlatformComponent } from './../_components/platforms/choose-platform/choose-platform.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PlatformsComponent } from './../_components/platforms/platforms.component';
import { WagersComponent } from './../_components/wagers/wagers.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    WagersComponent,
    PlatformsComponent,
    ShowPlatformComponent,
    ChoosePlatformComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    WagersComponent,
    PlatformsComponent,
    ShowPlatformComponent,
  ]
})
export class SharedModule { }
