import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private httpClient: HttpClient) { }


  sendSourceLog(data: ISourceLogBody) {
    return this.httpClient.post(`${environment.apiUrl}/logs/redirect`, data);
  }
}

export interface ISourceLogBody {
  action: string;
  sourceLink: string;
}

export enum LogActionEnum {
  ENTRY = 'entry',
  REGISTER = 'register'
}
