import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-guard-info',
  templateUrl: './guard-info.component.html',
  styleUrls: ['./guard-info.component.scss']
})
export class GuardInfoComponent implements OnInit {

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<GuardInfoComponent>) {
               }

  ngOnInit(): void {
  }

}
