import { ToastService } from './../../../../../_services/toast.service';
import { UsersService } from './../../../../../_services/users.service';
import { AuthUserService } from './../../../../../_services/auth/auth-user.service';
import { fade } from './../../../../../_animations/animations';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Component, OnInit, Inject, OnDestroy, ViewChild } from '@angular/core';

@Component({
  selector: 'app-user-payout',
  templateUrl: './user-payout.component.html',
  styleUrls: ['./user-payout.component.scss'],
  animations: [fade]
})
export class UserPayoutComponent implements OnInit, OnDestroy {

private subscription: Subscription = new Subscription();
@ViewChild('ccNumber', { static: false }) ccNumber;
public form: FormGroup;
public cardType;
public minPayout = 200;
public maxPayout = 100000;
public isLoad = false;

public payTypes = [
  {value: 'card', viewValue: 'Банковская карта', icon: '../../../../../assets/MyProfile/card.png'},
  {value: 'QIWI', viewValue: 'QIWI', icon: '../../../../../assets/MyProfile/qiwi.png'},
  {value: 'YandexMoney', viewValue: 'Яндекс.Деньги', icon: '../../../../../assets/MyProfile/yandex.png'},
  {value: 'WebMoney', viewValue: 'WebMoney WMR', icon: '../../../../../assets/MyProfile/webmoney.png'}
];
public actualPayType = {
  viewValue: '',
  icon: ''
};
  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              private authUserService: AuthUserService,
              private usersService: UsersService,
              private toastService: ToastService,
              public dialogRef: MatDialogRef<UserPayoutComponent>) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      payType: new FormControl(null, [Validators.required]),
      payData: new FormControl(null, [Validators.required]),
      amount: new FormControl(null, [Validators.required,  Validators.max(this.maxPayout), Validators.min(this.minPayout)])
    });
    this.subscription.add(this.form.get('payType')
      .valueChanges
      .subscribe(() => {
        this.payTypes.find(payType => {
          if (payType.value === this.form.get('payType').value) {
            this.actualPayType.viewValue = payType.viewValue;
            this.actualPayType.icon = payType.icon;
          }
        });
    }));
    this.subscription.add(this.form.get('payData')
    .valueChanges
    .subscribe(() => {
      this.cardType = this.ccNumber.resolvedScheme$._value;
    }));
  }

  toPayout() {
    this.isLoad = true;
    const uid = this.data.userUid;
    const data = {
      payType: this.form.get('payType').value,
      payData: this.form.get('payData').value,
      amount: this.form.get('amount').value,
    };
    this.subscription.add(this.usersService.payouRequest(uid, data)
    .subscribe ((response: any) => {
      this.toastService.success(response.message);
      setTimeout(() => {
        this.dialogRef.close(true);
      }, 600);
      }, (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
