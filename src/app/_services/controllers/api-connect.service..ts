import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { Subscription, Observable, Subject } from 'rxjs';
import { GetMatch } from '../../_models/match';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiConnectService {
  public subscription = new Subscription();
  public connect = true;
  public counter = 1;
  public getApiStatus$ = new Subject();
  public request: GetMatch = {
    page: 1,
    pageSize: 7,
    platform: 'all',
    bidFrom: 0,
    bidTo: 10000,
    sort: {isDateAsc: false, isBidAsc: false, isFirstData: true}
  };
  constructor(private httpClient: HttpClient,
              public router: Router) {}

  getMatchlist() {
    console.log('try to connect ...');
    this.subscription.add(this.httpClient.get(`${environment.apiUrl}/cmsPublic/matchList?page=${this.request.page}&pageSize=${this.request.pageSize}&isDateAsc=${this.request.sort.isDateAsc}&isBidAsc=${this.request.sort.isBidAsc}&isFirstDate=${this.request.sort.isFirstData}&platform=${this.request.platform}&bidFrom=${this.request.bidFrom}&bidTo=${this.request.bidTo}`)
      .subscribe((res) => {
        this.connect = true;
        this.getApiStatus$.next(true);
        console.log('successful connection');
        this.subscription.unsubscribe();
      }, (err) => {
        if (this.counter < 3) {
          setTimeout(() => this.getMatchlist(), 1000);
          this.counter++;
        } else {
          this.connect = false;
          this.router.navigate(['fix']);
          console.log('error connection');
          this.getApiStatus$.next(false);
          this.subscription.unsubscribe();
        }
      }));
  }

  getApiStatus() {
    return this.connect;
  }

};
