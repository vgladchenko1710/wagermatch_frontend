import { Title, Meta } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { faqList } from './faq-text';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  public faqList = faqList();

  constructor(private titleService: Title,
              private meta: Meta) {
      this.titleService.setTitle(`Часто задаваемые вопросы`+' ‣Wagermatch');
      this.meta.updateTag({name: 'description', content: 'Список частых вопросов. Учавствие в матчах, споры, блокировки аккаунтов и прочие.'})
    }

  ngOnInit(): void {
  }

}
