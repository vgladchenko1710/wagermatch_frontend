import { SharedModule } from './../../_shared/shared.module';
import { ComponentsModule } from './../../_components/components.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomMaterialModule } from './../../@theme/custom-material.module';
import { MyProfileComponent } from './my-profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { SettingComponent } from './setting/setting.component';
import { ProfileMenuComponent } from './profile-menu/profile-menu.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { FinanceComponent } from './finance/finance.component';
import { ArbitrazComponent } from './arbitraz/arbitraz.component';
import { PayoutComponent } from './finance/payout/payout.component';
import { RechargeComponent } from './account/recharge/recharge.component';
import { CreditCardDirectivesModule } from 'angular-cc-library';




@NgModule({
  declarations: [
    MyProfileComponent,
    AccountComponent,
    SettingComponent,
    ProfileMenuComponent,
    ChangePasswordComponent,
    FinanceComponent,
    ArbitrazComponent,
    PayoutComponent,
    RechargeComponent
  ],
  imports: [
    CustomMaterialModule,
    ComponentsModule,
    CommonModule,
    NgbPaginationModule,
    ProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RoundProgressModule,
    CreditCardDirectivesModule,
    SharedModule
  ],
  providers: [
  ],
  entryComponents: [
    PayoutComponent
  ]
})
export class MyProfileModule {}
