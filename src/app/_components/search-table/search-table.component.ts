import { Pagination } from './../../_models/pagination';
// import { UsersFilterComponent } from './../../_dialogs/users-filter/users-filter.component';
import { BehaviorSubject, Subscription, Subject } from 'rxjs';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, Input, ViewChild, OnChanges, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { debounceTime } from 'rxjs/operators';

const DEBOUNCE_TIME = 600;

@Component({
  selector: 'app-search-table',
  templateUrl: './search-table.component.html',
  styleUrls: ['./search-table.component.scss']
})
export class SearchTableComponent implements OnInit, OnChanges, OnDestroy {
  @Input() data: any = [];
  @Input() columns: any = {};
  @Input() onRowClick: (event: any) => any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @Input() isLoading: boolean;
  @Input() selectedRows: (event: any) => any;
  @Input() clearSelected: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  @Input() useSelect: boolean;
  @Input() useNumbers: boolean;
  @Input() useFilters = true;
  @Input() useSearch: boolean;
  @Input() useButtonsSelect: boolean;
  @Input() dateFilterData;
  @Input() dropdownOptions;
  @Input() dropdownValue;
  @Input() paginationData: Pagination;
  @Input() buttonsSelect;
  @Input() buttonsSelectValue;
  @Input() paginationSizeOptions = [30, 50, 100, 200];
  @Input() totalRows;
  @Input() usePagination: boolean;
  @Output() usersFilterUpdate = new EventEmitter();
  @Output() dateFilter = new EventEmitter();
  @Output() paginationUpdate = new EventEmitter();
  @Output() dropdownUpdate = new EventEmitter();
  @Output() buttonsSelectUpdate = new EventEmitter();
  @Output() elementClick = new EventEmitter();
  public subscription = new Subscription();
  public searchChanged: Subject<any> = new Subject<any>();
  public dataSource: any;
  public displayedColumns: string[] = [];
  public userFilterData = {
    useFilter: false,
    filterValues: null
  };
  selection = new SelectionModel(true, []);

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
    this.selectedRows(this.selection.selected);
  }
  toggleRow(row) {
    this.selection.toggle(row);
    this.selectedRows(this.selection.selected);
  }
  checkboxLabel(row?): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  constructor(public dialog: MatDialog) {
    this.searchChanged
      .pipe(debounceTime(DEBOUNCE_TIME))
      .subscribe((model: any) => {
        this.updatePagination(model, 'filter');
    });
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
    if (this.usePagination) {
      this.paginationData.pageSize = this.paginationSizeOptions[0];
    }
    this.clearSelected.subscribe((event: any) => {
      this.selection.clear();
    });
  }

  textFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    if (this.usePagination) {
      this.searchChanged.next(filterValue );
    } else {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }

  updateDropdown(value: string) {
    this.dropdownUpdate.emit(value);

  }

  updatePagination(data, type: 'page'|'sort'|'filter') {
    switch (type) {
      case 'page':
        this.paginationData.pageIndex = data.pageIndex;
        this.paginationData.pageSize = data.pageSize;
        break;
      case 'sort':
        this.paginationData.orderBy = data.active;
        this.paginationData.orderDirection = data.direction;
        break;
      case 'filter':
        this.paginationData.filterValue = data;
        break;
    }
    this.paginationUpdate.emit({data, type});
  }


  dateUpdate(event) {
    this.dateFilter.emit(this.dateFilterData);
  }


  ngOnChanges() {
    if (!this.totalRows) {
      this.totalRows = this.paginationData.totalItems;
    }
    const columns = [];
    if (this.useSelect) {
      columns.push('select');
    }
    if (this.useNumbers) {
      columns.push('number');
    }
    for (const column of this.columns) {
      columns.push(column.fieldName);
    }
    this.dataSource = new MatTableDataSource(this.data);
    this.displayedColumns = columns;
    !this.usePagination ? this.dataSource.sort = this.sort : null;
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
