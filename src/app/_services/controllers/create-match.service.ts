import { LoginComponent } from './../../_dialogs/login/login.component';
import { JoinMatchComponent } from '../../_dialogs/join-match/join-match.component';
import { InfoComponent } from '../../_dialogs/info/info.component';
import { Subscription } from 'rxjs';
import { MatchesService } from './../matches.service';
import { ToastService } from './../toast.service';
import { Router } from '@angular/router';
import { AuthUserService } from './../auth/auth-user.service';
import { MatDialog } from '@angular/material/dialog';
import { Injectable, OnDestroy } from '@angular/core';
import { CreateMatchComponent } from 'src/app/_dialogs/create-match/create-match.component';

@Injectable({
  providedIn: 'root'
})
export class CreateMatchService implements OnDestroy {
  public subscription: Subscription = new Subscription ();
  public activePlatform = '';

  constructor(private dialog: MatDialog,
              public router: Router,
              public toastService: ToastService,
              private authUserService: AuthUserService) {
                this.activePlatform = this.authUserService.getUserPlatform();
               }

  checkStatus(matchUid?: string, matchPlarform?: string, winAmount?: any, withAlly?: any) {
    this.activePlatform = this.authUserService.getUserPlatform();
    if (this.authUserService.isAuthenticated()) {
      if (this.authUserService.isActive()) {
        matchUid ? this.checkPlatform(matchUid, matchPlarform, winAmount, withAlly) : this.openCreateMatch();
      } else {
        this.router.navigate(['my-profile/setting']);
        this.toastService.error('Выбери платформу и впиши никнейм, чтобы учавствовать в матчах!');
      }
    } else {
      matchUid ? this.openLogin('Присоедениться к матчу') : this.openLogin('Создать матч');
    }
  }

  checkPlatform(matchUid, matchPlarform, winAmount, withAlly) {
    if (this.activePlatform === matchPlarform) {
      this.openJoinToMatch(matchUid, winAmount, withAlly);
    } else {
      const infoDialog = this.dialog.open(InfoComponent, {
        disableClose: true,
        backdropClass: 'dialog-back',
        width: '600px',
        data: { head: 'Платформа выбранного матча не совпадает с указанной вами платформой!',
                infoText: 'Выберите другой матч или поменяйте платформу в Личном Кабинете',
                btn: 'Поменять платформу',
                btnOrange: 'Выбрать другой матч'
              },
      });
      this.subscription.add(infoDialog.afterClosed()
        .subscribe(result => {
          if (result === 'btnOrange') {
            this.router.navigate(['matches/all-matches']);
          } else if (result === 'btn') {
            this.router.navigate(['my-profile/setting']);
          }
        }));
    }
  }

  openCreateMatch() {
    this.dialog.open(CreateMatchComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
  }



  openJoinToMatch(matchUid: string, winAmount: number, withAlly: boolean) {
    this.dialog.open(JoinMatchComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: {matchUid, winAmount, withAlly}
    });
  }

  openLogin(loginGuardMesssage: string) {
    this.dialog.open(LoginComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {loginGuardMesssage}
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
