import { NgxImageCompressService } from 'ngx-image-compress';
import { MatchesService } from './../../_services/matches.service';
import { ToastService } from './../../_services/toast.service';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject, ElementRef, ViewChild } from '@angular/core';

const MAX_SIZE = 5 * 1024 * 1024; // 10MB

@Component({
  selector: 'app-result-match',
  templateUrl: './result-match.component.html',
  styleUrls: ['./result-match.component.scss']
})
export class ResultMatchComponent implements OnInit {
  @ViewChild('FileInput', { static: false }) FileInput: ElementRef;
  public file: any;
  public fileName: string;
  public isWin: boolean;
  public isLoad = false;
  public isImageLoading = false;

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              public dialogRef: MatDialogRef<ResultMatchComponent>,
              public toastService: ToastService,
              public matchesService: MatchesService,
              private imageCompress: NgxImageCompressService,
              private authUserService: AuthUserService) { }

  ngOnInit(): void {
    this.isWin = this.data.isWin;
  }

  selectFile(event: any) {
    this.fileName = event.target.files[0]?.name;
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (item: any) => {
        const localUrl = item.target.result;
        this.compressFile(localUrl);
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  compressFile(image) {
    this.isImageLoading = true;
    this.imageCompress.compressFile(image, -1, 50, 50).then(
    result => {
      this.file = this.dataURItoBlob(result.split(',')[1]);
      this.isImageLoading = false;
    });
  }
  dataURItoBlob(dataURI) {
    const byteString = window.atob(dataURI);
    const arrayBuffer = new ArrayBuffer(byteString.length);
    const int8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < byteString.length; i++) {
    int8Array[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([int8Array], { type: 'image/jpeg' });
    return blob;
  }

  sendResult(result: string) {
    this.isLoad = true;
    const fd = new FormData();
    fd.append('result', result);
    if (this.isWin) {
      fd.append('img', this.file);
    }
    this.matchesService.sendResult(this.authUserService.getUserUid(), this.data.matchUid, fd)
      .subscribe((res: any) => {
        this.toastService.success(res.message);
        setTimeout( () => {
          this.dialogRef.close(true);
        }, 1000);
    }, (err) => {
      this.isLoad = false;
      this.toastService.warning(err.error.errors[0].message);
    });
  }

  clickInput() {
    this.FileInput.nativeElement.click();
  }

}
