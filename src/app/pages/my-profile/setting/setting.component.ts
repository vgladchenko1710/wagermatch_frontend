import { MatchStatusEnum } from './../../matches/match.model';
import { MatchesService } from './../../../_services/matches.service';
import { Router } from '@angular/router';
import { Auth } from './../../../_services/auth/auth.enum';
import { ToastService } from './../../../_services/toast.service';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { UsersService } from './../../../_services/users.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faXbox, faPlaystation } from '@fortawesome/fontawesome-free-brands';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  private uid = '';
  public isLoad = false;
  public isVk = false;

  constructor(private authUserService: AuthUserService,
              public toastService: ToastService,
              public router: Router,
              private usersService: UsersService,
              private matchesService: MatchesService) {
                this.createForm();
                this.uid = this.authUserService.getUserUid();
                this.isVk = this.authUserService.isVk();
                this.getUserSettings();
                this.getUserMatches();
              }

  ngOnInit(): void {
  }

  getUserSettings() {
    this.subscription.add(this.usersService.getUsersSetting(this.uid)
      .subscribe((response: any) => {
        const user = response.result;
        this.updateValues(user);
        this.form.markAllAsTouched();
      },  (err) => {
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  getUserMatches() {
    this.subscription.add(this.matchesService.getMyMatches(this.uid, 1, 5, false, false, true, MatchStatusEnum.FINISHED)
    .subscribe(response => {
      const matchList: any[] = response['matchList'];
      if (matchList?.length>0) {
        this.form.get('login').disable();
      }
    }));
  }

  createForm() {
    this.form = new FormGroup({
      login: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      email: new FormControl({value: null, disabled: true}),
      vkID: new FormControl({value: null, disabled: true}),
      platform: new FormControl('', [Validators.required]),
      gamertag: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
    });
  }

  get platform() {
    return this.form.get('platform').value;
  }

  updateValues(user: any) {
    this.form.patchValue({
      login: user.login,
      email: user.email,
      vkID: user.vkID,
      platform: user.platform ? user.platform : '',
      gamertag: user.gamertag
    });
  }
  saveValues() {
    this.isLoad = true;
    const user = {
      login : this.form.get('login').value,
      platform : this.platform,
      gamertag: this.form.get('gamertag').value
    };
    this.subscription.add(this.usersService.setUserSetting(this.uid, user)
      .subscribe((response: any) => {

        const newUser = JSON.parse(localStorage.getItem(Auth.KEY));
        newUser.login = user.login;
        newUser.platform = user.platform;
        newUser.gamertag = user.gamertag;
        localStorage.setItem(Auth.KEY, JSON.stringify(newUser));

        this.toastService.success(response.message);
        setTimeout(() => {
          this.authUserService.isActive() ?
            window.location.href = window.location.protocol + '/my-profile/setting' :
            window.location.href = window.location.protocol + '/matches/all-matches';
        }, 1500);
      },  (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
