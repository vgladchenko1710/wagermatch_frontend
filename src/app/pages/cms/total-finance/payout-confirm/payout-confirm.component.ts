import { IAdminOutcome } from './../total-finance.component';
import { AdminService } from './../../_cms-services/admin.service';
import { InfoComponent } from './../../../../_dialogs/info/info.component';
import { ToastService } from './../../../../_services/toast.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-payout-confirm',
  templateUrl: './payout-confirm.component.html',
  styleUrls: ['./payout-confirm.component.scss']
})
export class PayoutConfirmComponent implements OnInit {
  public isLoad = false;
  private adminUid = '';
  public isCancel: boolean;
  public cancelDescriptions = [
    'Некоректный платежные данные.',
    'Нарушение регламент сайта, а именно п.'
  ];
  public cancelDescription = '';

  constructor(@Inject (MAT_DIALOG_DATA) public data: IAdminOutcome,
              private authUserService: AuthUserService,
              private adminService: AdminService,
              public toastService: ToastService,
              public dialog: MatDialog,
              public dialogRef: MatDialogRef<PayoutConfirmComponent>) { }

  ngOnInit(): void {
    this.adminUid = this.authUserService.getUserUid();
  }

  acceptPayout() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: `Ты действительно хочешь потвердить выплату ?`,
              infoText: '',
              btn: `Вернуться назад`,
              btnOrange: `Потвердить`
            },
    });
    infoDialog.afterClosed()
    .subscribe(result => {
      if (result === 'btnOrange') {
        this.isLoad = true;
        const request = {
          uid : this.data.uid,
          isSuccesfully  : true,
          description   : `Вывод средств прошел успешно. Номер: ${this.data.uid}`
        };
        this.adminService.changePaymentStatus(this.adminUid, request)
          .subscribe ((response: any) => {
          this.toastService.success(response.message);
          this.isLoad = false;
          this.dialogRef.close(true);
          }, (err) => {
            this.isLoad = false;
            this.toastService.warning(err.error.errors[0].message);
        });
      }
    });
  }

  cancelPayout() {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: `Ты действительно хочешь отказать в выводе на выплату ?`,
              infoText: '',
              btn: `Вернуться назад`,
              btnOrange: `Отказать в выводе`
            },
    });
    infoDialog.afterClosed()
    .subscribe(result => {
      if (result === 'btnOrange') {
        const request = {
          uid : this.data.uid,
          isSuccesfully  : false,
          description   : `${this.cancelDescription} Номер: ${this.data.uid}`
        };
        this.adminService.changePaymentStatus(this.adminUid, request)
          .subscribe ((response: any) => {
          this.toastService.success(response.message);
          this.isLoad = false;
          this.dialogRef.close(true);
          }, (err) => {
            this.isLoad = false;
            this.toastService.warning(err.error.errors[0].message);
        });
      }
    });
  }


}
