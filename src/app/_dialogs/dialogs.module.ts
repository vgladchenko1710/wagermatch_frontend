import { ComponentsModule } from './../_components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoInfoComponent } from './promo-info/promo-info.component';
import { CustomMaterialModule } from '../@theme/custom-material.module';



@NgModule({
  declarations: [
    PromoInfoComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    ComponentsModule
  ]
})
export class DialogsModule { }
