import { advantages } from "./main-page.text";
import { Title, Meta } from "@angular/platform-browser";
import { MatchesService } from "./../../_services/matches.service";
import { AuthUserService } from "./../../_services/auth/auth-user.service";
import { GetMatch } from "./../../_models/match";
import { CreateMatchService } from "./../../_services/controllers/create-match.service";
import { Subscription } from "rxjs";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { faXbox, faPlaystation } from "@fortawesome/fontawesome-free-brands";

@Component({
  selector: "app-main-page",
  templateUrl: "./main-page.component.html",
  styleUrls: ["./main-page.component.scss"],
})
export class MainPageComponent implements OnInit, OnDestroy {
  @ViewChild("howItWorks", { static: false }) howItWorks: ElementRef;
  @ViewChild("advantages", { static: false }) advantages: ElementRef;
  private subscription: Subscription = new Subscription();
  public isLoading = false;
  public request: GetMatch = {
    page: 1,
    pageSize: 7,
    platform: this.authUserService.getUserPlatform(),
    bidFrom: 0,
    bidTo: 10000,
    sort: { isDateAsc: false, isBidAsc: false, isFirstData: true },
  };
  XboxIcon = faXbox;
  Ps4Icon = faPlaystation;
  public matchesList = [];
  show = false;
  step = [false, false, false, false];
  public advantagesList = advantages;

  constructor(
    private authUserService: AuthUserService,
    public matchesService: MatchesService,
    private router: Router,
    private route: ActivatedRoute,
    public createMatchService: CreateMatchService,
    private titleService: Title,
    private meta: Meta
  ) {
    this.titleService.setTitle(
      `Wagermatch — сервис для заключения пари в FIFA 21. Сыграй матч на вагеры на PlayStation 4, PlayStation 5 или Xbox One, Xbox Series X.` +
        " ‣Wagermatch"
    );
    // this.titleService.setTitle(`Wagermatch — это первый автоматизированный сервис для расчета матчей и турниров в FIFA 21.`+' ‣Wagermatch');
    this.meta.updateTag({
      name: "description",
      content:
        "Wagermatch дает возможность заключать пари в игре FIFA 21 для игроков со всего мира. Мы гарантируем полную безопасность и честность поединков в FIFA 21.",
    });
    //this.meta.updateTag({name: 'description', content: 'Wagermatch дает возможность соревноватся в FIFA 21 игрокам со всего мира. Мы гарантируем полную безопасность и честность поединков в FIFA 21.'})
  }

  showMore(stepId) {
    this.show = !this.show;
    if (stepId !== "close") {
      this.step[stepId] = !this.step[stepId];
    } else {
      // tslint:disable-next-line: forin
      for (const i in this.step) {
        this.step[i] = false;
      }
    }
  }
  ngOnInit() {
    this.getMatches();
    this.route.fragment.subscribe((fragment) => {
      fragment && this.toAnchor(fragment);
    });
  }

  getMatches() {
    this.isLoading = true;
    this.subscription.add(
      this.matchesService.getMatchlist(this.request).subscribe(
        (res: any) => {
          this.matchesList = res.matchList;
          this.isLoading = false;
        },
        (err) => {
          // this.router.navigate(['auth/fix']);
          this.isLoading = false;
        }
      )
    );
  }

  toAnchor(fragment) {
    const element =
      fragment === "howItWorks"
        ? this.howItWorks?.nativeElement?.offsetTop
        : this.advantages?.nativeElement?.offsetTop;
    setTimeout(() => {
      element > 0
        ? window.scroll({ top: element, behavior: "smooth" })
        : this.toAnchor(fragment);
    }, 200);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
