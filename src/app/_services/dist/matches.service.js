"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.MatchesService = void 0;
var environment_1 = require("../../environments/environment");
var core_1 = require("@angular/core");
var MatchesService = /** @class */ (function () {
    function MatchesService(httpClient) {
        this.httpClient = httpClient;
    }
    MatchesService.prototype.createMatch = function (userUid, match) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/create-match/" + userUid, match);
    };
    MatchesService.prototype.getMatchInfo = function (matchUid, userUid) {
        return this.httpClient.get(environment_1.environment.apiUrl + "/matches/match-info/" + matchUid + "?userUid=" + userUid);
    };
    MatchesService.prototype.editMatch = function (userUid, matchUid, match) {
        return this.httpClient.put(environment_1.environment.apiUrl + "/matches/edit-match/" + userUid + "/" + matchUid, match);
    };
    MatchesService.prototype.joinToMatch = function (userUid, matchUid) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/join-match/" + userUid + "/" + matchUid, {});
    };
    MatchesService.prototype.readyToMatch = function (userUid, matchUid, isReady) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/ready/" + userUid + "/" + matchUid, isReady);
    };
    MatchesService.prototype.startMatch = function (userUid, matchUid) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/start/" + userUid + "/" + matchUid, {});
    };
    MatchesService.prototype.dropFropMatch = function (userUid, matchUid) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/drop-from-match/" + userUid + "/" + matchUid, {});
    };
    MatchesService.prototype.changeEnemy = function (userUid, matchUid) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/change-enemy/" + userUid + "/" + matchUid, {});
    };
    MatchesService.prototype.deleteMatch = function (userUid, matchUid) {
        return this.httpClient["delete"](environment_1.environment.apiUrl + "/matches/delete-match/" + userUid + "/" + matchUid);
    };
    MatchesService.prototype.getMatchlist = function (request, userUid) {
        var finaluserUid = userUid ? userUid : '';
        return this.httpClient.post(environment_1.environment.apiUrl + "/cmsPublic/matchList?userUid=" + finaluserUid, request);
    };
    MatchesService.prototype.sendResult = function (userUid, matchUid, data) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/result/" + userUid + "/" + matchUid, data);
    };
    MatchesService.prototype.sendNoMatch = function (userUid, matchUid, data) {
        return this.httpClient.post(environment_1.environment.apiUrl + "/matches/draw/" + userUid + "/" + matchUid, data);
    };
    MatchesService.prototype.getMyMatches = function (userUid, page, pageSize, isDateAsc, isBidAsc, isFirstDate, status) {
        return this.httpClient.get(environment_1.environment.apiUrl + "/matches/my-matches/" + userUid + "?page=" + page + "&pageSize=" + pageSize + "&isDateAsc=" + isDateAsc + "&isBidAsc=" + isBidAsc + "&isFirstDate=" + isFirstDate + "&status=" + status);
    };
    MatchesService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], MatchesService);
    return MatchesService;
}());
exports.MatchesService = MatchesService;
