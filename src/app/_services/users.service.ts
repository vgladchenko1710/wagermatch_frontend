import { AuthUserService } from './auth/auth-user.service';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { InterkassaData } from '../pages/my-profile/account/recharge/interkassa-recharge';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private httpClient: HttpClient,
              private authUserService: AuthUserService) { }

  get userUid() {
    return this.authUserService.getUserUid();
  }

  getUsersSetting(uid: any) {
    return this.httpClient.get(`${environment.apiUrl}/users/myprofile/setting/${uid}`);
  }

  setUserSetting(uid: any, userData: any) {
    return this.httpClient.put(`${environment.apiUrl}/users/myprofile/setting/${uid}`, userData);
  }
  getUserAccountInfo(uid: any) {
    return this.httpClient.get(`${environment.apiUrl}/users/myprofile/account/${uid}`);
  }
  getTransaktionsList(uid: any) {
    return this.httpClient.get(`${environment.apiUrl}/users/myprofile/transactions/${uid}`);
  }
  toCheckPromo(promocode: string) {
    return this.httpClient.get(`${environment.apiUrl}/users/${this.userUid}/promocode/check/${promocode}`);
  }
  toRecharge(userUid: string, data: {price: number}) {
    return this.httpClient.post(`${environment.apiUrl}/payments/${userUid}`, data);
  }
  toRechargeFreeKassa(userUid: string, data: {price: number, description: string}) {
    return this.httpClient.post(`${environment.apiUrl}/payments/free-kassa-generate/${userUid}`, data);
  }
  toRechargeInterKassa(data: InterkassaData) {
    return this.httpClient.post(`${environment.apiUrl}/payments/start`, data);
  }
  payouRequest(uid: string, data: any) {
    return this.httpClient.post(`${environment.apiUrl}/users/payout/${uid}`, data);
  }
  changePassword(uid: any, password: any) {
    return this.httpClient.put(`${environment.apiUrl}/auth/password/${uid}`, password);
  }
  getConflicts(userUid: any) {
    return this.httpClient.get(`${environment.apiUrl}/users/${userUid}/myprofile/conflicts`);
  }
  getRates(userUid: string) {
    return this.httpClient.get(`${environment.apiUrl}/admin/${userUid}/settings-rate`);
  }
  // ADMIN
  getUsersList(adminUid: string, rowsFrom?: number, rowsTo? :number, orderBy?: string, orderDirection?: string, filterValue?: string, status?: string) {
    return this.httpClient.get(`${environment.apiUrl}/admin/${adminUid}/user-list?rowsFrom=${rowsFrom}&rowsTo=${rowsTo}&orderBy=${orderBy}&orderDirection=${orderDirection}&filterValue=${filterValue}&status=${status}`);
  }

  deactivateUsers(adminUid: string, data: any) {
    return this.httpClient.put(`${environment.apiUrl}/admin/${adminUid}/user-status`, data);
  }

  changeAdminStatus(adminUid: string, userUid: string, data: any) {
    return this.httpClient.put(`${environment.apiUrl}/admin/${adminUid}/make-admin/${userUid}`, data);
  }
}
