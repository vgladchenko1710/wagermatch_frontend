import { equalToFieldValue } from '../../../_services/errors/validators';
import { ToastService } from './../../../_services/toast.service';
import { AuthService } from './../../../_services/auth/auth.service';
import { LoginComponent } from './../login.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements OnInit {
  @ViewChild('password', { static: false }) password: ElementRef;
  @ViewChild('passwordRepeat', { static: false }) passwordRepeat: ElementRef;
  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  private key: string;
  public error = false;
  public access = false;
  public success = false;
  private showPassword = false;
  public passwordImage = '../../../assets/LoginRegister/show.svg';
  constructor(public router: Router,
              public authService: AuthService,
              private dialog: MatDialog,
              public toastService: ToastService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap
    .subscribe((params: ParamMap) => {
      this.key =  params.get('key');
      this.uidVerify();
    });
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      newPassword: new FormControl(null, [Validators.required, Validators.minLength(7), Validators.maxLength(50)]),
      confirmPassword: new FormControl(null, [Validators.required])
    });
    // password confirm
    this.subscription.add(this.form.get('newPassword')
    .valueChanges
    .subscribe(() => {
      const control = this.form.get('confirmPassword');
      control.setValidators([Validators.required, equalToFieldValue(this.form.get('newPassword').value)]);
      control.updateValueAndValidity();
    }));
    this.subscription.add(this.form.get('confirmPassword')
      .valueChanges
      .subscribe(() => {
        this.form.get('confirmPassword').setValidators([Validators.required, equalToFieldValue(this.form.get('newPassword').value)]);
      }));
  }


  uidVerify() {
    this.authService.passwordChangingAccess(this.key)
    .subscribe(response => {
        this.access = true;
        }, (err) => {
          this.toastService.error(err.error.errors[0].message);
          setTimeout(() => this.router.navigate([this.key]), 2000);
    });
  }

  passwordChange() {
    this.authService.passwordChange(this.key, {password: this.form.get('newPassword').value})
    .subscribe(response => {
        this.access = false;
        this.success = true;
        this.redirect();
        }, (err) => {
          this.toastService.error(err.error.errors[0].message);
    });
  }

  openLogin() {
    this.dialog.open(LoginComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
  }

  redirect() {
    setTimeout(() => {
      this.openLogin();
      this.router.navigate(['/']);
    }, 3000);
}

togglePaswordShown() {
  this.showPassword = !this.showPassword;
  if (this.showPassword) {
    this.password.nativeElement.setAttribute('type', 'text');
    this.passwordRepeat.nativeElement.setAttribute('type', 'text');
    this.passwordImage = '../../../assets/LoginRegister/show_open.svg';
  } else {
    this.password.nativeElement.setAttribute('type', 'password');
    this.passwordRepeat.nativeElement.setAttribute('type', 'password');
    this.passwordImage = '../../../assets/LoginRegister/show.svg';
  }
}

}
