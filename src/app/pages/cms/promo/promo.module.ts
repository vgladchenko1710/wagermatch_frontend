import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from './../../../@theme/custom-material.module';
import { PromoActionComponent } from './promo-action/promo-action.component';
import { PromoService } from './../_cms-services/promo.service';
import { ComponentsModule } from './../../../_components/components.module';
import { PromoRoutingModule } from './promo-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PromoComponent } from './promo.component';

@NgModule({
  imports: [
    CommonModule,
    PromoRoutingModule,
    ComponentsModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    PromoComponent,
    PromoActionComponent
  ],
  providers: [
    PromoService
  ]
})
export class PromoModule { }
