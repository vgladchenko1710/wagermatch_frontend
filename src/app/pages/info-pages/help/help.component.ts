import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { helpList } from './help-text';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
  public helpList = helpList();
  constructor(public router: Router,
              public route: ActivatedRoute,
              private titleService: Title,
              private meta: Meta) {
                this.titleService.setTitle(`Помощь`+' ‣Wagermatch');
                this.meta.updateTag({name: 'description', content: 'Помочь по пользованию в Wagermatch. Как создать матч в FIFA 21, проведение матчи, а так же почему возникают споры на Wagermatch.'})
              }

  ngOnInit(): void {
  }

  open(link) {
    this.router.navigate([`../${link}`],  {relativeTo: this.route});
  }

}
