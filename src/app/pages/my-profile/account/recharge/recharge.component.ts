import { PromoInfoComponent } from './../../../../_dialogs/promo-info/promo-info.component';
import { IPromo, PromoStatusEnum } from './../../../cms/promo/promo.model';
import { debounceTime } from 'rxjs/operators';
import { ISettingsRates } from './../../../cms/main-settings/main-settings.component';
import { InfoComponent } from './../../../../_dialogs/info/info.component';
import { fadeMix, fade } from './../../../../_animations/animations';
import { ToastService } from './../../../../_services/toast.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { UsersService } from './../../../../_services/users.service';
import { Subject, Subscription } from 'rxjs';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { formatDate } from '@angular/common';
import { createSendInterkassaRechargeForm, InterkassaData } from './interkassa-recharge';

const DEBOUNCE_TIME = 500;

@Component({
  selector: 'app-recharge',
  templateUrl: './recharge.component.html',
  styleUrls: ['./recharge.component.scss'],
  animations: [fade, fadeMix]
})
export class RechargeComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  public rates: ISettingsRates;
  public minRecharge = 50;
  public maxRecharge = 100000;

  public isRateLoaded: boolean;
  public isLoad: boolean;
  public isPromoLoading: boolean;
  public promoChanged$ = new Subject<string>();
  public promo: IPromo;
  PromoStatusEnum = PromoStatusEnum;

  public userUid = '';
  public actualPayType = {
    viewValue: '',
    icon: ''
  };
  public wagerRate: number = 1;
  public currency: 'руб.'|'грн';
  public payTypes = [
    {value: 'card_RUB', viewValue: 'Банковская карта (руб)', icon: '../../../../../assets/MyProfile/card.png'},
    {value: 'card_UAH', viewValue: 'Банковская карта (грн)', icon: '../../../../../assets/MyProfile/card.png'},
    {value: 'others', viewValue: 'Другое', icon: '../../../../../assets/MyProfile/others.png'},
  ];

  constructor(public dialogRef: MatDialogRef<RechargeComponent>,
              private authUserService: AuthUserService,
              public toastService: ToastService,
              private usersService: UsersService,
              private dialog: MatDialog) {
                this.userUid = this.authUserService.getUserUid();
                this.listenOnPromoChanges();
              }

  ngOnInit(): void {
    this.form = new FormGroup({
      payType: new FormControl(null, [Validators.required]),
      amount: new FormControl(0, [Validators.required, Validators.max(this.maxRecharge), Validators.min(this.minRecharge)]),
      promocode: new FormControl(null)
    });
    this.getRate();
    this.subscription.add(this.form.get('payType')
      .valueChanges
      .subscribe(() => {
        this.payTypes.find(payType => {
          if (payType.value === this.form.get('payType').value) {
            this.actualPayType.viewValue = payType.viewValue;
            this.actualPayType.icon = payType.icon;
          }
        });
        if (this.form.get('payType').value === 'card_UAH') {
          this.wagerRate = this.rates.hrnRate;
          this.currency = 'грн';
        } else {
          this.wagerRate = this.rates.rubRate;
          this.currency = 'руб.';
        }
        this.form.get('amount').patchValue(null);
        this.form.get('amount').setValidators([Validators.required, Validators.max(this.maxRecharge/this.wagerRate), Validators.min(this.minRecharge/this.wagerRate)]);
        this.form.get('amount').updateValueAndValidity();
    }));
  }


  getRate() {
    this.usersService.getRates(this.userUid)
      .subscribe ((response: any) => {
        this.rates = response.result;
        this.minRecharge = +response.result.minIncome;
        this.maxRecharge = +response.result.maxIncome;
        this.isRateLoaded = true;
        this.form.get('amount').setValidators([Validators.required,  Validators.max(this.maxRecharge), Validators.min(this.minRecharge)]);
    });
  }

  toRecharge() {
    switch(this.form.get('payType').value) {
      case('card_RUB'): this.checkPromoValidation();
        break;
      case('card_UAH'): this.checkPromoValidation();
        break;
      case('others'): this.freekassaRecharge();
        break;
    }
  }

  keyupPromo(promocode: string) {
    this.isPromoLoading = true;
    this.promoChanged$.next(promocode);
  }

  listenOnPromoChanges() {
    this.subscription.add(this.promoChanged$
      .pipe(debounceTime(DEBOUNCE_TIME))
      .subscribe((promocode: string) => {
        if (promocode) {
          this.checkPromo(promocode)
        } else {
          this.promo = null;
          this.isPromoLoading = false;
        }
    }));
  }

  checkPromo(promocode: string) {
    this.subscription.add(this.usersService.toCheckPromo(promocode)
    .subscribe ((res: any) => {
      this.isPromoLoading = false;
      this.promo = res.result;
      if (this.promo.status === PromoStatusEnum.ACTIVE) this.showActivePromoMessage(promocode);
    }, (err) => {
      this.toastService.error(err.error.errors[0].message);
      this.isPromoLoading = false;
    }));
    setTimeout(()=> this.isPromoLoading = false, 2000);
  }

  showActivePromoMessage(promoCode: string) {
    this.dialog.open(PromoInfoComponent, {
      disableClose: false,
      backdropClass: 'dialog-back',
      width: '500px',
      data: {...this.promo, code: promoCode}
    });
  }

  checkPromoValidation() {
    const payData: InterkassaData = {
      userUid: this.userUid,
      paymentId: formatDate(new Date(), 'MMddhhmmss', 'en'),
      amount: this.form.get('amount').value,
      paymentType:  "Interkassa",
      currency: this.form.get('payType').value.substring(5),
      code: null
    }
    if (this.promo?.status === PromoStatusEnum.ACTIVE) {
      if (payData.amount*this.wagerRate >= this.promo.minAmount) {
        console.log(this.promo)
        payData.code = this.form.get('promocode').value;
        this.interkassaRecharge(payData);
      } else {
        this.confirmNotUsingPromo(payData);
      }
    } else {
      this.interkassaRecharge(payData);
    }
  }

  confirmNotUsingPromo(payData: InterkassaData) {
    const infoDialog = this.dialog.open(InfoComponent, {
      disableClose: false,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: `Минимальная сумма пополнения для использования промокода: ${this.promo.minAmount} вагеров!`,
              infoText: 'Ты действительно хочешь продолжить пополнение баланса без использования промокода?',
              btn: 'Отменить',
              btnOrange: 'Продолжить'
            },
    });
    this.subscription.add(infoDialog.afterClosed().subscribe(res=> res === 'btnOrange' ? this.interkassaRecharge(payData): null))
  }

  interkassaRecharge(payData: InterkassaData) {
    this.isLoad = true;
    this.subscription.add(this.usersService.toRechargeInterKassa(payData)
      .subscribe ((res: any) => {
        createSendInterkassaRechargeForm(payData);
      }, (err) => {
        this.toastService.error(err.error.errors[0].message);
        this.isLoad = false;
    }));
  }

  freekassaRecharge() {
    this.isLoad = true;
    const data = {
      price: this.form.get('amount').value,
      description: this.userUid
    }
    this.subscription.add(this.usersService.toRechargeFreeKassa(this.userUid, data)
      .subscribe ((res: any) => {
          setTimeout(() => {
            window.open(res.link)
            this.dialogRef.close(true);
          }, 300);
        }, (err) => {
          this.toastService.error(err.error.errors[0].message);
          this.isLoad = false;
    }));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
