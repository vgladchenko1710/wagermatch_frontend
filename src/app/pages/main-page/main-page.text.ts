export const advantages = [
  {
    title: 'Гарантия и безопасность',
    description: `<p>Сервис Wagermatch гарантирует получение прибыли в случае победы в матче Fifa 21.</p>`
    // description: `<p>Сервис Wagermatch гарантирует честный расчет матчей в Fifa 21.</p>`
  },
  {
    title: 'Простота и автоматизация',
    description: `<p>Процесс регистрации и  создания матча на Wagermatch занимает до 2-х минут. Никакой лишней информации, анкет и бланков.</p>`
  },
  {
    title: 'Онлайн поддержка',
    description: `<p>В случае появления любых вопросов, ты можешь написать в техподдержку Wagermatch или на наш telegram канал <a href="https://t.me/wager_match">@wager_match</a></p>`
  },
  {
    title: 'Игра с другом',
    description: `<p>Wagermatch дает возможность играть с другом за одну команду и добиваться побед вместе. <a href="/info/helper/how-to-create-match">Смотреть подробнее.</a> </p>`
  }
]
