import { IMessage } from './../../_components/chat/chat.model';
export enum MatchStatusEnum {
  CREATED = 'created',
  FOUND = 'FOUND',
  READY = 'ready',
  MATCH = 'match',
  VERIFICATION = 'verification',
  FINISHED = 'finished',
  CONFLICT = 'conflict',
  ARCHIVE = 'Archive status'
}

export class IMatchInfo {
  bid: number;
  dateCreated: Date;
  enemyNickName: string;
  enemyWithAlly: boolean;
  gameMode: string;
  haveTime: number;
  isOwner: boolean;
  ownerLogin: string;
  ownerNickName: string;
  platform: string;
  status: {shortName: MatchStatusEnum, fullName: string}
  fullName: string;
  shortName: MatchStatusEnum;
  timeExpire: number;
  winAmount: string
  withAlly: boolean;
  messages?: IMessage[]
}