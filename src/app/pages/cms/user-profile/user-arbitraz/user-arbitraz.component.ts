import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { UsersService } from './../../../../_services/users.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-arbitraz',
  templateUrl: './user-arbitraz.component.html',
  styleUrls: ['./user-arbitraz.component.scss']
})
export class UserArbitrazComponent implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public userUid = '';
  public columns = [
    {
      fieldName: 'uid',
      title: 'Номер Спора',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'date',
      title: 'Дата',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'statusView',
      title: 'Статус',
      extraValue: 'status',
      useColumn: true
    },
    {
      fieldName: 'platform',
      title: 'Платформа',
      extraValue: 'platform',
      row: 1
    },
    {
      fieldName: 'owner',
      title: 'Создатель',
      extraValue: 'matchDetail',
      row: 1
    },
    {
      fieldName: 'btn',
      title: 'Открыть матч',
      extraValue: 'btn',
      row: 1
    },
    {
      fieldName: 'description',
      title: 'Описание',
      extraValue: 'useName',
      row: 2
    },
  ];
  public columnsMobile = [
    {
      fieldName: 'owner',
      title: 'Создатель',
      extraValue: 'matchDetail',
      useColumn: true
    },
    {
      fieldName: 'date',
      title: 'Дата',
      extraValue: 'useName',
      useColumn: true
    },
    {
      fieldName: 'statusView',
      title: 'Статус',
      extraValue: 'status',
      useColumn: true
    },
    {
      fieldName: 'uid',
      title: 'Номер Спора',
      extraValue: 'useName',
      row: 2
    },
    {
      fieldName: 'platform',
      title: 'Платформа',
      extraValue: 'platform',
      row: 1
    },

    {
      fieldName: 'btn',
      title: 'Открыть матч',
      extraValue: 'btn',
      row: 1
    },
    {
      fieldName: 'description',
      title: 'Описание',
      extraValue: 'useName',
      row: 3
    },
  ];
  public conflictList = [];

  constructor(private usersService: UsersService,
              public router: Router,
              public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.subscription.add( this.route.paramMap
      .subscribe ((params: any) => {
        this.userUid = params.get('userUid');
        this.getConflicts();
    }));
  }

  getConflicts() {
      this.subscription.add(this.usersService.getConflicts(this.userUid)
        .subscribe ((response: any) => {
          this.conflictList = response.result.list;
      }));
  }

  openMatch(match) {
    this.router.navigate(['matches/match', match.matchUid]);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
