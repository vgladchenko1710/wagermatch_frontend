import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment-confirm',
  templateUrl: './payment-confirm.component.html',
  styleUrls: ['./payment-confirm.component.scss']
})
export class PaymentConfirmComponent implements OnInit {
  public success = '200';
  public cancel = '500';
  public status = '';

  constructor(private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.queryParamMap
      .subscribe((qParams: ParamMap) => {
        this.status =  qParams.get('status');
        if (this.status!==this.cancel && this.status !== this.success) {
          this.notFound();
        }
        this.redirect();
    });
  }

  redirect() {
    setTimeout(() => {
      this.router.navigate(['/my-profile/account']);
    }, 3000);
  }

  notFound() {
    setTimeout(() => {
      this.router.navigate(['not-found'])
    }, 100);
  }

}
