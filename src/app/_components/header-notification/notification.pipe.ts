import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'notificationPipe'
})
export class NotificationPipe implements PipeTransform {
    transform(shortName: string, nickName: string) {
      const obj = {
        join: `Игрок ${nickName} присоеденился к матчу`,
        drop: `Игрок ${nickName} покинул матч`,
        changeEnemy: `Игрок ${nickName} отказался с тобой играть`,
        delete: `Игрок ${nickName} удалил матч к которому ты был присоеденен`,
        ready: `Игрок ${nickName} готов играть матч`,
        notReady: `Игрок ${nickName} не готов играть матч`,
        start: `Матч с игроком ${nickName} начался`,
        verification : `Игрок ${nickName} отправил результат матча`,
        conflict: `Возьник спор в матче с игроком ${nickName}`,
        conflictResolve: `Вынесено арбитражное решение по спору с игроком ${nickName}`,
        timesUp: `Срок действия созданного тобою матча истек. Матч больше неактивен`,
        finish: `Матч с игроком ${nickName} завершен`,
        autoResult: `Результат матча с игроком ${nickName} расчитан автоматически, так как время ожидания результата истекло`
      };
      return obj[shortName];
    }

}
