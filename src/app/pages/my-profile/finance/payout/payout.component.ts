import { MatchesEventService } from './../../../../_services/events/matches-event.service';
import { ISettingsRates } from './../../../cms/main-settings/main-settings.component';
import { ToastService } from './../../../../_services/toast.service';
import { UsersService } from './../../../../_services/users.service';
import { AuthUserService } from './../../../../_services/auth/auth-user.service';
import { fade, fadeMix } from './../../../../_animations/animations';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Component, OnInit, Inject, OnDestroy, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-payout',
  templateUrl: './payout.component.html',
  styleUrls: ['./payout.component.scss'],
  animations: [fade, fadeMix]
})
export class PayoutComponent implements OnInit {

private subscription: Subscription = new Subscription();
@ViewChild('ccNumber', { static: false }) ccNumber;
public rates: ISettingsRates;
public form: FormGroup;
public cardType;
public minPayout = 100;
public maxPayout = 100000;
public commission = 0;
public isLoad = false;


public wagerRate: number = 1;
public currency: 'руб.'|'грн';

// public payTypes = [
//   {value: 'card', viewValue: 'Банковская карта', icon: '../../../../../assets/MyProfile/card.png'},
//   // {value: 'QIWI', viewValue: 'QIWI', icon: '../../../../../assets/MyProfile/qiwi.png'},
//   // {value: 'YMoney', viewValue: 'ЮMoney', icon: '../../../../../assets/MyProfile/ymoney.png'},
//   {value: 'WebMoney', viewValue: 'WebMoney WMZ', icon: '../../../../../assets/MyProfile/webmoney.png'}
// ];

public payTypes = [
  {value: 'card_RUB', viewValue: 'Банковская карта (руб)', label: 'Номер карты', icon: '../../../../../assets/MyProfile/card.png'},
  {value: 'card_UAH', viewValue: 'Банковская карта (грн)', label: 'Номер карты', icon: '../../../../../assets/MyProfile/card.png'},
  {value: 'wallet_webmoney', viewValue: 'WebMoney WMZ', label: 'Номер кошелька', icon: '../../../../../assets/MyProfile/webmoney.png'}
];
public actualPayType = {
  viewValue: '',
  icon: ''
};
  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              private authUserService: AuthUserService,
              private usersService: UsersService,
              private toastService: ToastService,
              public dialogRef: MatDialogRef<PayoutComponent>,
              public matchesEventService: MatchesEventService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      payType: new FormControl(null, [Validators.required]),
      payData: new FormControl(null, [Validators.required]),
      amount: new FormControl(null, [Validators.required,  Validators.max(this.maxPayout), Validators.min(this.minPayout)])
    });
    this.getRate();
    this.subscription.add(this.form.get('payType')
      .valueChanges
      .subscribe(() => {
        this.payTypes.find(payType => {
          if (payType.value === this.form.get('payType').value) {
            this.actualPayType.viewValue = payType.viewValue;
            this.actualPayType.icon = payType.icon;
          }
        });
        if (this.form.get('payType').value === 'card_UAH') {
          this.wagerRate = this.rates.hrnRate;
          this.currency = 'грн';
        } else {
          this.wagerRate = this.rates.rubRate;
          this.currency = 'руб.';
        }
    }));
    this.subscription.add(this.form.get('payData')
    .valueChanges
    .subscribe(() => {
      this.cardType = this.ccNumber.resolvedScheme$._value;
    }));
  }

  getRate() {
    this.usersService.getRates(this.authUserService.getUserUid())
      .subscribe ((response: any) => {
        this.rates = response.result;
        this.minPayout = +response.result.minOutcome;
        this.maxPayout = +response.result.maxOutcome;
        this.commission = +response.result.outcomeRate;
        this.form.get('amount').setValidators([Validators.required,  Validators.max(this.maxPayout), Validators.min(this.minPayout)]);
        console.log(this.form.get('amount'));
    });
  }

  toPayout() {
    this.isLoad = true;
    const uid = this.authUserService.getUserUid();
    const data = {
      payType: this.form.get('payType').value,
      payData: this.form.get('payData').value,
      amount: this.form.get('amount').value,
    };
    this.subscription.add(this.usersService.payouRequest(uid, data)
    .subscribe ((response: any) => {
      this.toastService.success(response.message);
      this.matchesEventService.toUpdateBalance(-data.amount)
      setTimeout(() => {
        this.dialogRef.close(true);
      }, 600);
      }, (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
    }));
  }

}
