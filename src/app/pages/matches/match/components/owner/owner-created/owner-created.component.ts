import { environment } from './../../../../../../../environments/environment';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'owner-created',
  templateUrl: './owner-created.component.html',
  styleUrls: ['./owner-created.component.scss']
})
export class OwnerCreatedComponent implements OnInit {
  public env = environment;

  constructor() { }

  ngOnInit() {
  }

}
