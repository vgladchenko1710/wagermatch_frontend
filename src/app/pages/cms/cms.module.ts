import { UserMatchListComponent } from './user-matches/user-match-list/user-match-list.component';
import { UserMatchesComponent } from './user-matches/user-matches.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from './../../_components/components.module';
import { CustomMaterialModule } from './../../@theme/custom-material.module';
import { CmsRoutingModule } from './cms-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CmsComponent } from './cms.component';
import { ConflictsComponent } from './conflicts/conflicts.component';
import { ConflictInfoComponent } from './conflict-info/conflict-info.component';
import { UsersComponent } from './users/users.component';
import { TotalFinanceComponent } from './total-finance/total-finance.component';
import { MainSettingsComponent } from './main-settings/main-settings.component';
import { PayoutConfirmComponent } from './total-finance/payout-confirm/payout-confirm.component';
import { LogsComponent } from './logs/logs.component';



@NgModule({
  declarations: [
    DashboardComponent,
    CmsComponent,
    ConflictsComponent,
    ConflictInfoComponent,
    UsersComponent,
    UserMatchesComponent,
    UserMatchListComponent,
    TotalFinanceComponent,
    MainSettingsComponent,
    PayoutConfirmComponent,
    LogsComponent,
  ],
  imports: [
    CommonModule,
    CmsRoutingModule,
    CustomMaterialModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CmsModule { }
