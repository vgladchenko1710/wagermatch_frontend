import { PromoService } from './../../_cms-services/promo.service';
import { ISettingsRates } from './../../main-settings/main-settings.component';
import { ToastService } from './../../../../_services/toast.service';
import { AdminService } from './../../_cms-services/admin.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IPromo } from './../promo.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-promo-action',
  templateUrl: './promo-action.component.html',
  styleUrls: ['./promo-action.component.scss']
})
export class PromoActionComponent implements OnInit {
  public form: FormGroup;
  public isLoading: boolean;
  public durationDays: number = 0;
  public minDate = new Date();

  constructor(public dialogRef: MatDialogRef<PromoActionComponent>,
              @Inject (MAT_DIALOG_DATA) public editData: IPromo,
              private promoService: PromoService,
              private toastService: ToastService) { }

  ngOnInit() {
    this.createForm();
    if (this.editData) {
      this.updateValues();
    }
  }


  updateValues() {
    this.form.patchValue({...this.editData, dateFrom: new Date(this.editData.dateFrom), dateTo: new Date(this.editData.dateTo)});
    this.minDate = this.form.get('dateFrom').value > new Date() ? new Date : this.form.get('dateFrom').value;
    this.form.get('value').disable();
  }

  createForm() {
    this.form = new FormGroup({
      dateFrom: new FormControl(null, [Validators.required]),
      dateTo: new FormControl(null, [Validators.required]),
      code: new FormControl(null, [Validators.required, Validators.minLength(5)]),
      value: new FormControl(null, [Validators.required]),
      minAmount: new FormControl((null), [Validators.required]),
    });
    // dayDuration
    this.form.get('dateFrom')
    .valueChanges
    .subscribe(fromDate => {
      this.durationDays = ~~(fromDate && this.form.get('dateTo').value ?
        ((this.form.get('dateTo').value - fromDate) / 1000 / 60 / 60 / 24) + 1 : null);
    });
    this.form.get('dateTo')
      .valueChanges
      .subscribe(toDate => {
        this.durationDays =  ~~(toDate && this.form.get('dateFrom').value  ?
          ((toDate - this.form.get('dateFrom').value) / 1000 / 60 / 60 / 24) + 1 : null);
    });
  }

  addPromo() {
    this.isLoading = true;
    const promo: IPromo = {...this.form.value};
    promo.dateTo.setHours(23,59,59,999);
    this.promoService.addPromo(promo)
      .subscribe((res: any)=> {
        this.toastService.success(res.message);
        this.isLoading = false;
        setTimeout(()=> this.dialogRef.close(true), 1000);
      }, (err: any)=> {
        this.isLoading = false;
        this.toastService.error(err.error.errors[0].message);
      })
  }

  editPromo() {
    this.isLoading = true;
    const promo: IPromo = {...this.form.value, value: this.form.get('value').value, id: this.editData.id};
    promo.dateTo.setHours(23,55);
    this.promoService.editPromo(promo)
      .subscribe((res: any)=> {
        this.toastService.success("Промокод успешно изменен");
        this.isLoading = false;
        setTimeout(()=> this.dialogRef.close(true), 1000);
      }, (err: any)=> {
        this.isLoading = false;
        this.toastService.error(err.error.errors[0].message);
      })
  }

  deletePromo() {
    this.promoService.deletePromo({id: this.editData.id})
      .subscribe((res: any)=> {
        this.toastService.success("Промокод успешно удален");
        setTimeout(()=> this.dialogRef.close(true), 1000);
      }, (err: any)=> {
        this.toastService.error(err.error.errors[0].message);
      })
  }

}
