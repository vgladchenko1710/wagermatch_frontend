import { LogsService, LogActionEnum, ISourceLogBody } from './../../_services/logs.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from './../../../environments/environment';
import { ToastService } from './../../_services/toast.service';
import { AuthService } from './../../_services/auth/auth.service';
import { FormErrorStateMatcher } from './../../_services/errors/form-error-state.matcher';
import { Subscription } from 'rxjs';
import { EmailConfirmComponent } from './email-confirm/email-confirm.component';
import { LoginComponent } from './../login/login.component';
import { Component, OnInit, ElementRef, ViewChild, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IInitQueryParams } from 'src/app/app.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('password', { static: false }) password: ElementRef;
  public frontLink;
  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  public matcher = new FormErrorStateMatcher();
  private showPassword = false;
  public isLoad: boolean;
  public passwordImage = '../../assets/LoginRegister/show.svg';
  public initQueryParams: IInitQueryParams;

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<RegisterComponent>,
              private dialog: MatDialog,
              protected authService: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              private logsService: LogsService,
              public toastService: ToastService
              ) {
                this.frontLink = (environment.production ? 'https://' : '') + environment.frontend;
                this.initQueryParams = this.route.snapshot.queryParams;
              }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      login: new FormControl(this.initQueryParams?.login, [Validators.required, Validators.minLength(4)]),
      email: new FormControl(this.initQueryParams?.email, [Validators.required, Validators.email, Validators.maxLength(50)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      terms: new FormControl (null, [Validators.required])
    });
  }

  toRegister() {
    this.isLoad = true;
    const user = {
      login: this.form.get('login').value,
      email: this.form.get('email').value,
      password: this.form.get('password').value,
    } as any;
    if (this.initQueryParams.bonus) user.bonus = true;
    this.subscription.add(this.authService.register(user)
      .subscribe((response: any) => {
        this.sendSourceLog();
        this.removeInitQueryParams();
        this.emailConfirm();
        this.isLoad = false;
      },  (err) => {
        this.isLoad = false;
        this.toastService.error(err.error.errors[0].message);
      }
    ));
  }

  sendSourceLog() {
    const data: ISourceLogBody = {
      action: LogActionEnum.REGISTER,
      sourceLink: this.initQueryParams.sourceLinkAction
    };
    this.logsService.sendSourceLog(data).subscribe(()=> {});
  }

  removeInitQueryParams() {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {login: null, email: null},
      queryParamsHandling: "merge",
    });
  }

  togglePaswordShown() {
    this.showPassword = !this.showPassword;
    if (this.showPassword) {
      this.password.nativeElement.setAttribute('type', 'text');
      this.passwordImage = '../../assets/LoginRegister/show_open.svg';
    } else {
      this.password.nativeElement.setAttribute('type', 'password');
      this.passwordImage = '../../assets/LoginRegister/show.svg';
    }
  }

  toast() {
    this.toastService.success('Неверный логин или пароль!');
  }

  openLogin() {
    this.dialog.open(LoginComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
    this.dialogRef.close();
  }

  emailConfirm() {
    this.dialog.open(EmailConfirmComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
