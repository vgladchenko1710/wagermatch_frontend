import { FormErrorStateMatcher } from './../../../_services/errors/form-error-state.matcher';
import { ToastService } from './../../../_services/toast.service';
import { AuthService } from './../../../_services/auth/auth.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  private subscription: Subscription = new Subscription();
  public form: FormGroup;
  public matcher = new FormErrorStateMatcher();
  public isLoading: boolean;

  constructor(@Inject (MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialog,
              protected authService: AuthService,
              public toastService: ToastService,
              public dialogRef: MatDialogRef<ForgotPasswordComponent>) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(50)])
    });
  }

  toReset() {
    console.log('test');

    this.isLoading = true;
    this.subscription.add(this.authService.passwordRequest({email: this.form.get('email').value})
      .subscribe((result: any) => {
        this.toastService.success(result.message);
        this.dialogRef.close();
        },
        (err) => {
          this.isLoading = false;
          this.toastService.error(err.error.errors[0].message);
    }));
  }
}
