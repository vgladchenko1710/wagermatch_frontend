import { WindowsSizeService } from './../../../_services/help-services/window-size.service';
import { Title, Meta } from '@angular/platform-browser';
import { GetMatch } from './../../../_models/match';
import { AuthUserService } from './../../../_services/auth/auth-user.service';
import { MatchesService } from './../../../_services/matches.service';
import { CreateMatchService } from './../../../_services/controllers/create-match.service';
import { fadeMixfast } from './../../../_animations/animations';
import { FilterMobileComponent } from './filter-mobile/filter-mobile.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-all-matches',
  templateUrl: './all-matches.component.html',
  styleUrls: ['./all-matches.component.scss'],
  animations: [fadeMixfast]
})
export class AllMatchesComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  public userUid = '';
  public bidForm: FormGroup;
  MatchesList = [];
  public toShowMatches = 10;
  public activePage = 1;
  public overalMatches = 0;
  // FILTER
  public activePlatform = 'all';
  public loading = false;
  public bidLoading = false;
  public filterDate = false;
  public filterBid = false;
  public isFirstDate = true;
  public toShowFilterBtn = true;

  constructor(public createMatchService: CreateMatchService,
              private filterMobile: MatBottomSheet,
              public location: Location,
              private route: ActivatedRoute,
              private router: Router,
              private authUserService: AuthUserService,
              private matchesService: MatchesService,
              private titleService: Title,
              private meta: Meta,
              public windowsSizeService: WindowsSizeService) {
                this.titleService.setTitle(`Все матчи`+' ‣Wagermatch');
                this.meta.updateTag({name: 'description', content: 'Список всех созданных матчей Wagermatch. Присоедениться к матчу чтобы заключить пари в FIFA 21.'})
               }

  ngOnInit() {
    this.userUid = this.authUserService.getUserUid();
    this.createBidForm();
    this.subscription.add(this.route.queryParamMap
      .subscribe((params) => {
        this.activePage = + (params.get('page') ? params.get('page') : '1');
        this.activePlatform = params.get('platform') ? params.get('platform') : this.authUserService.getUserPlatform();
        this.isFirstDate = params.get('isFirstDate') === 'false' ? false : true;
        this.filterDate = params.get('filterDate') === 'true' ? true : false;
        this.filterBid = params.get('filterBid') === 'true' ? true : false;
        this.bidForm.patchValue({
          min: params.get('min'),
          max: params.get('max'),
        });
        this.loadMatches();
    }));
  }

  loadMatches() {
    this.loading = true;
    const request: GetMatch = {
      page: this.activePage,
      pageSize: this.toShowMatches,
      platform: this.activePlatform,
      bidFrom: this.bidForm.get('min').value ? this.bidForm.get('min').value : 0,
      bidTo: this.bidForm.get('max').value ? this.bidForm.get('max').value : 10000,
      sort: {isDateAsc: this.filterDate, isBidAsc: this.filterBid, isFirstData: this.isFirstDate}
    };
    this.subscription.add(this.matchesService.getMatchlist(request, this.userUid)
      .subscribe(response => {
        this.MatchesList = response['matchList'];
        this.overalMatches = response['count'];
        this.loading = false;
       }, (err) => {
        this.loading = false;
    }));
  }

  platformFilter(newPlatform) {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: { platform: newPlatform, page: null },
        queryParamsHandling: 'merge'
      });
  }

  createBidForm() {
    this.bidForm = new FormGroup({
      min: new FormControl(),
      max: new FormControl()
    });
  }

  bidFilter() {
    this.bidLoading = true;
    setTimeout(() => {
      this.bidLoading = false;
    }, 620);
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {min: this.bidForm.get('min').value, max: this.bidForm.get('max').value,  page: null},
        queryParamsHandling: 'merge'
      });
  }

  changeSort(isFirstDate: boolean) {
    isFirstDate ? this.filterDate = !this.filterDate : this.filterBid = !this.filterBid;
    this.isFirstDate = isFirstDate;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {isFirstDate: this.isFirstDate, filterDate: this.filterDate, filterBid: this.filterBid, page: null},
        queryParamsHandling: 'merge'
      });
  }

  onChangePage() {
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {page: this.activePage},
        queryParamsHandling: 'merge'
      });
  }

  openFilterMobile() {
    const filterData = {
      platform: this.activePlatform,
      min: this.bidForm.get('min').value,
      max: this.bidForm.get('max').value
    };
    const filterMobile = this.filterMobile.open(FilterMobileComponent, {data: filterData});
    this.subscription.add(filterMobile.afterDismissed()
      .subscribe ((res: any) => {
        res ? this.mobileFilter(res) : null;
    }));
  }

  mobileFilter(filterData) {
    this.activePlatform = filterData.platform;
    this.router.navigate(
      [],
      {
        relativeTo: this.route,
        queryParams: {min: filterData.min, max: filterData.max, platform: filterData.platform, page: null},
        queryParamsHandling: 'merge'
      });
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
    const limit = document.body.offsetHeight - window.innerHeight;
    this.toShowFilterBtn = window.pageYOffset / limit < 0.85;
  }

  back() {
    this.location.back();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
