import { environment } from '../../../../environments/environment';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ConflictService {
  constructor(private httpClient: HttpClient) {}

  getConflictsList(adminUid: string, status: string, rowsFrom?: number, rowsTo?: number) {
    return this.httpClient.get(`${environment.apiUrl}/admin/${adminUid}/conflicts?conflicts=${status}&rowsFrom=${rowsFrom}&rowsTo=${rowsTo}`);
  }

  getConflict(adminUid: string, conflictUid: string) {
    return this.httpClient.get(`${environment.apiUrl}/admin/${adminUid}/conflicts-info/${conflictUid}`);
  }

  resolveConflict(adminUid: string, conflictUid: string, data: any) {
    return this.httpClient.put(`${environment.apiUrl}/admin/${adminUid}/conflict-resolve/${conflictUid}`, data);
  }

}
