import { environment } from './../../../environments/environment';
import { InfoComponent } from './../info/info.component';
import { ToastService } from './../../_services/toast.service';
import { AuthUserService } from './../../_services/auth/auth-user.service';
import { FormErrorStateMatcher } from './../../_services/errors/form-error-state.matcher';
import { Router } from '@angular/router';
import { LoginGuardComponent } from './login-guard/login-guard.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './../register/register.component';
import { Component, OnInit, ElementRef, ViewChild, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  @ViewChild('password', { static: false }) password: ElementRef;
  public frontLink;
  public form: FormGroup;
  public matcher = new FormErrorStateMatcher();
  private showPassword = false;
  public passwordImage = '../../assets/LoginRegister/show.svg';
  public error = '';
  public submitted = false;
  public isLoading: boolean;
  private user = {
    login: '',
    password: '',
    rememberMe: null
  };

  constructor(@Inject (MAT_DIALOG_DATA) public data: {loginGuardMesssage?: string, params?: {login?: string}},
              private dialog: MatDialog,
              private authUserService: AuthUserService,
              public toastService: ToastService,
              public router: Router,
              public dialogRef: MatDialogRef<LoginComponent>
              ) {
                this.frontLink = (environment.production ? 'https://' : '') + environment.frontend;
                if (data.loginGuardMesssage) {
                  setTimeout(() => {
                    this.openLoginGuard(data.loginGuardMesssage);
                  }, 700);
                }
              }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.form = new FormGroup({
      email: new FormControl(this.data?.params?.login || null, [Validators.required, Validators.maxLength(50)]),
      password: new FormControl(null, [Validators.required, Validators.maxLength(50)])
    });
  }

  toLogin() {
    this.isLoading = true;
    this.user = {
      login: this.form.get('email').value,
      password: this.form.get('password').value,
      rememberMe: true
    };
    this.subscription.add(this.authUserService.login(this.user)
      .subscribe((result: any) => {
        result.isCookie ? this.adminLogin(result) : this.successLogin(result);
        },
      (err) => {
        this.isLoading = false;
        this.errorLogin(err);
      }
    ));
  }

  successLogin(result: any) {
    setTimeout(() => {
      this.dialogRef.close();
      result.firstTimeLogin ?
        this.openInfo() :
        this.toastService.success(result.message);
      return this.authUserService.isActive() ? this.router.navigateByUrl('matches/all-matches') : this.router.navigateByUrl('my-profile/setting');
    }, 1000);
  }

  adminLogin(result: any) {
    setTimeout(() => {
      this.dialogRef.close();
      this.toastService.success('Lecimy z koksem :)');
      this.router.navigateByUrl('cms');
    }, 500);
  }

  errorLogin(err: any) {
    this.toastService.error(err.error.errors[0].message);
  }

  togglePaswordShown() {
    this.showPassword = !this.showPassword;
    if (this.showPassword) {
      this.password.nativeElement.setAttribute('type', 'text');
      this.passwordImage = '../../assets/LoginRegister/show_open.svg';
    } else {
      this.password.nativeElement.setAttribute('type', 'password');
      this.passwordImage = '../../assets/LoginRegister/show.svg';
    }
  }

  openRegister() {
    this.dialog.open(RegisterComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
    this.dialogRef.close();
  }

  openForgotPassword() {
    this.dialog.open(ForgotPasswordComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '490px',
      data: {}
    });
    this.dialogRef.close();
  }

  openLoginGuard(action: string) {
    this.dialog.open(LoginGuardComponent, {
      backdropClass: 'dialog-back',
      width: '400px',
      data: {action}
    });
  }

  openInfo() {
    this.dialog.open(InfoComponent, {
      disableClose: true,
      backdropClass: 'dialog-back',
      width: '600px',
      data: { head: 'Приветствуем в Wagermatch!',
              infoText: 'Для участия в матчах необходимо выбрать платформу, а так же заполнить поле игровой никнейм',
              closeTimer: true
            }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
